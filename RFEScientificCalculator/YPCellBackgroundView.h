//
//  YPCellBackgroundView.h
//  YandexParking
//
//  Created by ArniDexian on 21/05/14.
//  Copyright (c) 2014 epam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YPCellBackgroundView : UIView

@property (nonatomic) UIColor* lineColor;
@property (nonatomic) UIColor* lineColor2;
@property (nonatomic) UIEdgeInsets lineInsets;
@property (nonatomic) BOOL showDivider;

@end
