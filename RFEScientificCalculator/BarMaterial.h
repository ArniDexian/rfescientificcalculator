//
//  BarMaterial.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 01.10.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Molecule.h"

/*
 Container for storing slab of given material with specified thickness
 */
@interface BarMaterial : NSObject

+(id)barWithMaterial:(MoleculeContainer*)material andThickness:(double)thickness;
-(id)initWithMaterial:(MoleculeContainer*)material andThickness:(double)thickness;

@property (nonatomic, strong) MoleculeContainer* material;
@property (nonatomic) double thickness;

@end
