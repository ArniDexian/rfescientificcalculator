//
//  Array2D.m
//  RFE Puzzle
//
//  Created by Arni-MacBook on 02.12.12.
//  Copyright (c) 2012 Archepix. All rights reserved.
//

#import "Array2D.h"

@interface Array2D(){
  int n_columns;
  int n_rows;
}

@property (nonatomic) NSMutableArray* rowArray;

@end

@implementation Array2D
@synthesize rowArray;

-(id)initWithCols:(int)col andRows:(int)rows
{
  self = [self init];
  if(self){
    [self initArraysWithCol:col andRows:rows];
  }
  return self;
}

-(id)initWithSize:(CGSize)size
{
  self = [self init];
  if(self){
    [self initArraysWithCol:size.width andRows:size.height];
  }
  return self;
}

-(void)initArraysWithCol:(int)cols andRows:(int)rows
{
  rowArray = [NSMutableArray arrayWithCapacity:rows];
  NSObject* obj = [NSObject new];
  for (int i = 0; i < rows; ++i) {
    NSMutableArray* iRow = [NSMutableArray arrayWithCapacity:cols];
    for (int i =0; i < cols; ++i) {
      [iRow addObject:obj];
    }
    [rowArray addObject:iRow];
  }
  
  n_columns = cols;
  n_rows = rows;
}

-(CGSize)dim
{
  return CGSizeMake(n_columns, n_rows);
}

-(int)cols
{
  return n_columns;
}

-(int)rows
{
  return n_rows;
}

-(void)putObject:(NSObject*)obj atX:(int)x andY:(int)y
{
  if(x >= [self cols] || y >= [self rows]){
    NSLog(@"can't add objects - out of exist range: %@ for query x:%i y:%i ", NSStringFromCGSize([self dim]), x, y);
    return;
  }
  
  NSMutableArray* _rowA = [rowArray objectAtIndex:y];
  [_rowA replaceObjectAtIndex:x withObject:obj];
}

-(NSObject*)getObjectAtX:(int)x andY:(int)y
{
  if(x >= [self cols] || y >= [self rows]){
    NSLog(@"can't add objects - out of exist range: %@ for query x:%i y:%i ", NSStringFromCGSize([self dim]), x, y);
    return nil;
  }
  
  NSMutableArray* _rowA = [rowArray objectAtIndex:y];
  NSObject* retObj = [_rowA objectAtIndex:x];
  return retObj;
}

-(void)putObject:(NSObject*)obj atCol:(int)x andRow:(int)y
{
  [self putObject:obj atX:x andY:y];
}

-(NSObject*)getObjectAtCol:(int)col andRow:(int)row
{
  return [self getObjectAtX:col andY:row];
}

-(void)swapObjAt:(CGPoint)obj1pl withAt:(CGPoint)obj2pl
{
  NSObject* obj1 = [self getObjectAtX:obj1pl.x andY:obj1pl.y];
  NSObject* obj2 = [self getObjectAtX:obj2pl.x andY:obj2pl.y];
  
  [self putObject:obj1 atX:obj2pl.x andY:obj2pl.y];
  [self putObject:obj2 atX:obj1pl.x andY:obj1pl.y];
}

-(id)copy
{
  Array2D* copyA = [[Array2D alloc]initWithCols:self.cols andRows:self.rows];
  for (int i = 0; i < n_rows; ++i) {
    for(int j = 0; j < n_columns; ++j){
      NSObject* obj = [self getObjectAtX:j andY:i];
      [copyA putObject:obj atX:j andY:i];
    }
  }
  return copyA;
}

@end


















