//
//  DataCalculator.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 10.04.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbsorptanceTable.h"
#import "Molecule.h"
#import "ChemicalElement.h"
#import "AppUtils.h"


/*======= THIS CLASS DOESN'T NEEDED ========*/
@interface DataCalculator : NSObject

+(BOOL)validateMolecule:(id)molecule;
+(double)molecularMassOfMolecule:(id)molecule;
+(NSArray*)simplifyMolecule:(id)molecule;
+(NSArray*)unitWeightOfAtomsInMolecule:(id)molecule;
+(double)absorptanceOfMolecule:(id)molecule;

@end
