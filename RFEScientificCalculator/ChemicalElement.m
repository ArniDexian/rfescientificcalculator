//
//  ChemicalElement.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 24.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "ChemicalElement.h"
#import "SBJSON.h"
#import "AppUtils.h"

@implementation ChemicalElement

- (instancetype)initWithCoder:(NSCoder *)coder
{
  self = [super init];
  if (self) {
    self.symbol = [coder decodeObjectForKey:NSStringFromSelector(@selector(symbol))];
    self.name = [coder decodeObjectForKey:NSStringFromSelector(@selector(name))];
    self.latName = [coder decodeObjectForKey:NSStringFromSelector(@selector(latName))];
    self.index = [[coder decodeObjectForKey:NSStringFromSelector(@selector(index))] intValue];
    self.atomicMass = [[coder decodeObjectForKey:NSStringFromSelector(@selector(atomicMass))] floatValue];
    self.period = [[coder decodeObjectForKey:NSStringFromSelector(@selector(period))] intValue];
    self.group = [[coder decodeObjectForKey:NSStringFromSelector(@selector(group))] intValue];
    self.energy = [[coder decodeObjectForKey:NSStringFromSelector(@selector(energy))] floatValue];
    self.density = [[coder decodeObjectForKey:NSStringFromSelector(@selector(density))] floatValue];
      
    self.decAtomicMass = [coder decodeObjectForKey:NSStringFromSelector(@selector(decAtomicMass))];
    self.decEnergy = [coder decodeObjectForKey:NSStringFromSelector(@selector(decEnergy))];
    self.decDensity = [coder decodeObjectForKey:NSStringFromSelector(@selector(decDensity))];
  }
  return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
  [coder encodeObject:self.symbol forKey:NSStringFromSelector(@selector(symbol))];
  [coder encodeObject:self.name forKey:NSStringFromSelector(@selector(name))];
  [coder encodeObject:self.latName forKey:NSStringFromSelector(@selector(latName))];
  [coder encodeObject:@(self.index) forKey:NSStringFromSelector(@selector(index))];
  [coder encodeObject:@(self.atomicMass) forKey:NSStringFromSelector(@selector(atomicMass))];
  [coder encodeObject:@(self.period) forKey:NSStringFromSelector(@selector(period))];
  [coder encodeObject:@(self.group) forKey:NSStringFromSelector(@selector(group))];
  [coder encodeObject:@(self.energy) forKey:NSStringFromSelector(@selector(energy))];
  [coder encodeObject:@(self.density) forKey:NSStringFromSelector(@selector(density))];
  
  [coder encodeObject:self.decAtomicMass forKey:NSStringFromSelector(@selector(decAtomicMass))];
  [coder encodeObject:self.decEnergy forKey:NSStringFromSelector(@selector(decEnergy))];
  [coder encodeObject:self.decDensity forKey:NSStringFromSelector(@selector(decDensity))];
}

- (id)copyWithZone:(NSZone *)zone {
  //the object is unique
  return self;
}

-(CGPoint) position
{
    return CGPointMake(self.period, self.group);
}

-(UIColor*)color
{
    return _color ? _color : [UIColor whiteColor];
}

+ (id)elementWithJSONDic:(NSDictionary*)dic {
  ChemicalElement* el = [[ChemicalElement alloc] init];
  
  el.index = [[dic objectForKey:@"index"] intValue];
  el.name = [dic objectForKey:@"name"];
  el.symbol = [dic objectForKey:@"symbol"];
  el.latName = [dic objectForKey:@"latName"];
  el.period = [[dic objectForKey:@"period"] intValue];
  el.group = [[dic objectForKey:@"group"] intValue];
  el.atomicMass = [[dic objectForKey:@"mass"] floatLocalizedValue];
  el.energy = [[dic objectForKey:@"energy"] floatLocalizedValue];
  el.density = [[dic objectForKey:@"density"] floatLocalizedValue];
  el.el_group = [[dic objectForKey:@"el_group"] intValue];
  el.color = [AppUtils colorFromHexString:[dic objectForKey:@"color"]];
  
  NSLocale* locale = [NSLocale localeWithLocaleIdentifier:@"RU"];
  el.decAtomicMass = [NSDecimalNumber decimalNumberWithString:[dic objectForKey:@"mass"] locale:locale];
  el.decEnergy = [NSDecimalNumber decimalNumberWithString:[dic objectForKey:@"energy"] locale:locale];
  el.decDensity = [NSDecimalNumber decimalNumberWithString:[dic objectForKey:@"density"] locale:locale];
  return el;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"%i %@ %@ %i/%i", self.index, self.name, self.symbol, self.period, self.group];
}

- (NSString*)nameStartingWith:(NSString*)startString {
  NSString* name = nil;
  if(startString) {
    NSString* startLowercase = [startString lowercaseString];
    if ([[self.symbol lowercaseString] hasPrefix:startLowercase]) {
      name = self.symbol;
    } else if([[self.latName lowercaseString] hasPrefix:startLowercase]) {
      name = self.latName;
    } else if([[self.name lowercaseString] hasPrefix:startLowercase]) {
      name = self.name;
    }
  }
  return name;
}

- (BOOL)isEqual:(ChemicalElement*)object {
  return [object isKindOfClass:[self class]] && self.index == object.index;
}

@end
