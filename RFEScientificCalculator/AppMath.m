//
//  AppMath.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 17.03.16.
//  Copyright © 2016 Boolbalabs LLC. All rights reserved.
//

#import "AppMath.h"


//RFNum RFNumInt(int intNum) {
//    return [NSDecimalNumber dec
//}
//
//RFNum RFMult(RFNum n1, RFNum n2) {
//    return nil;
//}

NSDecimalNumber* decFromInt(int intNum) {
    NSDecimalNumber* res = [[NSDecimalNumber alloc] initWithInt:intNum];
    return res;
}

NSDecimalNumber* decMult(NSDecimalNumber* d1, NSDecimalNumber* d2) {
    return [d1 decimalNumberByMultiplyingBy:d2];
}

NSDecimalNumber* decMultInt(NSDecimalNumber* d1, int num1) {
    return decMult(d1, decFromInt(num1));
}

NSDecimalNumber* decDiv(NSDecimalNumber* d1, NSDecimalNumber* d2) {
    return [d1 decimalNumberByDividingBy:d2];
}

NSDecimalNumber* decSum(NSDecimalNumber* d1, NSDecimalNumber* d2) {
    return [d1 decimalNumberByAdding:d2];
}

NSDecimalNumber* decDif(NSDecimalNumber* d1, NSDecimalNumber* d2) {
    return [d1 decimalNumberBySubtracting:d2];
}

NSDecimalNumber* decPow(NSDecimalNumber* d1, NSUInteger pow) {
    return [d1 decimalNumberByRaisingToPower:pow];
}

BOOL decEqual(NSDecimalNumber* d1, NSDecimalNumber* d2) {
    return [d1 compare:d2] == NSOrderedSame;
}

BOOL decLess(NSDecimalNumber* d1, NSDecimalNumber* d2) {
    return [d1 compare:d2] == NSOrderedAscending;
}

BOOL decGreater(NSDecimalNumber* d1, NSDecimalNumber* d2) {
    return [d1 compare:d2] == NSOrderedDescending;
}

BOOL decLessOrEqual(NSDecimalNumber* d1, NSDecimalNumber* d2) {
    NSComparisonResult res = [d1 compare:d2];
    return res == NSOrderedAscending || res == NSOrderedSame;
}

BOOL decGreaterOrEqual(NSDecimalNumber* d1, NSDecimalNumber* d2) {
    NSComparisonResult res = [d1 compare:d2];
    return res == NSOrderedDescending || res == NSOrderedSame;
}

BOOL decIsZero(NSDecimalNumber* d1) {
    return decEqual(d1, [NSDecimalNumber zero]);
}

BOOL decIsNan(NSDecimalNumber* d1) {
    return decEqual(d1, [NSDecimalNumber notANumber]);
}

BOOL decValid(NSDecimalNumber* d1) {
    return d1 != nil && !decIsNan(d1);
}

NSDecimalNumber* decAbs(NSDecimalNumber* num) {
    NSDecimalNumber* res = num;
    if (decLess(num, [NSDecimalNumber zero])) {
        res = decDif([NSDecimalNumber zero], num);
    }
    return res;
}

@implementation AppMath

+ (double)deltaFromDouble:(double)number {
    double res = [[self deltaDecimalNumberFromDouble:number] doubleValue];
    return res;
}

+ (NSDecimalNumber*)deltaDecimalNumberFromDouble:(double)number {
    NSString *strNum = [NSString stringWithFormat:@"%f", number];
    NSDecimalNumber *decNum = [[NSDecimalNumber alloc] initWithString:strNum];
    return [decNum deltaNumber];
}

@end
