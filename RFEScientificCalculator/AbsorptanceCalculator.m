//
//  AbsorptanceCalculator.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 14.04.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "AbsorptanceCalculator.h"
#import "PeriodicTable.h"
#import "AbsorptanceTable.h"

@interface AbsorptanceCalculator ()

@property (nonatomic) NSArray* molecule;
@property (nonatomic) NSArray* unitWeights;
@property (nonatomic) NSArray* unitWeightsDelta;
@property (nonatomic) NSDecimalNumber* absorptance;
@property (nonatomic) NSDecimalNumber* absorptanceDelta;

@property (nonatomic) NSDecimalNumber* energy;
@property (nonatomic) NSDecimalNumber* density;

@end




@implementation MoleculeAbsorptance

- (instancetype)init {
    self = [super init];
    return self;
}

- (BOOL)hasAbsorptance {
    return decValid(self.decAbsorptance);
}

- (NSDecimalNumber*)absorptanceError {
    NSDecimalNumber* res = nil;
    if (decValid(self.absorptanceDelta) && !decIsZero(self.decAbsorptance)) {
        res = decAbs(decDiv(self.absorptanceDelta, self.decAbsorptance));
    }
    return res;
}

- (NSDecimalNumber*)tableAbsorptanceError {
    NSDecimalNumber* res = nil;
    if (decValid(self.tableAbsorptanceDelta) && !decIsZero(self.decTableAbsorptance)) {
        res = decAbs(decDiv(self.tableAbsorptanceDelta, self.decTableAbsorptance));
    }
    return res;
}

@end


@implementation AbsorptanceCalculator

#pragma mark - properties

- (void)setMoleculeForCalculation:(id)molecule withDensity:(NSDecimalNumber*)density
{
  if (![[self class] validateMolecule:molecule]) return;
  
  self.molecule = [self simplifyMolecule:molecule];
  self.density = density;
  self.absorptance = nil;
  self.unitWeights = [self unitWeightOfAtomsInMolecule];
  self.unitWeightsDelta = [self unitWeightOfAtomsInMoleculeDelta];
}

#pragma mark - inittialization

+ (id)calculatorWithMolecule:(id)molecule andDensity:(NSDecimalNumber*)density {
  id inst = [[[self class] alloc] init];
  [inst setMoleculeForCalculation:molecule withDensity:density];
  return inst;
}

#pragma mark - calculation

+ (BOOL)validateMolecule:(id)molecule {
  if(molecule == nil) return NO;
  else return YES;
}

- (NSArray*)simplifyMolecule:(id)molecule {
  if(![molecule isKindOfClass:[MoleculeContainer class]] && ![molecule isKindOfClass:[NSArray class]]) {
    NSAssert(NO, @"simplifyMolecule bad argument");
    return nil;
  }
  
  NSMutableArray* molecs = [NSMutableArray array];
  [self recursivePutMolecule:molecule toArray:molecs quantity:1];
  NSArray* joined = [self joinMolecules:molecs];
  
  return joined;
}

- (NSArray*)joinMolecules:(NSArray*)molecs {
  NSMutableArray* joined = [NSMutableArray array];
  for (MoleculeContainer* molec in molecs) {
    NSUInteger prevInd = [joined indexOfObjectPassingTest:^BOOL(MoleculeContainer* obj, NSUInteger idx, BOOL *stop) {
      return [obj.element isEqual:molec.element];
    }];
    if (prevInd == NSNotFound) {
      [joined addObject:[molec copy]];
    } else {
      MoleculeContainer* cont = joined[prevInd];
      cont.quantity += molec.quantity;
    }
  }
  return joined;
}

- (void)recursivePutMolecule:(id)molecule toArray:(NSMutableArray*)array quantity:(int)quantity {
  if(!molecule) {
    return;
  }
  
  if([molecule isKindOfClass:[MoleculeContainer class]]) {
    MoleculeContainer* cont = [(MoleculeContainer*)molecule copy];
    if(cont.quantity < 1) cont.quantity = 1;
    
    if ([cont.element isKindOfClass:[ChemicalElement class]]) {
      cont.quantity *= quantity;
      [array addObject:cont];
    } else {
      [self recursivePutMolecule:cont.element toArray:array quantity:cont.quantity*quantity];
    }
  } else if([molecule isKindOfClass:[NSArray class]]) {
    NSArray* molecs = molecule;
    for (int i = 0; i < molecs.count; ++i) {
      [self recursivePutMolecule:molecs[i] toArray:array quantity:quantity];
    }
  }
}


#pragma mark - molecular mass 

- (NSDecimalNumber*)molecularMassOfMolecule {
    NSDecimalNumber* mass = [NSDecimalNumber zero];
    for (int i = 0; i < self.molecule.count; ++i) {
        MoleculeContainer* cont = [self.molecule objectAtIndex:i];
//        mass += cont.quantity * [(ChemicalElement*)cont.element atomicMass];
        mass = decSum(mass, decMultInt([(ChemicalElement*)cont.element decAtomicMass], cont.quantity));
    }
    return mass;
}

//dMi = d(n*a) = dn*a + n*da, where dn = 0
- (NSDecimalNumber*)molecularMassOfMoleculeDelta {
    NSDecimalNumber* delta = [NSDecimalNumber zero];
    for (MoleculeContainer* cont in self.molecule) {
//        delta = cont.quantity * [[cont element] deltaAtomicMass];
        delta = decMultInt([[cont element] deltaAtomicMassNumber], cont.quantity);
    }
    return delta;
}

#pragma mark - unit weight

- (NSArray*)unitWeightOfAtomsInMolecule {
    NSDecimalNumber* molecMass = [self molecularMassOfMolecule];
    if(molecMass == nil) {
        return nil;
    }
    
    NSMutableArray* unitWeights = [NSMutableArray array];
    for (int  i = 0; i < self.molecule.count; ++i) {
        MoleculeContainer* cont = [self.molecule objectAtIndex:i];
//        double unitW = cont.quantity * [(ChemicalElement*)cont.element atomicMass] / molecMass;
        NSDecimalNumber* unitW = decDiv(decMultInt([(ChemicalElement*)cont.element decAtomicMass], cont.quantity), molecMass);
        [unitWeights addObject:unitW];
    }
    return unitWeights;
}

//dUW = d(a/M) = (M*da - a*dM)/M^2
- (NSArray*)unitWeightOfAtomsInMoleculeDelta {
    NSDecimalNumber* M = [self molecularMassOfMolecule];
    if(M == nil) {
        return nil;
    }
    NSDecimalNumber* dM = [self molecularMassOfMoleculeDelta];
    
    NSMutableArray* unitWeightsDelta = [NSMutableArray array];
    for (int  i = 0; i < self.molecule.count; ++i) {
        MoleculeContainer* cont = [self.molecule objectAtIndex:i];
        NSDecimalNumber* a = [(ChemicalElement*)cont.element decAtomicMass];
        NSDecimalNumber* da = [(ChemicalElement*)cont.element deltaAtomicMassNumber];
//        double dUnitW = cont.quantity * (M * da - a * dM) / (M * M);
        NSDecimalNumber* dUnitW = decDiv(decMultInt(decDif(decMult(M, da), decMult(a, dM)), cont.quantity), decMult(M, M));
        [unitWeightsDelta addObject:dUnitW];
    }
    return unitWeightsDelta;
}

#pragma mark - absorptance

- (MoleculeAbsorptance*)absorptanceOfSimpleMolecule:(MoleculeContainer*)molec {
    PeriodicTable* periodicTable = [PeriodicTable sharedInstance];
    AbsorptanceTable* absTable = [AbsorptanceTable sharedInstance];
    
    MoleculeAbsorptance* abs = [MoleculeAbsorptance new];
    abs.decInputEnergy = self.energy;
    
    int absorberIndex = [(ChemicalElement*)molec.element index];
    PeriodicTableEnergyResult* source = [periodicTable elementWithEnergy:abs.decInputEnergy];
    
    if (source.approximated && source.approximatedRightElement != nil) {
        abs.interpolated = YES;
        abs.decTableEnergy = source.element.decEnergy;
        abs.decTableAbsorptance = [absTable absorptanceForSource:source.element.index andAbsorber:absorberIndex];
        abs.tableAbsorptanceDelta = [abs.decTableAbsorptance deltaNumber];
        
        NSDecimalNumber* A0 =  [absTable absorptanceForSource:source.approximatedLeftElement.index andAbsorber:absorberIndex];
        NSDecimalNumber* A1 = [absTable absorptanceForSource:source.approximatedRightElement.index andAbsorber:absorberIndex ];
        NSDecimalNumber* E0 =  source.approximatedLeftElement.decEnergy;
        NSDecimalNumber* E1 = source.approximatedRightElement.decEnergy;
        NSDecimalNumber* e = abs.decInputEnergy; //source.inputEnergy; is the same
        
        if (decValid(A0) && decValid(A1) && decValid(E0) && decValid(E1) && decValid(e)) {
            NSDecimalNumber* de = [e deltaNumber];
            NSDecimalNumber* dA0 = [A0 deltaNumber];
            NSDecimalNumber* dA1 = [A1 deltaNumber];
            NSDecimalNumber* dE0 = [E0 deltaNumber];
            NSDecimalNumber* dE1 = [E1 deltaNumber];
            
            // TEST BLOCK
//            {
//                double _A0 = [A0 doubleValue];
//                double _A1 = [A1 doubleValue];
//                double _E0 = [E0 doubleValue];
//                double _E1 = [E1 doubleValue];
//                double _e = [e doubleValue];
//
//                double _de = [de doubleValue];
//                double _dA0 = [dA0 doubleValue];
//                double _dA1 = [dA1 doubleValue];
//                double _dE0 = [dE0 doubleValue];
//                double _dE1 = [dE1 doubleValue];
//
//                double _abs = _A0 + (_A1 - _A0) * (_e - _E0) / (_E1 - _E0);
//                double _absDelta = _dA0 + ((_E1-_E0) * ((_dA1-_dA0)*(_e-_E0)+(_A1-_A0)*(_de-_dE0)) - (_A1-_A0)*(_e-_E0)*(_dE1-_dE0)) / ((_E1-_E0)*(_E1-_E0));
//            }
            // END TEST BLOCK
            
            //A0 + (A1 - A0) * (e - E0) / (E1 - E0);
            abs.decAbsorptance = decSum(A0, decDiv(decMult(decDif(A1, A0), decDif(e, E0)), decDif(E1, E0)));
            
            // dA0 + ((E1-E0) * ((dA1-dA0)*(e-E0)+(A1-A0)*(de-dE0)) - (A1-A0)*(e-E0)*(dE1-dE0)) / ((E1-E0)*(E1-E0));
            abs.absorptanceDelta = decSum(dA0/*1ый член*/, decDiv(/*числитель*/decDif(decMult(decDif(E1, E0), decSum(decMult(decDif(dA1, dA0), decDif(e, E0)), decMult(decDif(A1, A0), decDif(de, dE0)))), decMult(decMult(decDif(A1, A0), decDif(e, E0)), decDif(dE1, dE0))) , /*знаменатель*/decMult(decDif(E1, E0), decDif(E1, E0))));
        }
    } else {
        abs.interpolated = NO;
        abs.decAbsorptance = [absTable absorptanceForSource:source.element.index andAbsorber:absorberIndex];
        abs.absorptanceDelta = [abs.decAbsorptance deltaNumber];
    }
    
    return abs;
}

- (MoleculeAbsorptance*)absorptanceOfMoleculeWithEnergy:(NSDecimalNumber*)energy {
  if(!self.molecule.count) {
    return nil;
  }
  self.energy = energy;
  
  MoleculeAbsorptance* molecAbs = [MoleculeAbsorptance new];
  molecAbs.decInputEnergy = energy;
    
    //do not calculate for invalid energy
    if(![self energyValid]) {
        return molecAbs;
    }
  
  NSMutableArray* absorptances = [NSMutableArray array];
  for (MoleculeContainer* molec in self.molecule) {
     MoleculeAbsorptance *iAbs = [self absorptanceOfSimpleMolecule:molec];
    if (![iAbs hasAbsorptance]) {
      return molecAbs;
    }
    [absorptances addObject:iAbs];
  }
  
  //find table value
  for (MoleculeAbsorptance* abs in absorptances) {
    if (abs.interpolated) {
      molecAbs.interpolated = YES;
        molecAbs.decTableEnergy = abs.decTableEnergy;
      molecAbs.decTableAbsorptance = [self absorptanceFromArray:absorptances absProperty:@selector(decTableAbsorptance)];
      molecAbs.tableAbsorptanceDelta = [self absorptanceDeltaFromArray:absorptances absProperty:@selector(decTableAbsorptance) absDeltaProperty:@selector(tableAbsorptanceDelta)];
      break;
    }
  }
  
  self.absorptance = [self absorptanceFromArray:absorptances absProperty:@selector(decAbsorptance)];
  self.absorptanceDelta = [self absorptanceDeltaFromArray:absorptances absProperty:@selector(decAbsorptance) absDeltaProperty:@selector(absorptanceDelta)];
  
  molecAbs.decAbsorptance = self.absorptance;
    molecAbs.absorptanceDelta = self.absorptanceDelta;
  return molecAbs;
}

- (BOOL)energyValid {
    return self.energy != nil && !decIsZero(self.energy) && !decIsNan(self.energy);
}

- (NSDecimalNumber*)absorptanceFromArray:(NSArray*)absorptances absProperty:(SEL)selector {
    NSDecimalNumber* sum = [NSDecimalNumber zero];
  NSString* propName = NSStringFromSelector(selector);
  for (int i = 0; i < absorptances.count; ++i) {
    MoleculeAbsorptance* molecAbs = absorptances[i];
    NSDecimalNumber* abs = [molecAbs valueForKey:propName];
    NSDecimalNumber* unitWeight = _unitWeights[i];
//    sum += abs*unitWeight;
      sum = decSum(sum, decMult(abs, unitWeight));
  }
  
//  double res = sum * self.density;
    NSDecimalNumber* res = decMult(sum, self.density);
  return res;
}

- (NSDecimalNumber*)absorptanceDeltaFromArray:(NSArray*)absorptances absProperty:(SEL)absProperty absDeltaProperty:(SEL)absDeltaProperty {
    NSDecimalNumber* sum = [NSDecimalNumber zero];
    NSDecimalNumber* dSum = [NSDecimalNumber zero];
    NSString* propName = NSStringFromSelector(absProperty);
    NSString* deltaPropName = NSStringFromSelector(absDeltaProperty);
    for (int i = 0; i < absorptances.count; ++i) {
        MoleculeAbsorptance* molecAbs = absorptances[i];
        NSDecimalNumber* abs = [molecAbs valueForKey:propName];
        NSDecimalNumber* dAbs = [molecAbs valueForKey:deltaPropName];
        NSDecimalNumber* unitWeight = _unitWeights[i];
        NSDecimalNumber* dUnitWeight = self.unitWeightsDelta[i];
//        dSum += (abs * dUnitWeight + dAbs * unitWeight);
        dSum = decSum(dSum, decSum(decMult(abs, dUnitWeight), decMult(dAbs, unitWeight)));
//        sum += abs*unitWeight;
        sum = decSum(sum, decMult(abs, unitWeight));
    }
    
//    double res = dSum * self.density + sum * [self deltaDensity];
    NSDecimalNumber* res = decSum(decMult(dSum, self.density), decMult(sum, [self.density deltaNumber]));
    return res;
}


+ (ChemicalElement*)simpleMolecule:(id)molecule {
  if(![molecule isKindOfClass:[MoleculeContainer class]] && ![molecule isKindOfClass:[NSArray class]]) {
    return nil;
  }
  
  NSMutableArray* molecs = [NSMutableArray array];
  [self recursiveSumMolecule:molecule data:molecs];
  return [molecs count] == 1 ? molecs.firstObject : nil;
}

+ (BOOL)isEmptyMolecule:(id)molecule {
  if(![molecule isKindOfClass:[MoleculeContainer class]] && ![molecule isKindOfClass:[NSArray class]]) {
    return NO;
  }
  
  NSMutableArray* molecs = [NSMutableArray array];
  [self recursiveSumMolecule:molecule data:molecs];
  return [molecs count] == 0;
}

+ (BOOL)recursiveSumMolecule:(id)molecule data:(NSMutableArray*)array {
  if(molecule) {
    if([molecule isKindOfClass:[MoleculeContainer class]]) {
      MoleculeContainer* cont = (MoleculeContainer*)molecule;
      if ([cont.element isKindOfClass:[ChemicalElement class]]) {
        if (![array containsObject:cont.element]) {
          [array addObject:cont.element];
          if ([array count] >= 2) {
            return YES;
          }
        }
      } else {
        [self recursiveSumMolecule:cont.element data:array];
      }
    } else if([molecule isKindOfClass:[NSArray class]]) {
      NSArray* molecs = molecule;
      for (int i = 0; i < molecs.count; ++i) {
        if([self recursiveSumMolecule:molecs[i] data:array]) {
          return YES;
        }
      }
    }
  }
  return NO;
}

@end























