//
//  NSString+Convenience.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 08.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "NSString+Convenience.h"

@implementation NSString (Convenience)

- (BOOL)isDigitCharacterAtIndex:(NSUInteger)index {
	if (index < self.length) {
    unichar symb = [self characterAtIndex:index];
		static NSCharacterSet* charset = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
      charset = [NSCharacterSet decimalDigitCharacterSet];
    });
    
		return [charset characterIsMember:symb];
	}
	return NO;
}

- (BOOL)isStringDigit {
  BOOL isdig = [self isDigitCharacterAtIndex:0];
  return isdig;
}

- (BOOL)isStringLeftBracket {
  if(self.length == 1) {
    static NSCharacterSet* charset = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
      charset = [NSCharacterSet characterSetWithCharactersInString:@"[{<("];
    });
    unichar symb = [self characterAtIndex:0];
    return [charset characterIsMember:symb];
  } else {
    return NO;
  }
}

- (BOOL)isStringRightBracket {
  if(self.length == 1) {
    static NSCharacterSet* charset = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
      charset = [NSCharacterSet characterSetWithCharactersInString:@"]}>)"];
    });
    unichar symb = [self characterAtIndex:0];
    return [charset characterIsMember:symb];
  } else {
    return NO;
  }
}

- (BOOL)isStringNumber {
  static NSNumberFormatter* numberFormatter = nil;
  
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    numberFormatter = [[NSNumberFormatter alloc] init];
  });
  
  return [numberFormatter numberFromString:self] != nil;
}

- (float)floatLocalizedValue {
  return [[self stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
}

- (NSDecimalNumber*)decimalValue {
    NSString* numStr = [self stringByReplacingOccurrencesOfString:@"," withString:@"."];
    id res = [NSDecimalNumber decimalNumberWithString:numStr locale:[NSLocale localeWithLocaleIdentifier:@"EN"]];
    return res;
}

@end
