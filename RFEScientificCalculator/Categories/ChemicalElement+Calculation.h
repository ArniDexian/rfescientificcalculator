//
//  ChemicalElement+Calculation.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 17.03.16.
//  Copyright © 2016 Boolbalabs LLC. All rights reserved.
//

#import "ChemicalElement.h"

@interface ChemicalElement (Calculation)

- (double)deltaAtomicMass;
- (NSDecimalNumber*)deltaAtomicMassNumber;

@end
