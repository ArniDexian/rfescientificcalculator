//
//  ChemicalElement+Calculation.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 17.03.16.
//  Copyright © 2016 Boolbalabs LLC. All rights reserved.
//

#import "ChemicalElement+Calculation.h"

@implementation ChemicalElement (Calculation)

- (double)deltaAtomicMass {
    return [[self deltaAtomicMassNumber] doubleValue];
}

- (NSDecimalNumber*)deltaAtomicMassNumber {
    return [[self decAtomicMass] deltaNumber];
}

@end
