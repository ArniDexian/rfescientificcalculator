//
//  AppDelegate+Appearance.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 25/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "AppDelegate+Appearance.h"

@implementation AppDelegate (Appearance)

+ (void)setupAppearance {
  [self setupNavigationBar];
  
  [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
}

+ (void)setupNavigationBar {
  [[UINavigationBar appearance] setTitleTextAttributes:
   [NSDictionary dictionaryWithObjectsAndKeys:
    [UIColor whiteColor], (IS_OS_7_OR_HIGHER ? NSForegroundColorAttributeName : UITextAttributeTextColor),
    [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.], UITextAttributeTextShadowColor,
    [NSValue valueWithUIOffset:UIOffsetMake(0, 0)], UITextAttributeTextShadowOffset,
    [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.f], UITextAttributeFont,
    nil]];
  
  if (IS_OS_7_OR_HIGHER) {
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UITextField appearance] setTintColor:[UIColor whiteColor]];
  }

  UIImage *strImage = [[UIImage imageNamed: @"navbar"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 1, 10, 1)];
  [[UINavigationBar appearance] setBackgroundImage:strImage
                                     forBarMetrics:UIBarMetricsDefault];
}

@end
