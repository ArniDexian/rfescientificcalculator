//
//  NSDecimalNumber+Addition.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 17.03.16.
//  Copyright © 2016 Boolbalabs LLC. All rights reserved.
//

#import "NSDecimalNumber+Addition.h"

@implementation NSDecimalNumber (Addition)

- (NSDecimalNumber*)deltaNumber {
    signed int exp = self.decimalValue._exponent;
    //    half of the precision i.e 0.907 is 0.0005, 1.04 is 0.005
    exp = MIN(exp, 0);
    NSDecimalNumber* dANumber = [NSDecimalNumber decimalNumberWithMantissa:5 exponent:exp - 1 isNegative:NO];
    return dANumber;
}

@end
