//
//  NSString+Convenience.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 08.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Convenience)

- (BOOL)isDigitCharacterAtIndex:(NSUInteger)index;

- (BOOL)isStringDigit;
- (BOOL)isStringLeftBracket;
- (BOOL)isStringRightBracket;

- (BOOL)isStringNumber;

- (float)floatLocalizedValue;
- (NSDecimalNumber*)decimalValue;
@end
