//
//  NSDecimalNumber+Addition.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 17.03.16.
//  Copyright © 2016 Boolbalabs LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDecimalNumber (Addition)
- (NSDecimalNumber*)deltaNumber;
@end
