//
//  AppDelegate+Appearance.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 25/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Appearance)

+ (void)setupAppearance;

@end
