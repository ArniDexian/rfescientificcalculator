//
//  UIColor+Predefined.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 25/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Predefined)

+ (UIColor*)elementsBlueColor;
+ (UIColor*)backgroundScreenColor;
+ (UIColor*)mainLabelsColor;
@end
