//
//  UIColor+Predefined.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 25/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "UIColor+Predefined.h"

@implementation UIColor (Predefined)

+ (UIColor*)elementsBlueColor {
  return RGB(43.f, 212.f, 243.f);
}

+ (UIColor*)backgroundScreenColor {
  return RGB(103.f, 103.f, 103.f);
}

+ (UIColor*)mainLabelsColor {
  return [UIColor whiteColor];
}


@end
