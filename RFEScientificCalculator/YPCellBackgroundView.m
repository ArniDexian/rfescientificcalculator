//
//  YPCellBackgroundView.m
//  YandexParking
//
//  Created by ArniDexian on 21/05/14.
//  Copyright (c) 2014 epam. All rights reserved.
//

#import "YPCellBackgroundView.h"

@implementation YPCellBackgroundView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
      self.showDivider = YES;
      self.opaque = NO;
      self.backgroundColor = [UIColor clearColor];
      self.lineColor = RGB(86.f, 86.f, 86.f);
      self.lineColor2 = RGB(160.f, 160.f, 160.f);
      self.lineInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
  [super drawRect:rect];
  if (self.showDivider) {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    CGContextSetShouldAntialias(ctx, NO);
    CGContextSetStrokeColorWithColor(ctx, self.lineColor2.CGColor);
    
    CGFloat width = 1.f;//1.0f / [UIScreen mainScreen].scale;
    CGPoint fromPoint = CGPointMake(self.lineInsets.left, CGRectGetHeight(self.frame));
    CGPoint toPoint = CGPointMake(CGRectGetWidth(rect) - self.lineInsets.right, fromPoint.y);
    
    CGContextSetLineWidth(ctx, width);
    
    CGContextMoveToPoint(ctx, fromPoint.x, fromPoint.y);
    CGContextAddLineToPoint(ctx, toPoint.x, toPoint.y);
    CGContextStrokePath(ctx);
    
    //second line
    CGContextSetStrokeColorWithColor(ctx, self.lineColor.CGColor);
  
    CGContextMoveToPoint(ctx, fromPoint.x, fromPoint.y - width);
    CGContextAddLineToPoint(ctx, toPoint.x, toPoint.y - width);
    
    CGContextStrokePath(ctx);
  }
}

@end
