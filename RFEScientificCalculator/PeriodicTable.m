//
//  PeriodicTable.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 10.04.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "PeriodicTable.h"
#import "LocalStorageProvider.h"

@implementation PeriodicTableEnergyResult

@end

@implementation PeriodicTable

- (void)setElements:(NSArray *)elements {
//  [super willChangeValueForKey:@"elements"];
  _elements = elements;
//  [super didChangeValueForKey:@"elements"];
}

+ (PeriodicTable *)sharedInstance {
  static PeriodicTable *sharedSingleton = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedSingleton = [[PeriodicTable alloc] init];
  });
  return sharedSingleton;
}

- (id)init {
  self = [super init];
  if(self){
    [self loadElements];
  }
  return self;
}


- (void)loadElements {
  [LocalStorageProvider requestElementsWithComplete:^(NSArray *elements) {
    NSLog(@"periodic elements loaded, count: %lu", (unsigned long)elements.count);
    self.elements = elements;
    self.maxEnergy = [elements valueForKeyPath:@"@max.decEnergy"];
  } error:^(NSError *error) {
    NSLog(@"error loading periodic table elements: %@", error);
  }];
}


#pragma mark - methods

- (PeriodicTableEnergyResult*)elementWithEnergy:(NSDecimalNumber*)energy {
  if(!self.elements || decGreater(decDif(energy, self.maxEnergy), decFromInt(1))) { //(energy - self.maxEnergy) > 1.0
    return nil;
  }
  
  PeriodicTableEnergyResult* result = [PeriodicTableEnergyResult new];
  result.inputEnergy = energy;
  
  NSDecimalNumber* dE = decFromInt(10000);//fabs(energy - [self.elements[0] energy]);
  
  int i;
  for (i = 0; i < self.elements.count; i++) {
    ChemicalElement* iChem = self.elements[i];
//    double idE = fabs(iChem.energy - energy);
/*      NSDecimalNumber* idE = nil;
      @try {
          idE = decAbs(decDif(iChem.decEnergy, energy));
      } @catch (NSException *exception) {
          NSLog(@"%@, %@", iChem.decEnergy, energy);
      } @finally {
          NSLog(@"");
//          idE = [NSDecimalNumber zero];
      }*/
      if (decValid(iChem.decEnergy)) {
          NSDecimalNumber* idE = decAbs(decDif(iChem.decEnergy, energy));
          if (decLess(idE, dE)) {//(idE < dE) {
              dE = idE;
              result.element = iChem;
              if (decIsZero(dE)) {//dE == .0) {
                  break;
              }
          }
      }
  }
  
     result.approximated = !decIsZero(dE);// dE != .0;
  //set left and right nearest elements
  if (result.approximated) {
    int ind = (int)[self.elements indexOfObject:result.element];
    ChemicalElement *leftElement, *rightElement;
    if (ind - 1 >= 0 && ind - 1 < [_elements count]) {
      leftElement = self.elements[ind - 1];
    }
    if (ind + 1 >= 0 && ind + 1 < [self.elements count]) {
      rightElement = self.elements[ind + 1];
    }
    if (decGreater(result.element.decEnergy, result.inputEnergy)) {//result.element.energy > result.inputEnergy) {
      result.approximatedLeftElement = leftElement;
      result.approximatedRightElement = result.element;
    } else {
      result.approximatedLeftElement = result.element;
      result.approximatedRightElement = rightElement;
    }
  }
  
  return result;
}

- (NSArray*)elementsStartingWithName:(NSString *)name {
  NSArray* result = nil;
  if(name.length > 0) {
    NSPredicate* predic = [NSPredicate predicateWithFormat:@"(symbol BEGINSWITH[cd] %@) OR"
                           "(name BEGINSWITH[cd] %@) OR (latName BEGINSWITH[cd] %@)",
                           name, name, name];
    result = [self.elements filteredArrayUsingPredicate:predic];
  }
  return result;
}

@end


















