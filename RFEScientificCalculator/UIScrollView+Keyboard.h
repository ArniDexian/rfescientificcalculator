//
//  UIScrollView+Keyboard.h
//  YandexParking
//
//  Created by Victor Kravchenko on 9/6/13.
//  Copyright (c) 2013 epam. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (Keyboard)

- (void)registerForKeyboardNotifications;
- (void)unregisterFromKeyboardNotifications;
- (void)restoreScrolViewInsets;
@end
