//
//  UIScrollView+Keyboard.m
//  YandexParking
//
//  Created by Victor Kravchenko on 9/6/13.
//  Copyright (c) 2013 epam. All rights reserved.
//

#import "UIScrollView+Keyboard.h"
#import <objc/runtime.h>

@implementation UIScrollView (Keyboard)

- (void)registerForKeyboardNotifications {
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(keyboardWasShown:)
                                               name:UIKeyboardDidShowNotification object:nil];
  [[NSNotificationCenter defaultCenter] addObserver:self
                                           selector:@selector(keyboardWillBeHidden:)
                                               name:UIKeyboardWillHideNotification object:nil];
}

- (void)unregisterFromKeyboardNotifications {
  [[NSNotificationCenter defaultCenter] removeObserver:self
                                                  name:UIKeyboardDidShowNotification
                                                object:nil];
  [[NSNotificationCenter defaultCenter] removeObserver:self
                                                  name:UIKeyboardWillHideNotification
                                                object:nil];
}

- (void)keyboardWasShown:(NSNotification*)aNotification {
  NSDictionary* info = [aNotification userInfo];
  CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
  UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
  self.contentInset = contentInsets;
  self.scrollIndicatorInsets = contentInsets;
  self.scrollEnabled = YES;
}

- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
  [self restoreScrolViewInsets];
}

- (void)restoreScrolViewInsets {
  UIEdgeInsets contentInsets = UIEdgeInsetsZero;
  [UIView animateWithDuration:.25f
                   animations:^{
                     self.contentInset = contentInsets;
                     self.scrollIndicatorInsets = contentInsets;
                   }];
}

@end