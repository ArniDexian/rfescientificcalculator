//
//  Formatter.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 25.05.16.
//  Copyright © 2016 Boolbalabs LLC. All rights reserved.
//

#import "Formatter.h"

static const int kDecimalPrecise = 4;
static const int kPercentPrecise = 3;

@implementation Formatter

+ (instancetype)shared {
  static id _inst = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _inst = [self new];
  });
  return _inst;
}

- (instancetype)init {
    self = [super init];
    _decimalFormatter = [[NSNumberFormatter alloc] init];
    _decimalFormatter.numberStyle = kCFNumberFormatterDecimalStyle;
    _decimalFormatter.minimumFractionDigits = 0;
    _decimalFormatter.maximumFractionDigits = kDecimalPrecise;
    
    _percentFormatter = [[NSNumberFormatter alloc] init];
    _percentFormatter.numberStyle = kCFNumberFormatterScientificStyle;
    _percentFormatter.minimumFractionDigits = 0;
    _percentFormatter.maximumFractionDigits = kPercentPrecise;
    return self;
}

- (NSString*)stringFromDecimal:(NSDecimalNumber*)decimal {
    return [self.decimalFormatter stringFromNumber:decimal];
}

- (NSString*)stringPercentFromDecimal:(NSDecimalNumber*)decimal {
    return [self.percentFormatter stringFromNumber:decimal];
}

@end
