//
//  UIBarButtonItem+ImageButton.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 25/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "UIBarButtonItem+ImageButton.h"

@implementation UIBarButtonItem (ImageButton)

+ (instancetype)barButtonWithImage:(UIImage*)image target:(id)target selector:(SEL)action {
  UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
  button.size = image.size;
  [button setImage:image forState:UIControlStateNormal];
  [button addTarget:target action:action forControlEvents:UIControlEventTouchDown];
  UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:button];
  return item;
}

@end
