//
//  UIBarButtonItem+ImageButton.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 25/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (ImageButton)

+ (instancetype)barButtonWithImage:(UIImage*)image target:(id)target selector:(SEL)action;

@end
