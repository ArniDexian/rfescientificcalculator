//
//  DataCalculator.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 10.04.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "DataCalculator.h"

@implementation DataCalculator

+(BOOL)validateMolecule:(id)molecule {
  if(molecule == nil) return NO;
  else return YES;
}

+(NSArray*)simplifyMolecule:(id)molecule {
  NSArray* result = nil;
  if([molecule isKindOfClass:[MoleculeContainer class]]){
    result = [NSArray arrayWithObject:molecule];
  }else if([molecule isKindOfClass:[NSArray class]]){
    result = molecule;
  }
  return result;
}

+(double)molecularMassOfMolecule:(id)molecule {
  if(![self validateMolecule:molecule])return 0;
  
  NSArray* molec = [self simplifyMolecule:molecule];
  double mass = .0;
  for (int i = 0; i < molec.count; ++i) {
    MoleculeContainer* cont = [molec objectAtIndex:i];
    mass += cont.quantity * [(ChemicalElement*)cont.element atomicMass];
  }
  return mass;
}

+(NSArray*)unitWeightOfAtomsInMolecule:(id)molecule {
  if(![self validateMolecule:molecule])return nil;
  
  double molecMass = [self molecularMassOfMolecule:molecule];
  if(molecMass == 0) return nil;
  
  NSArray* molec = [self simplifyMolecule:molecule];
  NSMutableArray* unitWeights = [NSMutableArray arrayWithCapacity:molec.count];
  for (int  i = 0; i < molec.count; ++i) {
    MoleculeContainer* cont = [molec objectAtIndex:i];
    double unitW = cont.quantity * [(ChemicalElement*)cont.element atomicMass] / molecMass;
    NSNumber* unitWNum = [NSNumber numberWithDouble:unitW];
    [unitWeights addObject:unitWNum];
  }
  return unitWeights;
}

/*+(double)absorptanceOfSimpleMolecule:(MoleculeContainer*)molec
 {
 PeriodicTable* periodicTable = [PeriodicTable sharedInstance];
 AbsorptanceTable* absTable = [AbsorptanceTable sharedInstance];
 
 int absorberIndex = [(ChemicalElement*)molec.element index];
 int sourceIndex = [periodicTable elementWithEnergy:self.energy];
 
 double abs = [absTable absorptanceForSource:sourceIndex andAbsorber:absorberIndex];
 return abs;
 }*/

+(double)absorptanceOfMolecule:(id)molecule {
  if(![self validateMolecule:molecule])return 0;
  
  NSArray* molecules = [self simplifyMolecule:molecule];
  if(!molecules.count) return 0;
  
  NSArray* unitWeights = [self unitWeightOfAtomsInMolecule:molecule];
  NSMutableArray* absorptances = [NSMutableArray arrayWithCapacity:molecules.count];
  for (MoleculeContainer* molec in molecules) {
    double iAbs = [self absorptanceOfMolecule:molec];
    [absorptances addObject:[NSNumber numberWithDouble:iAbs]];
  }
  
  double sum = .0;
  for (int i = 0; i < absorptances.count; ++i) {
    double abs = [absorptances[i] doubleValue];
    double unitWeight = [unitWeights[i] doubleValue];
    sum += abs*unitWeight;
  }
  return sum;
}



@end






















