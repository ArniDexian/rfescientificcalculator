//
//  BarMaterial.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 01.10.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "BarMaterial.h"

@implementation BarMaterial

+(id)barWithMaterial:(MoleculeContainer *)material andThickness:(double)thickness
{
    BarMaterial* bar = [[[self class] alloc] initWithMaterial:material andThickness:thickness];
    return bar;
}

-(id)initWithMaterial:(MoleculeContainer *)material andThickness:(double)thickness
{
    if(self = [super init]){
        _material = material;
        _thickness = thickness;
    }
    return self;
}

@end
