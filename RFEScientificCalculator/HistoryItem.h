//
//  HistoryItem.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 22/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HistoryItem : NSObject <NSCoding>

@property (nonatomic, strong) UIImage*            formulaImage;
@property (nonatomic, strong) NSString*           stringFormula;
@property (nonatomic, strong) NSDecimalNumber*    density;
@property (nonatomic, strong) MoleculeContainer*  molecule;
@property (nonatomic, strong) NSDate*             lastInput;

+ (instancetype)historyItemWithMolecule:(MoleculeContainer*)molecule density:(NSDecimalNumber*)density formulaImage:(UIImage*)image;

@end
