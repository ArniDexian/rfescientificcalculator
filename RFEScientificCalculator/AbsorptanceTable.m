//
//  AbsorptanceTable.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 10.04.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "AbsorptanceTable.h"
#import "LocalStorageProvider.h"

static const CGSize TableStartIndex = {10, 1};
static const CGSize AbsTableSize = {83, 94};

@implementation AbsorptanceTable

+(AbsorptanceTable *)sharedInstance {
  static AbsorptanceTable *sharedSingleton = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedSingleton = [[AbsorptanceTable alloc] init];
  });
  return sharedSingleton;
}

-(id)init {
  self = [super init];
  if(self) {
    [self initFromFile];
  }
  return self;
}

-(void)initFromFile {
  [LocalStorageProvider requestAbsorptanceWithComplete:^(NSArray *elements) {
    [self popTableByArray:elements];
  } error:^(NSError *error) {
    NSLog(@"error getting absorptances");
  }];
}

-(void)popTableByArray:(NSArray*)rowData {
  self.data = rowData;
}

- (NSDecimalNumber*)absorptanceForSource:(int)sourceInd andAbsorber:(int)absorberInd {
    if(sourceInd < TableStartIndex.width || absorberInd < TableStartIndex.height) {
        return nil;
    }
    
    int col = sourceInd - TableStartIndex.width;
    int row = absorberInd - TableStartIndex.height;
    
    NSDecimalNumber* abs = nil;
    if (col < AbsTableSize.width && row < AbsTableSize.height) {
        NSArray* colArray = self.data[row];
        abs = colArray[col];
    }
    
    return abs;
}

@end
