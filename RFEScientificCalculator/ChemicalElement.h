//
//  ChemicalElement.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 24.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    UndefinedChemGroup,
    MainChemGroup,
    LanthanideChemGroup,
    ActinideChemGroup
} GroupType;

@interface ChemicalElement : NSObject <NSCoding, NSCopying>

+ (id)elementWithJSONDic:(NSDictionary*)dic;

@property (nonatomic, strong) NSString* symbol;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* latName;
@property (nonatomic) int index;
@property (nonatomic) float atomicMass;

@property (nonatomic) int period;
@property (nonatomic) int group;

@property (nonatomic) float energy;
@property (nonatomic) float density;

//precise calculation
@property (nonatomic, strong) NSDecimalNumber* decAtomicMass;
@property (nonatomic, strong) NSDecimalNumber* decEnergy;
@property (nonatomic, strong) NSDecimalNumber* decDensity;

///position is (element group:1-18, period 1-7) for main chem group
///and location for lanthanide and actinide chem groups
@property (nonatomic) CGPoint position;
@property (nonatomic, strong) UIColor* color;
@property (nonatomic) GroupType el_group;

- (NSString*)nameStartingWith:(NSString*)startString;

@end
