//
//  AppUtils.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 24.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UIView+Frame.h"
#import "AppDefaults.h"
#import <QuartzCore/QuartzCore.h>
#import "Macroses.h"

@interface KeyValuePair : NSObject

@property (nonatomic, strong) id key;
@property (nonatomic, strong) id value;
+(KeyValuePair*)key:(id)key value:(id)value;
@end


@interface AppUtils : NSObject

///Get list of params from url stored in string
+(NSMutableArray*)URLParams:(NSString *)url;
///combine url with given params list for base url
+(NSString*)urlWithParams:(NSMutableArray*)params forBaseUrl:(NSString*)baseUrl;
///get concatenated string of urls from dictionry without ordering
+(NSString*)paramWithDict:(NSDictionary*)params;
///search for specified value of key directly from url string
+(NSString *)valueOfURL:(NSString *)url forKey:(NSString*)key;
///unescape string
+(NSString*)encodeURL:(NSString*)escapedStr;
///escape string
+(NSString*)decodeURL:(NSString*)unescapeStr;
///the device is tablet
+(BOOL)isIPad;
///mobile
+(BOOL)isIPhone;
///adjust label size by content
+(void)adjustLabelWidth:(UILabel*)lbl withMargin:(int)marg;
+(void)adjustLabelWidth:(UILabel*)lbl constrainedToSize:(CGSize)size withMargin:(int)marg;
///adjust button width by it's content title. Margin from left & right, minWidth - set button width no less than the value
+(void)adjustButtonWidth:(UIButton *)button withMargin:(int)marg minWidth:(int)minButtonWidth;
///get current orientation
+(UIInterfaceOrientation)orientation;
///there and below - get sizes of frames for current device
+(CGSize)landscapeFrameSize;
+(CGSize)portraitFrameSize;
+(CGSize)landscapeFrameSizeWithBar;
+(CGSize)portraitFrameSizeWithBar;
+(CGSize)currentFrameSize;
+(CGSize)currentFrameSizeWithBar;
+(CGRect)landscapeBounds;
+(CGRect)portraitBounds;
///center segment control elements and set spec font
+(void)adjustSegmControl:(UISegmentedControl*)segmentedControl withFont:(UIFont*)font;
///check if the aspect ratio of device is 4 to 3
+(BOOL)aspectIs4x3;

///address book - not works
+(NSString*)getUserEmail;
+(BOOL)isVal:(Float64)val1 apprEqualTo:(Float64)val2;
+(NSString *)base64EncodingWithLineLength:(NSData*) data;
+(void)stopTimer:(NSTimer*)timer;

+(void)openUrlInBrowser:(NSURL*)url;
+ (NSString*)md5HexDigest:(NSString*)input;

+ (BOOL)validateEmailWithString:(NSString*)email;
+ (UIColor *)colorFromHexString:(NSString *)hexString;

+(BOOL)isFileVideo:(NSString*)path;
+(BOOL)isFileImage:(NSString*)path;

+(CAGradientLayer*)gradientWithTopColor:(UIColor *)topColor andBottomColot:(UIColor *)bottomColor forBounds:(CGRect)bounds andCornerRadius:(CGFloat)radius;
+(CAGradientLayer*)gradientWithLeftColor:(UIColor *)leftColor andRightColot:(UIColor *)rightColor forBounds:(CGRect)bounds andCornerRadius:(CGFloat)radius;
+(void)setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners withRadius:(CGFloat)radius;

+(UIView*)barButtonView:(UIBarButtonItem*)item;
+(void)setBorderForSubviews:(UIView*)view;

@end
