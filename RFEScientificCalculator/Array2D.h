//
//  Array2D.h
//  RFE Puzzle
//
//  Created by Arni-MacBook on 02.12.12.
//  Copyright (c) 2012 Archepix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Array2D : NSObject

-(id)initWithCols:(int)col andRows:(int)rows;
-(id)initWithSize:(CGSize)size;

-(CGSize)dim;
-(int)cols;
-(int)rows;

-(void)putObject:(NSObject*)obj atX:(int)x andY:(int)y;
-(NSObject*)getObjectAtX:(int)x andY:(int)y;

-(void)putObject:(NSObject*)obj atCol:(int)col andRow:(int)row;
-(NSObject*)getObjectAtCol:(int)col andRow:(int)row;

-(void)swapObjAt:(CGPoint)obj1 withAt:(CGPoint)obj2;
-(id)copy;
@end
