//
//  LocalStorageProvider.h
//  RFE
//
//  Created by Arni-MacBook on 10.02.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppUtils.h"

typedef void (^ResultHandler)(NSArray* elements);
typedef void (^ErrorHandler)(NSError* error);

@interface LocalStorageProvider : NSObject

+ (void)requestElementsWithComplete:(ResultHandler)complete error:(ErrorHandler)errHandler;
+ (void)requestAbsorptanceWithComplete:(ResultHandler)complete error:(ErrorHandler)errHandler;

@end