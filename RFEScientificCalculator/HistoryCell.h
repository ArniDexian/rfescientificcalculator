//
//  HistoryCell.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 22/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryCell : UITableViewCell

+ (CGFloat)cellHeight;

@property (weak, nonatomic) IBOutlet UIImageView *formulaImage;

@end
