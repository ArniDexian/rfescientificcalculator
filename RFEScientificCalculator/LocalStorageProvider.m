//
//  LocalStorageProvider.m
//  RFE
//
//  Created by Arni-MacBook on 10.02.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "LocalStorageProvider.h"
#import "ChemicalElement.h"
#import "JSON.h"

@implementation LocalStorageProvider

+(void)requestElementsWithComplete:(ResultHandler)complete error:(ErrorHandler)errHandler {
  dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
  dispatch_async(queue, ^ {
    NSString* path = [[NSBundle mainBundle] pathForResource:@"ElementsList" ofType:@"json"];
    NSError* err = nil;
    NSString* json = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&err];
    NSArray* elements = [json JSONValue];
    if([elements isKindOfClass:[NSArray class]]){
      NSMutableArray* ready = [NSMutableArray array];
      for (int i = 0; i< elements.count; ++i) {
        NSDictionary* dic = elements[i];
        ChemicalElement* ch = [ChemicalElement elementWithJSONDic:dic];
        [ready addObject:ch];
      }
      NSLog(@"elements count: %lu", (unsigned long)ready.count);
      dispatch_async(dispatch_get_main_queue(), ^ {
        if(complete)
          complete(ready);
      });
    }else{
      dispatch_async(dispatch_get_main_queue(), ^ {
        if(errHandler)
          errHandler(nil);
      });
    }
  });
}

+(void)requestAbsorptanceWithComplete:(ResultHandler)complete error:(ErrorHandler)errHandler {
  dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
  dispatch_async(queue, ^ {
    NSString* path = [[NSBundle mainBundle] pathForResource:@"AbsData" ofType:@"csv"];
    NSError* err = nil;
    NSString* dataStr = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&err];
    
    NSArray *contentArray = [dataStr componentsSeparatedByString:@"\r"];
    NSMutableArray* resultArray = [NSMutableArray arrayWithCapacity:contentArray.count];
    
    NSLocale* locale = [NSLocale localeWithLocaleIdentifier:@"EN"];

    for (NSString *item in contentArray) {
      NSArray *itemArray = [item componentsSeparatedByString:@";"];
      NSMutableArray* rowData = [NSMutableArray arrayWithCapacity:itemArray.count];
      for (NSString* str in itemArray) {
        NSDecimalNumber* number = [NSDecimalNumber decimalNumberWithString:str locale:locale];
        if(number) {
          [rowData addObject:number];
        } else {
          [rowData addObject:[NSDecimalNumber decimalNumberWithString:@"0"]];
          NSLog(@"wasn't parsed: %@", str);
        }

      }
      [resultArray addObject:rowData];
    }
    
    NSLog(@"absorptances count: %lu", (unsigned long)resultArray.count);
    dispatch_async(dispatch_get_main_queue(), ^ {
      if(complete)
        complete(resultArray);
    });
  });
}

@end















