//
//  UIImage+Colorize.h
//  FindTheWay
//
//  Created by ArniDexian on 17.11.13.
//  Copyright (c) 2013 Arni-MacBook. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Colorize)

+ (UIImage*)imageWithColor:(UIColor *)color;

- (UIImage *)imageWithTint:(UIColor *)tintColor;

@end
