//
//  HistoryDataSource.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 22/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "HistoryDataSource.h"
#import "HistoryCell.h"

static NSString* const kHistoryKey = @"kHistoryKey";

static NSMutableArray* _data;

@implementation HistoryDataSource

+ (instancetype)historyDataSourceWithTable:(UITableView*)tableView {
  HistoryDataSource* dataSource = [HistoryDataSource new];
  tableView.delegate = dataSource;
  tableView.dataSource = dataSource;
  return dataSource;
}

- (void)setData:(NSMutableArray *)data {
  _data = data;
}

- (NSMutableArray*)data {
  return _data;
}

+ (void)initialize {
  _data = [[NSKeyedUnarchiver unarchiveObjectWithData:[[NSUserDefaults standardUserDefaults] dataForKey:kHistoryKey]] mutableCopy];
  if (_data == nil) {
    _data = [NSMutableArray array];
  }
}

- (instancetype)init
{
  self = [super init];
  if (self) {
    self.numberOfItemsToShow = 4;
  }
  return self;
}

- (NSInteger)numberOfRows {
  return [self isShowMoreButtonVisible] ? self.numberOfItemsToShow + 1 : [self.data count];
}

- (BOOL)isShowMoreButtonVisible {
  return self.showMoreButton && self.data.count > self.numberOfItemsToShow;
}

- (BOOL)isMoreButtonAtIndexPath:(NSIndexPath*)indexPath {
  return [self isShowMoreButtonVisible] && [self numberOfRows] - 1 == indexPath.row;
}

#pragma mark - UITableView data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [self numberOfRows];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  if ([self isMoreButtonAtIndexPath:indexPath]) {
    static NSString* reusableIdentifier = @"ShowMoreIdentifier";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
    if (cell == nil) {
      cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableIdentifier];
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.textLabel.text = self.showMoreButtonText;
    cell.textLabel.textAlignment = NSTextAlignmentRight;
    cell.backgroundColor = [UIColor clearColor];
    cell.opaque = NO;
    cell.textLabel.textColor =[UIColor whiteColor];
    cell.selectedBackgroundView = [UIView new];
    cell.selectedBackgroundView.backgroundColor = RGBA(0.f, 0.f, 0.f, 0.1f);
    return cell;
  } else {
    static NSString* reusableIdentifier = @"HistoryCellIdentifier";
    HistoryCell* historyCell = [tableView dequeueReusableCellWithIdentifier:reusableIdentifier
                                                        forIndexPath:indexPath];
    historyCell.formulaImage.image = [self.data[indexPath.row] formulaImage];
    historyCell.selectedBackgroundView = [UIView new];
    historyCell.selectedBackgroundView.backgroundColor = RGBA(0.f, 0.f, 0.f, 0.1f);
    return historyCell;
  }
}

#pragma mark - UITableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:YES];
  if ([self isMoreButtonAtIndexPath:indexPath]) {
    [self.delegate historyDidShowMorePressed];
  } else {
    HistoryItem* item = self.data[indexPath.row];
    [self.delegate historyDidSelectItem:item];
    item.lastInput = [NSDate date];
    [self sortData];
  }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  return [self isMoreButtonAtIndexPath:indexPath] ? 40.f : [HistoryCell cellHeight];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
  return [self isMoreButtonAtIndexPath:indexPath] ? NO : YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
  return [self isMoreButtonAtIndexPath:indexPath] ? UITableViewCellEditingStyleNone : UITableViewCellEditingStyleDelete;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
  if (editingStyle == UITableViewCellEditingStyleDelete) {
    [self.data removeObjectAtIndex:indexPath.row];
    [self saveHistory];
    if (![self isShowMoreButtonVisible]) {
      [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    [tableView reloadData];
  }
}

#pragma mark - logic

- (void)addItem:(HistoryItem *)item {
  if (item) {
    NSUInteger prev = [self.data indexOfObject:item];
    if (prev != NSNotFound) {
      HistoryItem* old = [self.data objectAtIndex:prev];
      old.formulaImage = item.formulaImage;
      old.lastInput = [NSDate date];
      old.density = item.density;
    } else {
      item.lastInput = [NSDate date];
      [self.data addObject:item];
    }
    [self.delegate historyDataSourceChanged];
    [self sortData];
  }
}

- (void)sortData {
  [self.data sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"lastInput" ascending:NO]]];
  [self saveHistory];
}

- (void)saveHistory {
  NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
  [defaults setObject:[NSKeyedArchiver archivedDataWithRootObject:self.data] forKey:kHistoryKey];
  [defaults synchronize];
}



@end
