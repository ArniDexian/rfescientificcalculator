//
//  HistoryDataSource.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 22/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HistoryDataSourceDelegate <NSObject>
@required
- (void)historyDataSourceChanged;
- (void)historyDidSelectItem:(HistoryItem*)item;
- (void)historyDidShowMorePressed;

@end


@interface HistoryDataSource : NSObject <UITableViewDataSource, UITableViewDelegate> 

+ (instancetype)historyDataSourceWithTable:(UITableView*)tableView;

@property (nonatomic, weak) id<HistoryDataSourceDelegate> delegate;

@property (nonatomic) NSMutableArray* data;
@property (nonatomic) NSUInteger numberOfItemsToShow;
@property (nonatomic) BOOL showMoreButton;
@property (nonatomic) NSString* showMoreButtonText;

- (void)addItem:(HistoryItem*)item;

@end
