//
//  UIView+Frame.m
//  RFE
//
//  Created by Arni-MacBook on 11.02.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "UIView+Frame.h"

@implementation UIView (Frame)

-(int)leftBound
{
    return self.frame.origin.x;
}

-(void)setLeftBound:(int)leftBound
{
    CGRect fr = self.frame;
    fr.origin.x = leftBound;
    [self setFrame:fr];
}

-(int)rightBound
{
    return self.frame.origin.x + self.width;
}

-(void)setRightBound:(int)rightBound
{
    CGRect frame = self.frame;
    frame.origin.x = rightBound - frame.size.width;
    self.frame = frame;
}

-(int)topBound
{
    return self.frame.origin.y;
}

-(void)setTopBound:(int)__topBound
{
    CGRect frame = self.frame;
    frame.origin.y = __topBound;
    self.frame = frame;
}

-(int)bottomBound
{
    return self.frame.origin.y + self.height;
}

-(void)setBottomBound:(int)__bottomBound
{
    CGRect frame = self.frame;
    frame.origin.y = __bottomBound - frame.size.height;
    self.frame = frame;
}

-(int)x
{
    return self.leftBound;
}

-(void)setX:(int)x
{
    [self setLeftBound:x];
}

-(int)y
{
    return self.topBound;
}

-(void)setY:(int)y
{
    [self setTopBound:y];
}

-(int)width
{
    return self.frame.size.width;
}

-(void)setWidth:(int)width
{
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}

-(int)height
{
    return self.frame.size.height;
}

-(void)setHeight:(int)height
{
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}

-(CGPoint)position
{
    return self.frame.origin;
}

-(void)setPosition:(CGPoint)pos
{
    CGRect cur = self.frame;
    cur.origin = pos;
    self.frame = cur;
}

-(CGSize)size
{
    return self.frame.size;
}

-(void)setSize:(CGSize)size
{
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}


@end


















