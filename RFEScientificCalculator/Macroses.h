//
//  Macroses.h
//  RFE
//
//  Created by Arni-MacBook on 04.05.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#ifndef Tvigle_Mobile_Macroses_h
#define Tvigle_Mobile_Macroses_h

#define ApplicationDelegate                 ((AppDelegate *)[[UIApplication sharedApplication] delegate])
#define UserDefaults                        [NSUserDefaults standardUserDefaults]
#define SharedApplication                   [UIApplication sharedApplication]
#define Bundle                              [NSBundle mainBundle]
#define MainScreen                          [UIScreen mainScreen]
#define ShowNetworkActivityIndicator()      [UIApplication sharedApplication].networkActivityIndicatorVisible = YES
#define HideNetworkActivityIndicator()      [UIApplication sharedApplication].networkActivityIndicatorVisible = NO
#define NetworkActivityIndicatorVisible(x)  [UIApplication sharedApplication].networkActivityIndicatorVisible = x
#define IsNetworkActivityIndicatorVisible   [UIApplication sharedApplication].networkActivityIndicatorVisible
#define NavBar                              self.navigationController.navigationBar
#define TabBar                              self.tabBarController.tabBar
#define NavBarHeight                        self.navigationController.navigationBar.bounds.size.height
#define TabBarHeight                        self.tabBarController.tabBar.bounds.size.height
#define ScreenWidth                         [[UIScreen mainScreen] bounds].size.width
#define ScreenHeight                        [[UIScreen mainScreen] bounds].size.height
#define DATE_COMPONENTS                     NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
#define TIME_COMPONENTS                     NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit
#define RGB(r, g, b)                        [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1.0f]
#define RGBF(r, g, b)                       [UIColor colorWithRed:(r) green:(g) blue:(b) alpha:1.0f]
#define RANDCOLOR()                         [UIColor colorWithRed:(float)(arc4random()%255) / 255.0f  green:(float)(arc4random()%255) / 255.0f  blue:(float)(arc4random()%255) / 255.0f alpha:1]
#define RGBA(r, g, b, a)                    [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:(a)]
#define SPRINTF(format, args...)            [NSString stringWithFormat:(format), args]
#define URL(str)                            [NSURL URLWithString:str]

#define round_div(a, b)                     ((a) + (b) - 1) / (b)

#define IsDevicePad                         /*NO*/[UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad
#define IsDevicePhone                       /*YES*/[UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone

#ifdef NDEBUG
// do nothing
#define TBLog(...)
#else
#define TBLog NSLog
#endif

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define IS_OS_6_OR_HIGHER         ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0)
#define IS_OS_7_OR_HIGHER         ([[UIDevice currentDevice].systemVersion floatValue] >= 7.0)

#define safeValue(a)                    a == nil ? [NSNull null] : a
#define safeDecimal(a)                    a == nil ? [NSDecimalNumber zero] : a
#endif
