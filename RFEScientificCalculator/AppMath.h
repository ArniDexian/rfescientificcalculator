//
//  AppMath.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 17.03.16.
//  Copyright © 2016 Boolbalabs LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NSDecimalNumber* DNum;

NSDecimalNumber* decFromInt(int intNum);
NSDecimalNumber* decMult(NSDecimalNumber* d1, NSDecimalNumber* d2);
NSDecimalNumber* decMultInt(NSDecimalNumber* d1, int num1);
NSDecimalNumber* decDiv(NSDecimalNumber* d1, NSDecimalNumber* d2);
NSDecimalNumber* decSum(NSDecimalNumber* d1, NSDecimalNumber* d2);
NSDecimalNumber* decDif(NSDecimalNumber* d1, NSDecimalNumber* d2);
NSDecimalNumber* decPow(NSDecimalNumber* d1, NSUInteger pow);
BOOL decEqual(NSDecimalNumber* d1, NSDecimalNumber* d2);
BOOL decLess(NSDecimalNumber* d1, NSDecimalNumber* d2);
BOOL decGreater(NSDecimalNumber* d1, NSDecimalNumber* d2);
BOOL decLessOrEqual(NSDecimalNumber* d1, NSDecimalNumber* d2);
BOOL decGreaterOrEqual(NSDecimalNumber* d1, NSDecimalNumber* d2);

BOOL decIsZero(NSDecimalNumber* d1);
BOOL decIsNan(NSDecimalNumber* d1);
BOOL decValid(NSDecimalNumber* d1);

NSDecimalNumber* decAbs(NSDecimalNumber* d1);

@interface AppMath : NSObject

+ (double)deltaFromDouble:(double)number;
+ (NSDecimalNumber*)deltaDecimalNumberFromDouble:(double)number;

@end
