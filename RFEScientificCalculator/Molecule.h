//
//  Molecule.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 26.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChemicalElement.h"

//used to store atom with it quantity
@interface MoleculeContainer : NSObject <NSCopying, NSCoding>

+ (id)emptyMolecule;
+ (id)moleculeWithArray:(NSArray*)array;
+ (id)moleculeWithElement:(id)element andQuantity:(int)quantity;

@property (nonatomic, strong) id element;
@property (nonatomic, assign) int quantity;


@end
