//
//  UIView+Frame.h
//  RFE
//
//  Created by Arni-MacBook on 11.02.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Frame)

@property (nonatomic) int leftBound;
@property (nonatomic) int rightBound;
@property (nonatomic) int height;
@property (nonatomic) int width;
@property (nonatomic) int bottomBound;
@property (nonatomic) int topBound;
@property (nonatomic) int x;
@property (nonatomic) int y;
@property (nonatomic) CGPoint position;
@property (nonatomic) CGSize size;

@end