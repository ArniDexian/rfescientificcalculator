//
//  PeriodicTableView.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 24.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ElementView.h"
#import "AppUtils.h"

@class PeriodicTableView;

@protocol PeriodicTableViewDelegate <UIScrollViewDelegate>

-(void)periodicTable:(PeriodicTableView*)table elementWasSelected:(ChemicalElement*)element;
@optional
-(void)periodicTableFrameWasChanged:(PeriodicTableView *)table;

@end


@interface PeriodicTableView : UIScrollView

@property (assign, nonatomic) id<PeriodicTableViewDelegate> delegate;
@property (nonatomic) BOOL disableChangingFrameSize;

-(void)swapRotationAnimated:(BOOL)animated;
-(void)rotateHorizontalAnimated:(BOOL)animated;
-(void)rotateVerticalAnimated:(BOOL)animated;
-(void)rotateToOrientation:(UIInterfaceOrientation)toOrient;

@end
