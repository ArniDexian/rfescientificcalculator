//
//  MoleculeDisplayView.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 26.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Molecule.h"

typedef enum{
  CharTypeOther,
  CharTypeSymbol,
  CharTypeIndex,
  CharTypeLeftBracket,
  CharTypeRightBracket
} CharType;

/**
 *Just display molecule
 */
@class MoleculeDisplayView;

@protocol MoleculeDisplayViewDelegate <UIScrollViewDelegate>

-(void)moleculeDisplayDidBeginEditing:(MoleculeDisplayView*)disaplay;

@end

@interface MoleculeDisplayView : UIScrollView

@property (nonatomic) NSMutableArray* molecules;
@property (nonatomic) MoleculeContainer* molecule;
@property (nonatomic) UIColor* textColor;
@property (nonatomic) int fontSize;
@property (nonatomic) BOOL selected;

@property (nonatomic, assign) id<MoleculeDisplayViewDelegate> delegate;

- (void)addMolecule:(MoleculeContainer*)molecule;
- (void)removeLast;
- (void)removeMolecule:(MoleculeContainer*)cont;
- (void)removeAll;
- (BOOL)containMolecule:(MoleculeContainer*)cont;


- (UIImage*)formulaImage;

@end
