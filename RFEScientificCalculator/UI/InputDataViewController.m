//
//  InputDataViewController.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 10.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "InputDataViewController.h"
#import "InputDataSourceDelegate.h"
#import "ResultsViewController.h"

@interface InputDataViewController () <UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic) InputDataSourceDelegate* dataSource;

@end


@implementation InputDataViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

  self.dataSource = [InputDataSourceDelegate dataSourceForTableView:self.tableView];
  
  [self.tableView registerForKeyboardNotifications];
}

- (void)dealloc {
  [self.tableView unregisterFromKeyboardNotifications];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - tableView delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.section == InputDataSectionMolecule && indexPath.row < self.dataSource.molecules.count) {
    return 94.f;
  } else {
    return 44.f;
  }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
  if (indexPath.section == InputDataSectionMolecule && indexPath.row == self.dataSource.molecules.count) {
    [self.dataSource addEmptyMolecule];
  } else if (indexPath.section == InputDataSectionCalculateButton) {
    [self openResultsViewController];
  }
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath*)indexPath {
  [self.dataSource saveValuesFromCell:cell atIndexPath:indexPath];
}

#pragma mark - data


#pragma mark - transitions
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
  
}

- (void)openResultsViewController {
  ResultsViewController* resultsViewController = [[ResultsViewController alloc] init];
  resultsViewController.substances = [self.dataSource obtainBarMaterials];
  resultsViewController.energy = [[self.dataSource energy] floatValue];
  resultsViewController.density = [[self.dataSource density] floatValue];
  [self.tableView endEditing:YES];
  [self.navigationController pushViewController:resultsViewController animated:YES];
}

@end
















