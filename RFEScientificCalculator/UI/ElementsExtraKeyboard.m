//
//  ElementsExtraKeyboard.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 04.10.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "ElementsExtraKeyboard.h"
#import "AppUtils.h"

const int BUTTONS_OFFSET = 5;
#define DIG_BTN_SIZE CGSizeMake(24, 36)

@interface ElementsExtraKeyboard ()

@property (nonatomic) UIButton* dig0;
@property (nonatomic) UIButton* dig1;
@property (nonatomic) UIButton* dig2;
@property (nonatomic) UIButton* dig3;
@property (nonatomic) UIButton* dig4;
@property (nonatomic) UIButton* dig5;
@property (nonatomic) UIButton* dig6;
@property (nonatomic) UIButton* dig7;
@property (nonatomic) UIButton* dig8;
@property (nonatomic) UIButton* dig9;
@property (nonatomic) UIButton* leftBracket;
@property (nonatomic) UIButton* rightBracket;
@property (nonatomic) UIButton* removeBtn;
@property (nonatomic) UIButton* rotateBtn;
@property (nonatomic) UIButton* hideBtn;

@end

@implementation ElementsExtraKeyboard

-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initButtons];
    }
    return self;
}

-(void)initButtons
{
    self.dig0 = [self createButtonWithText:@"0"];
    [self setPositionForButton:self.dig0 afterButton:nil];
    [self.dig0 addTarget:self action:@selector(dig0Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig1 = [self createButtonWithText:@"1"];
    [self setPositionForButton:self.dig1 afterButton:self.dig0];
    [self.dig1 addTarget:self action:@selector(dig1Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig2 = [self createButtonWithText:@"2"];
    [self setPositionForButton:self.dig2 afterButton:self.dig1];
    [self.dig2 addTarget:self action:@selector(dig2Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig3 = [self createButtonWithText:@"3"];
    [self setPositionForButton:self.dig3 afterButton:self.dig2];
    [self.dig3 addTarget:self action:@selector(dig3Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig4 = [self createButtonWithText:@"4"];
    [self setPositionForButton:self.dig4 afterButton:self.dig3];
    [self.dig4 addTarget:self action:@selector(dig4Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig5 = [self createButtonWithText:@"5"];
    [self setPositionForButton:self.dig5 afterButton:self.dig4];
    [self.dig5 addTarget:self action:@selector(dig5Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig6 = [self createButtonWithText:@"6"];
    [self setPositionForButton:self.dig6 afterButton:self.dig5];
    [self.dig6 addTarget:self action:@selector(dig6Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig7 = [self createButtonWithText:@"7"];
    [self setPositionForButton:self.dig7 afterButton:self.dig6];
    [self.dig7 addTarget:self action:@selector(dig7Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig8 = [self createButtonWithText:@"8"];
    [self setPositionForButton:self.dig8 afterButton:self.dig7];
    [self.dig8 addTarget:self action:@selector(dig8Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig9 = [self createButtonWithText:@"9"];
    [self setPositionForButton:self.dig9 afterButton:self.dig8];
    [self.dig9 addTarget:self action:@selector(dig9Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.leftBracket = [self createButtonWithText:@"["];
    [self setPositionForButton:self.leftBracket afterButton:self.dig9];
    [self.leftBracket addTarget:self action:@selector(leftBracketClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.rightBracket = [self createButtonWithText:@"]"];
    [self setPositionForButton:self.rightBracket afterButton:self.leftBracket];
    [self.rightBracket addTarget:self action:@selector(rightBracketClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.removeBtn = [self createButtonWithText:@"<-"];
    self.removeBtn.width = 60;
    [self setPositionForButton:self.removeBtn afterButton:self.rightBracket];
    self.removeBtn.rightBound = self.rightBound;
    [self.removeBtn addTarget:self action:@selector(removeClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if([AppUtils isIPhone]){
        self.rotateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.rotateBtn.size = CGSizeMake(DIG_BTN_SIZE.height, DIG_BTN_SIZE.height);
        self.rotateBtn.rightBound = self.removeBtn.leftBound - BUTTONS_OFFSET;
        self.rotateBtn.topBound = self.removeBtn.topBound;
        [self.rotateBtn setBackgroundImage:[[UIImage imageNamed:@"rotate_btn_sel"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
        [self.rotateBtn setBackgroundImage:[[UIImage imageNamed:@"rotate_btn"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
        [self.rotateBtn addTarget:self action:@selector(rotateAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.rotateBtn];
    }
    
    self.hideBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.hideBtn.size = CGSizeMake(DIG_BTN_SIZE.height, DIG_BTN_SIZE.height);
    self.hideBtn.rightBound = ([AppUtils isIPhone] ? self.rotateBtn.leftBound : self.removeBtn.leftBound) - BUTTONS_OFFSET;
    self.hideBtn.topBound = self.removeBtn.topBound;
    [self.hideBtn setBackgroundImage:[[UIImage imageNamed:@"hide_btn_sel"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
    [self.hideBtn setBackgroundImage:[[UIImage imageNamed:@"hide_btn"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
    [self.hideBtn addTarget:self action:@selector(hideAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.hideBtn];
    self.size = CGSizeMake(self.width, self.hideBtn.bottomBound + BUTTONS_OFFSET);// - self.dig0.topBound);
}

-(UIButton*)createButtonWithText:(NSString*)text
{
    UIFont* font = [UIFont boldSystemFontOfSize:20];
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.size = DIG_BTN_SIZE;
    button.titleLabel.font = font;
    [button setTitle:text forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setBackgroundImage:[[UIImage imageNamed:@"dig_bkg_dark"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
    [button setBackgroundImage:[[UIImage imageNamed:@"dig_bkg_dark_sel"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
    [self addSubview:button];
    return button;
}

-(void)setPositionForButton:(UIButton*)btn afterButton:(UIButton*)lastBtn
{
    int width = self.width;
    int oX = !lastBtn ? 5/*DISPLAY_OFFSET*/ : lastBtn.rightBound + BUTTONS_OFFSET;
    int oY = !lastBtn ? BUTTONS_OFFSET : lastBtn.topBound;
    
    if((oX + btn.width + BUTTONS_OFFSET) > width){
        oX = 5/*DISPLAY_OFFSET*/;
        oY = lastBtn.bottomBound + BUTTONS_OFFSET;
    }
    
    btn.position = CGPointMake(oX, oY);
    
}

#pragma mark - actions

-(void)hideAction:(id)sender
{
    if([self.delegate respondsToSelector:@selector(extraKeyboardHidePressed:)]){
        [self.delegate extraKeyboardHidePressed:self];
    }
}

-(void)rotateAction:(id)sender
{
    if([self.delegate respondsToSelector:@selector(extraKeyboardRotatePressed:)]){
        [self.delegate extraKeyboardRotatePressed:self];
    }
}

-(void)dig0Clicked:(id)sender
{
    if([self.delegate respondsToSelector:@selector(extraKeyboard0Pressed:)]){
        [self.delegate extraKeyboard0Pressed:self];
    }
}

-(void)dig1Clicked:(id)sender
{
    if([self.delegate respondsToSelector:@selector(extraKeyboard1Pressed:)]){
        [self.delegate extraKeyboard1Pressed:self];
    }
}

-(void)dig2Clicked:(id)sender
{
    if([self.delegate respondsToSelector:@selector(extraKeyboard2Pressed:)]){
        [self.delegate extraKeyboard2Pressed:self];
    }
}

-(void)dig3Clicked:(id)sender
{
    if([self.delegate respondsToSelector:@selector(extraKeyboard3Pressed:)]){
        [self.delegate extraKeyboard3Pressed:self];
    }
}

-(void)dig4Clicked:(id)sender
{
    if([self.delegate respondsToSelector:@selector(extraKeyboard4Pressed:)]){
        [self.delegate extraKeyboard4Pressed:self];
    }
}

-(void)dig5Clicked:(id)sender
{
    if([self.delegate respondsToSelector:@selector(extraKeyboard5Pressed:)]){
        [self.delegate extraKeyboard5Pressed:self];
    }
}

-(void)dig6Clicked:(id)sender
{
    if([self.delegate respondsToSelector:@selector(extraKeyboard6Pressed:)]){
        [self.delegate extraKeyboard6Pressed:self];
    }
}

-(void)dig7Clicked:(id)sender
{
    if([self.delegate respondsToSelector:@selector(extraKeyboard7Pressed:)]){
        [self.delegate extraKeyboard7Pressed:self];
    }
}

-(void)dig8Clicked:(id)sender
{
    if([self.delegate respondsToSelector:@selector(extraKeyboard8Pressed:)]){
        [self.delegate extraKeyboard8Pressed:self];
    }
}

-(void)dig9Clicked:(id)sender
{
    if([self.delegate respondsToSelector:@selector(extraKeyboard9Pressed:)]){
        [self.delegate extraKeyboard9Pressed:self];
    }
}

-(void)leftBracketClicked:(id)sender
{
    if([self.delegate respondsToSelector:@selector(extraKeyboardLeftBracketPressed:)]){
        [self.delegate extraKeyboardLeftBracketPressed:self];
    }
}

-(void)rightBracketClicked:(id)sender
{
    if([self.delegate respondsToSelector:@selector(extraKeyboardRightBracketPressed:)]){
        [self.delegate extraKeyboardRightBracketPressed:self];
    }
}

-(void)removeClicked:(id)sender
{
    if([self.delegate respondsToSelector:@selector(extraKeyboardRemovePressed:)]){
        [self.delegate extraKeyboardRemovePressed:self];
    }
}

@end

















