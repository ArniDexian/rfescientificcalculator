//
//  EditDataTableViewCell.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 10.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "EditDataTableViewCell.h"

@implementation EditDataTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
  self.textField.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse {
  [super prepareForReuse];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  NSString* resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
  BOOL valid = [resultString length] == 0 || [resultString isStringNumber];
  if (valid) {
    textField.text = resultString;
    [self delegateDidChangeValue:@([resultString doubleValue])];
  }
  return NO;
}

- (void)delegateDidChangeValue:(NSNumber*)value {
  if ([self.delegate respondsToSelector:@selector(editDataTableCell:didChangeValue:)]) {
    [self.delegate editDataTableCell:self didChangeValue:value];
  }
}


@end
