//
//  HelpViewController.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 24/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString* const kHelpSegueIdentifier;

@interface HelpViewController : UIViewController

@end
