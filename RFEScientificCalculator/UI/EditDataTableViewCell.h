//
//  EditDataTableViewCell.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 10.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class EditDataTableViewCell;

@protocol EditDataTableViewCellDelegate <NSObject>

- (void)editDataTableCell:(EditDataTableViewCell*)cell didChangeValue:(NSNumber*)value;

@end

@interface EditDataTableViewCell : UITableViewCell <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *label;

@property (nonatomic, weak) id<EditDataTableViewCellDelegate> delegate;

@end
