//
//  MainViewController.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 24.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoleculeDisplayView.h"
#import "PeriodicTableView.h"

@interface MainViewController : UIViewController<PeriodicTableViewDelegate, MoleculeDisplayViewDelegate, UITextFieldDelegate>

@property (nonatomic) MoleculeDisplayView* moleculeDisplay;

@end
