//
//  PlotView.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 04.04.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "PlotView.h"

NSString* const kXAxisName = @"kXAxisName";
NSString* const kYAxisName = @"kYAxisName";
NSString* const kYAxisValue = @"kYAxisValue";
NSString* const kYErrorValue = @"kYErrorValue";

@interface PlotView (){
  NSArray *plotData;
  CPTPlotSpaceAnnotation *symbolTextAnnotation;
}

@property (nonatomic) CPTGraphHostingView* hostView;
@property (nonatomic) CPTScatterPlot* dataSourcePlot;

@end


@implementation PlotView

#pragma mark - properties

-(CPTGraphHostingView*)hostView {
  if(_hostView == nil){
    _hostView = [[CPTGraphHostingView alloc] initWithFrame:self.bounds];
    _hostView.allowPinchScaling = YES;
    [self addSubview:_hostView];
    
    self.translatesAutoresizingMaskIntoConstraints = NO;
    _hostView.translatesAutoresizingMaskIntoConstraints = NO;

    NSDictionary* views = @{@"view" : _hostView};
    [_hostView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|-(==0)-[view]-(==0)-|" options:NSLayoutFormatAlignAllBaseline metrics:nil views:views]];
    [_hostView.superview addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-(==0)-[view]-(==0)-|" options:NSLayoutFormatAlignAllBaseline metrics:nil views:views]];
 
    [self layoutIfNeeded];
  }
  return _hostView;
}

#pragma mark - init

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    [self initPlot];
  }
  return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder  {
  self = [super initWithCoder:aDecoder];
  if (self) {
    [self initPlot];
  }
  return self;
}

-(void)initPlot {
  [self initGraph];
  [self initDataSourcePlot];
  [self setupPlot];
}

-(void)initGraph {
  CGRect bounds = self.hostView.bounds;
  
  CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:bounds];
  self.hostView.hostedGraph = graph;
  [graph applyTheme:[CPTTheme themeNamed:kCPTPlainBlackTheme]];//kCPTPlainWhiteTheme]];
  
  //remove border
  graph.plotAreaFrame.borderLineStyle = nil;
  graph.paddingLeft = 0;
  graph.paddingRight = 0;
  graph.paddingTop = 0;
  graph.paddingBottom = 0;
  graph.plotAreaFrame.borderWidth = 0;
  graph.plotAreaFrame.cornerRadius = 0;
}

- (void)initDataSourcePlot {
  // Create a plot that uses the data source method
  _dataSourcePlot = [[CPTScatterPlot alloc] init];
  _dataSourcePlot.identifier = @"Data Source Plot";
  
  CPTMutableLineStyle *lineStyle = [_dataSourcePlot.dataLineStyle mutableCopy];
  lineStyle.lineWidth              = 1.0;
  lineStyle.lineColor              = [CPTColor greenColor];
  _dataSourcePlot.dataLineStyle = lineStyle;
  
  _dataSourcePlot.dataSource = self;
  
  // Set plot delegate, to know when symbols have been touched
  // We will display an annotation when a symbol is touched
  _dataSourcePlot.delegate                        = self;
  _dataSourcePlot.plotSymbolMarginForHitDetection = 5.0f;
  
  [self.hostView.hostedGraph addPlot:_dataSourcePlot];
}

#pragma mark - data source control

- (void) reloadData {
  plotData = [self.dataSource dataForPlotView:self];
  [self.hostView.hostedGraph reloadData];
  [self setupAppearanceForData];
}

#pragma mark - Chart behavior

- (void)setupPlot {
  // Setup scatter plot space
  CPTGraph *graph = self.hostView.hostedGraph;
  CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)graph.defaultPlotSpace;
  plotSpace.allowsUserInteraction = YES;
  plotSpace.delegate              = self;
  
  // Grid line styles
  CPTMutableLineStyle *axisLineStyle = [CPTMutableLineStyle lineStyle];
  axisLineStyle.lineWidth = .5;
  axisLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:.5];
  
  CPTMutableLineStyle *majorGridLineStyle = [CPTMutableLineStyle lineStyle];
  majorGridLineStyle.lineWidth = 0.75;
  majorGridLineStyle.lineColor = [[CPTColor colorWithGenericGray:0.2] colorWithAlphaComponent:0.75];
  
  CPTMutableLineStyle *minorGridLineStyle = [CPTMutableLineStyle lineStyle];
  minorGridLineStyle.lineWidth = 0.25;
  minorGridLineStyle.lineColor = [[CPTColor whiteColor] colorWithAlphaComponent:0.1];
  
  CPTMutableLineStyle *redLineStyle = [CPTMutableLineStyle lineStyle];
  redLineStyle.lineWidth = 10.0;
  redLineStyle.lineColor = [[CPTColor redColor] colorWithAlphaComponent:0.5];
  
  // Axes
  // Label x axis with a fixed interval policy
  CPTXYAxisSet *axisSet = (CPTXYAxisSet *)graph.axisSet;
  CPTXYAxis *x = axisSet.xAxis;
  x.axisLineStyle               = axisLineStyle;
  x.majorTickLineStyle          = axisLineStyle;
  x.minorTickLineStyle          = axisLineStyle;
  x.labelingPolicy              = CPTAxisLabelingPolicyAutomatic;
  x.orthogonalCoordinateDecimal = CPTDecimalFromString(@"1.0");
  x.minorTicksPerInterval       = 5;
  x.axisConstraints             = [CPTConstraints constraintWithRelativeOffset:0.5];
  //grid
  x.majorGridLineStyle          = majorGridLineStyle;
  x.minorGridLineStyle          = minorGridLineStyle;
  
  // Label y with an automatic label policy.
  CPTXYAxis *y = axisSet.yAxis;
  y.axisLineStyle               = axisLineStyle;
  y.majorTickLineStyle          = axisLineStyle;
  y.minorTickLineStyle          = axisLineStyle;
  y.labelingPolicy              = CPTAxisLabelingPolicyAutomatic;
  y.orthogonalCoordinateDecimal = CPTDecimalFromString(@"1.0");
  y.minorTicksPerInterval       = 5;
  y.axisConstraints             = [CPTConstraints constraintWithRelativeOffset:0.5];//[CPTConstraints constraintWithLowerOffset:0.0];
  //grid
  y.majorGridLineStyle          = majorGridLineStyle;
  y.minorGridLineStyle          = minorGridLineStyle;
  y.labelOffset                 = 10.0;
  
  // Set axes
  //graph.axisSet.axes = [NSArray arrayWithObjects:x, y, y2, nil];
  graph.axisSet.axes = [NSArray arrayWithObjects:x, y, nil];
  
  // Add plot symbols
  CPTMutableLineStyle *symbolLineStyle = [CPTMutableLineStyle lineStyle];
  symbolLineStyle.lineColor     = [CPTColor blackColor];
  CPTPlotSymbol *plotSymbol     = [CPTPlotSymbol ellipsePlotSymbol];
  plotSymbol.fill               = [CPTFill fillWithColor:[CPTColor blueColor]];
  plotSymbol.lineStyle          = symbolLineStyle;
  plotSymbol.size               = CGSizeMake(5.0, 5.0);
  _dataSourcePlot.plotSymbol    = plotSymbol;
  
  // Add legend
//  graph.legend                 = [CPTLegend legendWithGraph:graph];
//  graph.legend.textStyle       = x.titleTextStyle;
//  graph.legend.fill            = [CPTFill fillWithColor:[CPTColor darkGrayColor]];
//  graph.legend.borderLineStyle = x.axisLineStyle;
//  graph.legend.cornerRadius    = 5.0;
//  graph.legend.swatchSize      = CGSizeMake(25.0, 25.0);
//  graph.legendAnchor           = CPTRectAnchorBottom;
//  graph.legendDisplacement     = CGPointMake(0.0, 12.0);
}

- (void)setupAxisNames {
  CPTXYAxisSet *axisSet = (CPTXYAxisSet *)self.hostView.hostedGraph.axisSet;
  CPTXYAxis *x = axisSet.xAxis;
  CPTXYAxis *y = axisSet.yAxis;
  x.title         = self.xAxisName ? self.xAxisName : @"X Axis";
  x.titleOffset   = -20.f;
  x.axisTitle.contentLayer.paddingRight = ScreenWidth - 20;
  
  y.title         = self.yAxisName ? self.yAxisName : @"Y Axis";
  y.titleOffset   = -45.f;
  y.axisTitle.contentLayer.paddingTop = ScreenHeight - 90;
  y.axisTitle.rotation = .0f;
}

- (void)setupAppearanceForData {
  [self setupAxisNames];
  [self scaleToFitData];
}

#pragma mark - scale
- (void)scaleToFitData {
  CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *)self.hostView.hostedGraph.defaultPlotSpace;
  CGFloat scaleFactor = 1.3;
  [plotSpace scaleToFitPlots:[NSArray arrayWithObjects:_dataSourcePlot, nil]];
  CPTMutablePlotRange *xRange = [plotSpace.xRange mutableCopy];
  CPTMutablePlotRange *yRange = [plotSpace.yRange mutableCopy];
  [xRange expandRangeByFactor:CPTDecimalFromDouble(scaleFactor)];
  [yRange expandRangeByFactor:CPTDecimalFromDouble(scaleFactor)];
  plotSpace.xRange = [xRange copy];
  plotSpace.yRange = [yRange copy];
  
  //bound draggable area
  [xRange expandRangeByFactor:CPTDecimalFromDouble(scaleFactor)];
  [yRange expandRangeByFactor:CPTDecimalFromDouble(scaleFactor)];
  plotSpace.globalXRange = xRange;
  plotSpace.globalYRange = yRange;
}


#pragma mark -
#pragma mark Plot Data Source Methods

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
  return [plotData count];
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
  NSString *key = (fieldEnum == CPTScatterPlotFieldX ? kXAxisName : kYAxisName);
  NSNumber *num = [[plotData objectAtIndex:index] valueForKey:key];
  
  if ( fieldEnum == CPTScatterPlotFieldY ) {
    num = [NSNumber numberWithDouble:[num doubleValue]];
  }
  return num;
}

#pragma mark -
#pragma mark Plot Space Delegate Methods

//-(CPTPlotRange *)plotSpace:(CPTPlotSpace *)space willChangePlotRangeTo:(CPTPlotRange *)newRange forCoordinate:(CPTCoordinate)coordinate {
//  // Impose a limit on how far user can scroll in x
//  if ( coordinate == CPTCoordinateX ) {
//    CPTPlotRange *maxRange            = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(-1.0f) length:CPTDecimalFromFloat(6.0f)];
//    CPTMutablePlotRange *changedRange = [newRange mutableCopy];
//    [changedRange shiftEndToFitInRange:maxRange];
//    [changedRange shiftLocationToFitInRange:maxRange];
//    newRange = changedRange;
//  }
//  
//  return newRange;
//}

#pragma mark -
#pragma mark CPTScatterPlot delegate method

-(void)scatterPlot:(CPTScatterPlot *)plot plotSymbolWasSelectedAtRecordIndex:(NSUInteger)index {
  CPTXYGraph *graph = (CPTXYGraph*)self.hostView.hostedGraph;
  
  if ( symbolTextAnnotation ) {
    [graph.plotAreaFrame.plotArea removeAnnotation:symbolTextAnnotation];
    symbolTextAnnotation = nil;
  }
  
  // Setup a style for the annotation
  CPTMutableTextStyle *hitAnnotationTextStyle = [CPTMutableTextStyle textStyle];
  hitAnnotationTextStyle.color    = [CPTColor greenColor];
  hitAnnotationTextStyle.fontSize = 16.0f;
  hitAnnotationTextStyle.fontName = @"HelveticaNeue-Light";
  
  // Determine point of symbol in plot coordinates
  NSNumber *x          = [[plotData objectAtIndex:index] valueForKey:kXAxisName];
  NSNumber *y          = [[plotData objectAtIndex:index] valueForKey:kYAxisName];
  NSArray *anchorPoint = [NSArray arrayWithObjects:x, y, nil];
  
  double xVal = [x doubleValue];
  double yVal = [[[plotData objectAtIndex:index] valueForKey:kYAxisValue] doubleValue];
  NSString *yString = [NSString stringWithFormat:@"%@: %.2f\n%@: %.3f", self.xAxisName, xVal, self.yAxisValueName, yVal];
  NSDecimalNumber* error = [[plotData objectAtIndex:index] valueForKey:kYErrorValue];
    if(decValid(error)) {
        yString = [yString stringByAppendingFormat:@"±%@", [[Formatter shared] stringPercentFromDecimal:error]];
    }
    
  // Now add the annotation to the plot area
  CPTTextLayer *textLayer = [[CPTTextLayer alloc] initWithText:yString style:hitAnnotationTextStyle];
  symbolTextAnnotation              = [[CPTPlotSpaceAnnotation alloc] initWithPlotSpace:graph.defaultPlotSpace anchorPlotPoint:anchorPoint];
  symbolTextAnnotation.contentLayer = textLayer;
  symbolTextAnnotation.displacement = CGPointMake(0.0f, 20.0f);
  [graph.plotAreaFrame.plotArea addAnnotation:symbolTextAnnotation];
}

@end





















