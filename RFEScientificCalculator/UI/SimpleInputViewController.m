//
//  SimpleInputViewController.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 24.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "SimpleInputViewController.h"
#import "MoleculeField.h"
#import "ResultsViewController.h"
#import "SimpleResultViewController.h"
#import "HistoryDataSource.h"
#import "HistoryViewController.h"
#import "HelpViewController.h"

@interface SimpleInputViewController () <UITextFieldDelegate, MoleculeFieldDelegate, HistoryDataSourceDelegate> {
  NSString* userInputDensity_;
}

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet MoleculeField *moleculeField;
@property (weak, nonatomic) IBOutlet UITextField *densityField;
@property (weak, nonatomic) IBOutlet UIButton *calculateButton;
@property (weak, nonatomic) IBOutlet UITableView *historyTableView;

@property (weak, nonatomic) IBOutlet UILabel *historyLabel;
@property (weak, nonatomic) IBOutlet UILabel *densityLabel;
@property (weak, nonatomic) IBOutlet UILabel *formulaTitleLabel;


@property (nonatomic) HistoryDataSource* historyData;

@property (nonatomic) AbsorptanceCalculator* absCalculator;

- (IBAction)calculateActions:(id)sender;

@end

@implementation SimpleInputViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  // Do any additional setup after loading the view.
  
  self.navigationController.navigationBar.translucent = NO;
  self.absCalculator = [AbsorptanceCalculator new];
  
  [self.scrollView registerForKeyboardNotifications];
  
  [self setupHistoryDataSource];
  [self setupUI];
}

- (void)setupUI {
  self.title = NSLocalizedString(@"Ввод", nil);
  self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonWithImage:[UIImage imageNamed:@"button_help.png"]
                                                                        target:self selector:@selector(helpAction:)];

  self.scrollView.backgroundColor = [UIColor backgroundScreenColor];
  [self.calculateButton setupMainButtonStyle];
  
  self.historyTableView.opaque = NO;
  self.historyTableView.backgroundView = nil;
  self.historyTableView.backgroundColor = [UIColor clearColor];
}

- (void)setupHistoryDataSource {
  self.historyTableView.allowsMultipleSelectionDuringEditing = NO;
  self.historyData = [HistoryDataSource historyDataSourceWithTable:self.historyTableView];
  self.historyData.delegate = self;
  self.historyData.showMoreButton = YES;
  self.historyData.showMoreButtonText = NSLocalizedString(@"Ещё", nil);
  
  self.historyTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
  self.densityField.delegate = self;
  self.moleculeField.delegate = self;
  
  self.historyLabel.textColor = [UIColor mainLabelsColor];
  self.densityLabel.textColor = [UIColor mainLabelsColor];
  self.formulaTitleLabel.textColor = [UIColor mainLabelsColor];
}

- (void)dealloc {
  [self.scrollView unregisterFromKeyboardNotifications];
}

- (void)viewWillAppear:(BOOL)animated {
  [self.historyTableView reloadData];
}

#pragma mark - UI

- (void)changeDensity:(CGFloat)density {
  self.densityField.text = density > .0f ? SPRINTF(@"%.4f", density) : @"";
}

- (void)updateCalcButtonState {
  BOOL densityExist = [self.densityField.text floatValue] > 0.f;
  self.calculateButton.enabled = densityExist;
}

- (void)clearInput {
  [self.moleculeField clear];
  self.densityField.text = @"";
}

#pragma mark - actions

- (void)helpAction:(id)sender {
  [self performSegueWithIdentifier:kHelpSegueIdentifier sender:self];
}

- (IBAction)calculateActions:(id)sender {
  BOOL densityExist = [self.densityField.text floatLocalizedValue] > 0.f;
  BOOL formulaExist = ![AbsorptanceCalculator isEmptyMolecule:self.moleculeField.moleculeDisplay.molecules];
  if (densityExist && formulaExist) {
    [self showResultsViewController];
  } else {
    [[[UIAlertView alloc] initWithTitle:@"Ошибка"
                                message:@"Введите формулу и плотность"
                               delegate:nil
                      cancelButtonTitle:@"Ок" otherButtonTitles: nil] show];
  }
}

- (void)moleculeField:(MoleculeField *)moleculeField didEnterElement:(ChemicalElement *)element {
  [self updateDensity];
}

- (void)moleculeFieldDidRemoveLastElement:(MoleculeField *)moleculeField {
  [self updateDensity];
}

- (void)updateDensity {
  NSArray* molecules = self.moleculeField.moleculeDisplay.molecules;
  ChemicalElement* simplified = [AbsorptanceCalculator simpleMolecule:molecules];
  if (simplified) {
    userInputDensity_ = @"";
    [self changeDensity:[simplified density]];
  }  else if ([userInputDensity_ floatValue] == .0f){
    [self changeDensity:.0f];
  }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  NSString* resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
  BOOL valid = [resultString length] == 0 || [resultString isStringNumber];
  if (valid) {
    textField.text = resultString;
  }
  userInputDensity_ = resultString;
  return NO;
}

#pragma mark - show results
- (void) showResultsViewController {
  SimpleResultViewController* resultViewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SimpleResultViewController class])];
  UIImage* formulaImage = [self.moleculeField.moleculeDisplay formulaImage];
  [resultViewController setMolecue:self.moleculeField.moleculeDisplay.molecule
        withDensity:[self.densityField.text decimalValue]
       formulaImage:formulaImage];
  
  
  HistoryItem* item = [HistoryItem historyItemWithMolecule:self.moleculeField.moleculeDisplay.molecule
                                                   density:[self.densityField.text decimalValue]
                                              formulaImage:formulaImage];
  [self.historyData addItem:item];
  
  
  [self showViewController:resultViewController];
}

- (void) showResultsViewControllerWithHistoryItem:(HistoryItem*)item {
  SimpleResultViewController* resultViewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SimpleResultViewController class])];
  [resultViewController setWithHistoryItem:item];
  [self showViewController:resultViewController];
}

- (void)showViewController:(UIViewController*)resultViewController {
  [self clearInput];
  [self.view endEditing:YES];
  [self.navigationController pushViewController:resultViewController animated:YES];
}

#pragma mark - history 
- (void)historyDidSelectItem:(HistoryItem*)item {
  [self showResultsViewControllerWithHistoryItem:item];
}

- (void)historyDidShowMorePressed {
  [self performSegueWithIdentifier:kHistorySegueIdentifier sender:self];
}

- (void)historyDataSourceChanged {
//  [self.historyTableView reloadData];
}
@end







