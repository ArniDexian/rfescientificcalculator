//
//  MoleculeFieldInputHandler.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 08.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MoleculeFieldInputHandlerDelegate <NSObject>

@required
-(void)didEnterChemicalElement:(ChemicalElement*)element;
-(void)didSuggestChemicalElements:(NSArray*)elements withText:(NSString*)text;
-(void)didEnterDigit:(NSInteger)digit;
-(void)didDeletePressed;
-(void)didLeftBracketPressed;
-(void)didRightBracketPressed;
-(void)didEnterPressed;

@end

/**
 *Adopts to UIKeyInput protocol. Handle key input and translates it to ChemicalElement object,
 *like it was entered from periodic table
 */
@interface MoleculeFieldInputHandler : NSObject<UIKeyInput>

@property (nonatomic, weak) id<MoleculeFieldInputHandlerDelegate> delegate;
@property (nonatomic, readonly) NSString* text;

- (void)clearInput;

@end
