//
//  UIButton+Design.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 25/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "UIButton+Design.h"

@implementation UIButton (Design)

- (void)setupMainButtonStyle {
  UIEdgeInsets capInsets = UIEdgeInsetsMake(5.f, 5.f, 5.f, 5.f);
  [self setBackgroundImage:[[UIImage imageNamed:@"button.png"] resizableImageWithCapInsets:capInsets resizingMode:UIImageResizingModeStretch]
                  forState:UIControlStateNormal];
  [self setBackgroundImage:[[UIImage imageNamed:@"button_selected.png"] resizableImageWithCapInsets:capInsets resizingMode:UIImageResizingModeStretch]
                  forState:UIControlStateHighlighted];
  
  self.titleLabel.font = [UIFont fontWithName:@"HelveticaHeue" size:17.f];
  
  [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

@end
