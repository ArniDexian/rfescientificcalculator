//
//  BarMaterialCell.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 01.10.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoleculeDisplayView.h"
#import "BarMaterial.h"

@interface BarMaterialCell : UITableViewCell<UITextFieldDelegate>

@property (nonatomic) MoleculeDisplayView* moleculeDisplay;
@property (nonatomic) UILabel* materialLabel;
@property (nonatomic) UITextField* materialThicknessField;

@property (nonatomic) BarMaterial* material;

@end
    