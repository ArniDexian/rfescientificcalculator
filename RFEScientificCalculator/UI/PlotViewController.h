//
//  PlotViewController.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 03.04.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PlotView.h"

@interface PlotViewController : UIViewController <PlotViewDataSource>

@property (nonatomic, weak) IBOutlet PlotView* plotView;

@property (nonatomic) AbsorptanceCalculator* absorptanceCalculator;
@property (nonatomic) UIImage* formulaImage;

@end
