//
//  InputDataViewController.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 04.10.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "_InputDataViewController.h"
#import "BarMaterialCell.h"
#import "PeriodicTable.h"
#import "AbsorptanceTable.h"
#import "ResultsViewController.h"

typedef enum{
  TableSectionMaterials = 0,
  TableSectionEnergy = 1,
  TableSectionResultButton = 2
} TableSection;

@interface _InputDataViewController ()

///array of materials, which contain @[stackArray, isBracketOpen, barMaterial]
@property (nonatomic) NSMutableArray* materials;
@property (nonatomic) int currentMaterial;
@property (nonatomic) NSMutableArray* stack;
@property (nonatomic) BOOL isBracketOpen;

@property (nonatomic) PeriodicTableView* periodicTable;
@property (nonatomic) ElementsExtraKeyboard* extraKeyboard;
@property (nonatomic) UIView* keyboardContainer;

@property (nonatomic) MoleculeDisplayView* moleculeDisplay;

@end

@implementation _InputDataViewController

-(NSMutableArray*)materials {
  if(!_materials){
    _materials = [NSMutableArray arrayWithObject:[NSMutableArray arrayWithObjects:[NSMutableArray array], @(NO),[BarMaterial new], nil]];
  }
  return _materials;
}

-(NSMutableArray*)stack {
  return self.materials[_currentMaterial][0];
}

-(BOOL)isBracketOpen {
  return [self.materials[_currentMaterial][1] boolValue];
}

-(void)setIsBracketOpen:(BOOL)isBracketOpen {
  self.materials[_currentMaterial][1] = @(isBracketOpen);
}

-(PeriodicTableView*)periodicTable {
  if(_periodicTable == nil){
    CGSize size = self.keyboardContainer.size;
    _periodicTable = [[PeriodicTableView alloc] initWithFrame:CGRectMake(0, self.extraKeyboard.bottomBound, size.width, size.height - self.extraKeyboard.bottomBound)];
    _periodicTable.delegate = self;
    _periodicTable.disableChangingFrameSize = YES;
    [self.keyboardContainer addSubview:_periodicTable];
  }
  return _periodicTable;
}

-(ElementsExtraKeyboard*)extraKeyboard {
  if(!_extraKeyboard){
    _extraKeyboard = [[ElementsExtraKeyboard alloc] initWithFrame:CGRectMake(0, 0, self.keyboardContainer.width, 0)];
    _extraKeyboard.delegate = self;
    _extraKeyboard.backgroundColor = RGBA(255.0f, 255.0f, 255.0f, 0.3f);
    [self.keyboardContainer addSubview:_extraKeyboard];
  }
  return _extraKeyboard;
}

-(UIView*)keyboardContainer {
  if(!_keyboardContainer){
    _keyboardContainer = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, self.view.height)];
    _keyboardContainer.backgroundColor = RGBA(.0f, .0f, .0f, 0.5f);
    _keyboardContainer.hidden = YES;
    [self.view addSubview:_keyboardContainer];
  }
  return _keyboardContainer;
}

- (id)initWithStyle:(UITableViewStyle)style {
  self = [super initWithStyle:UITableViewStyleGrouped];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapView:)];
  tap.cancelsTouchesInView = NO;
  [self.view addGestureRecognizer:tap];
  
  [PeriodicTable sharedInstance];
  [self extraKeyboard];
  [self periodicTable];
  
  [AbsorptanceTable sharedInstance];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

-(void)tapView:(id)sender {
  [self hideSystemKeyboard];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  // Return the number of sections.
  return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  switch (section) {
    case TableSectionMaterials:
      return 1 + self.materials.count;
    case TableSectionEnergy: return 1;
    case TableSectionResultButton: return 1;
    default: return 1;
  }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell *cell;
  switch (indexPath.section) {
    case TableSectionMaterials:
    {
      //the last cell in the section - add cell button
      if([self isIndexPathAddMaterialButton:indexPath]){
        static NSString *CellIdentifier = @"Cell1";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(cell == nil){
          cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.textLabel.text = @"Add material";
      }else{//cells with materials
        static NSString *CellIdentifier = @"Cell2";
        BarMaterialCell* barCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if(barCell == nil){
          barCell = [[BarMaterialCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
          barCell.materialLabel.text = @"Thickness:";
          barCell.selectionStyle = UITableViewCellSelectionStyleNone;
          barCell.moleculeDisplay.delegate = self;
        }
        barCell.material = self.materials[indexPath.row][2];
        cell = barCell;
      }
    }
      break;
      
    case TableSectionEnergy:
    {
      static NSString *CellIdentifier = @"Cell3";
      cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
      if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
      }
      cell.textLabel.text = @"Energy field";
    }
      break;
      
    case TableSectionResultButton:
    {
      static NSString *CellIdentifier = @"Cell4";
      cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
      if(cell == nil){
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
      }
      cell.textLabel.text = @"Results";
    }
      break;
  }
  
  
  
  return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  CGFloat height;
  if(indexPath.section == TableSectionMaterials){
    if(indexPath.row == [self tableView:tableView numberOfRowsInSection:indexPath.section]-1)
      height = 40;
    else height = 85;
  }else height = 40;
  return height;
}

-(NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  if([self isIndexPathAddMaterialButton:indexPath] || indexPath.section == TableSectionResultButton)
    return indexPath;
  else return nil;
}

// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
  // Return NO if you do not want the specified item to be editable.
  if(indexPath.section == TableSectionMaterials){
    return indexPath.row != 0 && ![self isIndexPathAddMaterialButton:indexPath];
  }else return NO;
}

-(BOOL)isIndexPathAddMaterialButton:(NSIndexPath*)indexPath {
  return indexPath.section == TableSectionMaterials && indexPath.row == [self tableView:self.tableView numberOfRowsInSection:indexPath.section]-1;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
  if (editingStyle == UITableViewCellEditingStyleDelete) {
    [self.materials removeObjectAtIndex:indexPath.row];
    // Delete the row from the data source
    [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
  }
}

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:NO];
  
  if([self isIndexPathAddMaterialButton:indexPath]){
    [self addNewBarMaterialCell];
  } else if (indexPath.section == TableSectionResultButton) {
    [self openResultViewController];
  }
}

-(void)addNewBarMaterialCell {
  [self.materials addObject:[NSMutableArray arrayWithObjects:[NSMutableArray array], @(NO), [BarMaterial new], nil]];
  [self.tableView reloadData];
}

#pragma mark - actions

- (void)openResultViewController {
  ResultsViewController* resultViewController = [ResultsViewController new];
  resultViewController.substances = self.materials;
  [self.navigationController pushViewController:resultViewController animated:YES];
}

#pragma mark - keyboard

-(void)hideSystemKeyboard {
  [self.view endEditing:NO];
}

-(void)moleculeDisplayDidBeginEditing:(MoleculeDisplayView *)molecDispl {
  [self hideSystemKeyboard];
  [self showPeriodicTable];
  
  molecDispl.selected = YES;
  
  UITableViewCell* cell = (UITableViewCell*)molecDispl.superview.superview;//1st-container, 2nd-cell
  NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
  [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
  CGRect rectCell = [self.tableView rectForRowAtIndexPath:indexPath];
  
  _keyboardContainer.y = rectCell.origin.y + rectCell.size.height;
  
  _currentMaterial = indexPath.row;
  _moleculeDisplay = molecDispl;
}

-(void)setKeyboardContainerFrame:(CGRect)frame {
  _keyboardContainer.frame = frame;
  _periodicTable.size = CGSizeMake(frame.size.width, frame.size.height-(_periodicTable.y));
}

-(void)showPeriodicTable {
  int y = 0/*100 + self.tableView.contentOffset.y*/;
  
  [self setKeyboardContainerFrame:CGRectMake(0, y, self.view.width, self.view.height-100)];
  self.keyboardContainer.hidden = NO;
}

-(void)hidePeriodicTable {
  self.keyboardContainer.hidden = YES;
  //    self.tableView.userInteractionEnabled = YES;
  _moleculeDisplay.selected = NO;
  
  [self saveCurrentMolecule];
}

-(void)saveCurrentMolecule
{
  BarMaterial* mat = self.materials[_currentMaterial][2];
  mat.material = [_moleculeDisplay molecule];
}

#pragma mark - extra keyboard actions
-(void)extraKeyboard0Pressed:(ElementsExtraKeyboard *)keyboard
{
  [self inputDigit:0];
}

-(void)extraKeyboard1Pressed:(ElementsExtraKeyboard *)keyboard
{
  [self inputDigit:1];
}

-(void)extraKeyboard2Pressed:(ElementsExtraKeyboard *)keyboard
{
  [self inputDigit:2];
}

-(void)extraKeyboard3Pressed:(ElementsExtraKeyboard *)keyboard
{
  [self inputDigit:3];
}

-(void)extraKeyboard4Pressed:(ElementsExtraKeyboard *)keyboard
{
  [self inputDigit:4];
}

-(void)extraKeyboard5Pressed:(ElementsExtraKeyboard *)keyboard
{
  [self inputDigit:5];
}

-(void)extraKeyboard6Pressed:(ElementsExtraKeyboard *)keyboard
{
  [self inputDigit:6];
}

-(void)extraKeyboard7Pressed:(ElementsExtraKeyboard *)keyboard
{
  [self inputDigit:7];
}

-(void)extraKeyboard8Pressed:(ElementsExtraKeyboard *)keyboard
{
  [self inputDigit:8];
}

-(void)extraKeyboard9Pressed:(ElementsExtraKeyboard *)keyboard
{
  [self inputDigit:9];
}

-(void)extraKeyboardRotatePressed:(ElementsExtraKeyboard *)keyboard
{
  [self.periodicTable swapRotationAnimated:YES];
}

-(void)extraKeyboardRemovePressed:(ElementsExtraKeyboard *)keyboard
{
  MoleculeContainer* lastStack = [self.stack lastObject];
  if(lastStack != nil) [self removeMoleculeFromStackAndDisplay:lastStack];
  else [self.moleculeDisplay removeLast];
  
  //check if it's last bracket
  if(self.stack.count == 0 || [self lastOpenedMoleculeContainer] == nil)
    self.isBracketOpen = NO;
}

-(void)extraKeyboardLeftBracketPressed:(ElementsExtraKeyboard *)keyboard
{
  [self inputLeftBracket];
}

-(void)extraKeyboardRightBracketPressed:(ElementsExtraKeyboard *)keyboard
{
  [self inputRightBracket];
}

-(void)extraKeyboardHidePressed:(ElementsExtraKeyboard *)keyboard
{
  [self hidePeriodicTable];
}

#pragma mark - logic

-(void)periodicTable:(PeriodicTableView *)table elementWasSelected:(ChemicalElement *)element
{
  [self inputElement:element];
}

-(void)inputElement:(ChemicalElement*)element
{
  if(!self.isBracketOpen){
    [self.stack removeAllObjects];
  }
  
  MoleculeContainer* mol = [MoleculeContainer emptyMolecule];
  mol.element = element;
  [self addToStackMolecule:mol];
  if(self.isBracketOpen){
    [self.moleculeDisplay setNeedsDisplay];
  }else{
    [self.moleculeDisplay addMolecule:mol];
  }
}

-(void)inputDigit:(int)dig
{
  MoleculeContainer* molecule = [self lastMoleculeContainer];
  if(molecule == nil) return;
  
  molecule.quantity = [self quantityOfAtomsByAddingTo:molecule.quantity digit:dig];
  
  [self.moleculeDisplay setNeedsDisplay];
}

//ready
-(void)inputLeftBracket
{
  //удалить ввод из стека - первое открытие скобки
  if(!self.isBracketOpen) {
    [self.stack removeAllObjects];
    MoleculeContainer* molec = [MoleculeContainer emptyMolecule];
    [self addToStackMolecule:molec];
    [self.moleculeDisplay addMolecule:molec];
    self.isBracketOpen = YES;
    return;
  }
  
  //    MoleculeContainer* rootMolecule = [self.stack objectAtIndex:0];
  MoleculeContainer* newMolecule = [MoleculeContainer emptyMolecule];
  [self addToStackMolecule:newMolecule];
  [self.moleculeDisplay setNeedsDisplay];
  
}

//may be ready
-(void)inputRightBracket
{
  if(!self.isBracketOpen || !self.stack.count) return;
  for (int i = self.stack.count - 1; i >= 0; --i) {
    MoleculeContainer* iCon = [self.stack objectAtIndex:i];
    if([iCon.element isKindOfClass:[NSMutableArray class]] && iCon.quantity == -1){
      //if the array is empty
      if([(NSMutableArray*)iCon.element count] == 0){
        [self removeMoleculeFromStackAndDisplay:iCon];
        if(self.stack.count < 1) self.isBracketOpen = NO;
        return;
      }
      
      iCon.quantity = 0;
      
      if(i + 1 < self.stack.count)
        [self.stack removeObjectsInRange:NSMakeRange(i+1, self.stack.count-(i+1))];
      
      if(self.stack.count <= 1) self.isBracketOpen = NO;
      
      break;
    }
  }
  [self.moleculeDisplay setNeedsDisplay];
}

//result - was it added to other molecule
-(BOOL)addToStackMolecule:(MoleculeContainer*)molec
{
  [self.stack addObject:molec];
  if(self.stack.count == 0) return NO;
  
  for (int i = self.stack.count - 2; i >= 0; --i) {
    MoleculeContainer* iCon = [self.stack objectAtIndex:i];
    if([iCon.element isKindOfClass:[NSMutableArray class]]){
      if(iCon.quantity == -1){
        [(NSMutableArray*)iCon.element addObject:molec];
        return YES;
      }
    }
  }
  return NO;
}

-(MoleculeContainer*)lastOpenedMoleculeContainer
{
  if(self.stack.count == 0) return nil;
  MoleculeContainer* mol = nil;
  for (int i = self.stack.count - 1; i >= 0; --i) {
    MoleculeContainer* iCont = [self.stack objectAtIndex:i];
    if([iCont.element isKindOfClass:[NSMutableArray class]] && iCont.quantity == -1){
      mol = iCont;
      break;
    }
  }
  return mol;
}

//last visible molecule
-(MoleculeContainer*)lastMoleculeContainer
{
  if(self.stack.count == 0) return nil;
  MoleculeContainer* mol = [self.stack lastObject];
  for (int i = self.stack.count - 1; i >= 0; --i) {
    MoleculeContainer* iCont = [self.stack objectAtIndex:i];
    if([iCont.element isKindOfClass:[NSMutableArray class]]){
      //if we try to set quant to zero count array - remove it
      if([(NSMutableArray*)iCont.element count] == 0){
        [self removeMoleculeFromStackAndDisplay:iCont];
        if ([self lastOpenedMoleculeContainer] == nil) {
          self.isBracketOpen = NO;
        }
        mol = nil;
        break;
      }
      
      if(![[(NSMutableArray*)iCont.element lastObject] isEqual:self.stack.lastObject]) break;
      
      if(iCont.quantity != -1)
        mol = iCont;
      break;
    }
  }
  return mol;
}

-(void)removeMoleculeFromStackAndDisplay:(MoleculeContainer*)cont
{
  [self.moleculeDisplay removeMolecule:cont];
  [self removeMolecule:cont fromArray:self.stack];
}

-(void)removeMolecule:(MoleculeContainer*)molecToRemove fromArray:(NSMutableArray*)array
{
  if(array.count == 0) return;
  if([array containsObject:molecToRemove])
    [array removeObject:molecToRemove];
  for (MoleculeContainer* molec in array) {
    if([molec.element isKindOfClass:[NSMutableArray class]])
      [self removeMolecule:molecToRemove fromArray:molec.element];
  }
  
}

-(int)quantityOfAtomsByAddingTo:(int)quant digit:(int)dig
{
  if(quant <= 0) quant = 0;
  if(quant < 1){
    quant = dig;
  }else{
    quant = [[NSString stringWithFormat:@"%i%i", quant, dig] intValue];
  }
  if(quant > 999) quant = 999;
  
  return quant;
}




@end



















