//
//  InputDataViewController.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 04.10.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoleculeDisplayView.h"
#import "PeriodicTableView.h"
#import "ElementsExtraKeyboard.h"

@interface _InputDataViewController : UITableViewController<PeriodicTableViewDelegate, MoleculeDisplayViewDelegate, ElementsExtraKeyboardDelegate>

@end
