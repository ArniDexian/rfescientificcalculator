//
//  SimpleResultViewController.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 21/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "SimpleResultViewController.h"
#import "AbsorptanceCalculator.h"
#import "PlotViewController.h"
#import "HelpViewController.h"

@interface SimpleResultViewController ()

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UILabel *substanceTitle;
@property (weak, nonatomic) IBOutlet UIImageView *substanceImageView;
@property (weak, nonatomic) IBOutlet UILabel *inputEnergyLabel;
@property (weak, nonatomic) IBOutlet UITextField *energyField;
@property (weak, nonatomic) IBOutlet UILabel *absHintLabel;

@property (weak, nonatomic) IBOutlet UIImageView *absApproxIconView;
@property (weak, nonatomic) IBOutlet UIImageView *absIconView;
@property (weak, nonatomic) IBOutlet UILabel *absLabel;
@property (weak, nonatomic) IBOutlet UILabel *absApproxLabel;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *absAndButtonHintOffset;

@property (weak, nonatomic) IBOutlet UILabel *graphicHintLabel;
@property (weak, nonatomic) IBOutlet UIButton *showGraphicButton;

@property (nonatomic) UIImage* formulaImage;
@property (nonatomic) MoleculeContainer* molecule;
@property (nonatomic) NSDecimalNumber* density;
@property (nonatomic) NSDecimalNumber* energy;
@property (nonatomic) NSDecimalNumber* absFactor;

@property (nonatomic, strong) AbsorptanceCalculator* absorptanceCalculator;

- (IBAction)showGraphic:(id)sender;

@end

@implementation SimpleResultViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc {
  [self.scrollView unregisterFromKeyboardNotifications];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  [self setupUI];
  [self.scrollView registerForKeyboardNotifications];
}

- (void)setupUI {
  self.substanceImageView.layer.borderColor = [UIColor elementsBlueColor].CGColor;
  self.substanceImageView.layer.borderWidth = 1.f;
  self.scrollView.backgroundColor = [UIColor backgroundScreenColor];
  [self.showGraphicButton setupMainButtonStyle];
  
  self.inputEnergyLabel.textColor = [UIColor mainLabelsColor];
  self.absHintLabel.textColor = [UIColor mainLabelsColor];
  self.graphicHintLabel.textColor = [UIColor mainLabelsColor];
  self.absLabel.textColor = [UIColor elementsBlueColor];
  self.absApproxLabel.textColor = [UIColor orangeColor];
  self.substanceTitle.textColor = [UIColor mainLabelsColor];
  self.absIconView.image = [UIImage imageNamed:@"table.png"];
  self.absApproxIconView.image = [UIImage imageNamed:@"calc.png"];
  self.navigationItem.rightBarButtonItem = [UIBarButtonItem barButtonWithImage:[UIImage imageNamed:@"button_help.png"]
                                                                        target:self selector:@selector(helpAction:)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setMolecue:(MoleculeContainer*)molecule withDensity:(NSDecimalNumber*)density formulaImage:(UIImage*)image {
  self.molecule = molecule;
  self.density = density;
  self.formulaImage = image;
  self.absorptanceCalculator = [AbsorptanceCalculator calculatorWithMolecule:molecule andDensity:self.density];
  self.absFactor = [self.absorptanceCalculator absorptanceOfMoleculeWithEnergy:self.energy].decAbsorptance;
}

- (void)setWithHistoryItem:(HistoryItem*)item {
  [self setMolecue:item.molecule withDensity:item.density formulaImage:item.formulaImage];
}

- (void)viewWillAppear:(BOOL)animated {
  [self updateUI];
}

- (void)updateUI {
  self.substanceImageView.image = self.formulaImage;
  [self updateAbsFactor];
}

- (void)updateAbsFactor {
  MoleculeAbsorptance* abs = [self.absorptanceCalculator absorptanceOfMoleculeWithEnergy:[self.energyField.text decimalValue]];
  self.absFactor = abs.decAbsorptance;
  if (abs.interpolated) {
    self.absAndButtonHintOffset.constant = CGRectGetMaxY(_absApproxIconView.frame) - CGRectGetMaxY(_absIconView.frame) + 20.f;;
    self.absApproxIconView.hidden = NO;
    self.absApproxLabel.hidden = NO;
      self.absLabel.text = [self tableStringFromAbsorptace:abs];
    self.absApproxLabel.text = [self absStringFromNumber:abs.decAbsorptance error:decAbs(abs.absorptanceDelta)];
    self.absHintLabel.text = NSLocalizedString(@"Данной энергии соответствует интерполированный коэффициент поглощения", nil);
  } else {
    self.absAndButtonHintOffset.constant = 20.f;
    self.absApproxIconView.hidden = YES;
    self.absApproxLabel.hidden = YES;
    self.absLabel.text = [self absStringFromNumber:abs.decAbsorptance error:decAbs(abs.absorptanceDelta)];
    self.absHintLabel.text = NSLocalizedString(@"Данной энергии соответствует коэффициент поглощения", nil);
  }
}

- (NSString*)absStringFromNumber:(NSDecimalNumber*)abs error:(NSDecimalNumber*)error {
    NSString* res = nil;
    if (decValid(abs)) {
        NSString* num = [[Formatter shared] stringFromDecimal:abs];
        if (error) {
            res = [NSString stringWithFormat:@"%@ ± %@", num, [[Formatter shared] stringPercentFromDecimal:error]];
        } else {
            res = num;
        }
    } else {
        res = NSLocalizedString(@"Неизвестно", nil);
    }
    return res;
}

- (NSString*)tableStringFromAbsorptace:(MoleculeAbsorptance*)abs {
    NSString* res = nil;
    if (decValid(abs.decTableAbsorptance)) {
        NSDecimalNumber* error = decAbs(abs.tableAbsorptanceDelta);
        NSString* errMsg = error ? [NSString stringWithFormat:@"± %@", [[Formatter shared] stringPercentFromDecimal:error]] : @"";

        res = [NSString stringWithFormat:@"%@ %@ при E = %@", [[Formatter shared] stringFromDecimal:abs.decTableAbsorptance], errMsg, abs.decTableEnergy];
        
    } else {
        res = NSLocalizedString(@"Неизвестно", nil);
    }
    return res;
}

- (IBAction)showGraphic:(id)sender {
  PlotViewController* plot = [self.view.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"PlotViewControllerIdentifier"];
  plot.absorptanceCalculator = self.absorptanceCalculator;
  plot.formulaImage = self.formulaImage;
  [self.navigationController pushViewController:plot animated:YES];
}

- (void)helpAction:(id)sender {
  [self performSegueWithIdentifier:kHelpSegueIdentifier sender:self];
}

#pragma mark - input 
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  if ([string isEqualToString:@"\n"]) {
    [textField resignFirstResponder];
  } else {
    NSString* resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    BOOL valid = [resultString length] == 0 || [resultString isStringNumber];
    if (valid) {
      textField.text = resultString;
    }
    [self updateAbsFactor];
  }
  return NO;
}
@end







