//
//  MoleculeField.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 03.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoleculeDisplayView.h"

@class MoleculeField;

@interface MoleculeFieldValue : NSObject
@property (nonatomic, strong) MoleculeContainer* molecule;
+ (id)moleculeFieldValue;
@end

@protocol MoleculeFieldDelegate <NSObject>
@optional
- (void)moleculeField:(MoleculeField*)moleculeField didEnterElement:(ChemicalElement*)element;
- (void)moleculeFieldDidRemoveLastElement:(MoleculeField*)moleculeField;

@end

/**
 *Handle user input, display and manage editing moleclue
 */
@interface MoleculeField : UIView <UIKeyInput>

@property (nonatomic) MoleculeDisplayView* moleculeDisplay;
@property (nonatomic, weak) id<MoleculeFieldDelegate> delegate;

- (MoleculeFieldValue*)obtainFormulaValue;
- (void)setFormulaValue:(MoleculeFieldValue*)value;

/**
 *Clears input
 */
- (void)clear;

@end
