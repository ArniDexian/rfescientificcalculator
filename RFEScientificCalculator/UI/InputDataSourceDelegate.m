//
//  InputDataSourceDelegate.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 13.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "InputDataSourceDelegate.h"
#import "MoleculeBarTableViewCell.h"
#import "EditDataTableViewCell.h"

@interface InputDataSourceDelegate () <EditDataTableViewCellDelegate>
@property (nonatomic, weak) UITableView* tableView;
@end

@implementation InputDataSourceDelegate

+ (id)dataSourceForTableView:(UITableView *)tableView {
  InputDataSourceDelegate* dataSource = [InputDataSourceDelegate new];
  dataSource.tableView = tableView;
  tableView.dataSource = dataSource;
  return dataSource;
}

- (instancetype)init {
  self = [super init];
  if (self) {
    [self initializeData];
  }
  return self;
}

#pragma mark - tableView data source
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return InputDataSectionsCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  NSInteger count = 0;
  switch (section) {
    case InputDataSectionMolecule:
      count = self.molecules.count + 1;
      break;
      
    case InputDataSectionAdditionData:
      count = InputDataSectionAdditionDataRowsCount;
      break;
      
    case InputDataSectionCalculateButton:
      count = 1;
      break;
  }
  return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  UITableViewCell* cell = nil;
  switch (indexPath.section) {
    case InputDataSectionMolecule:
      cell = [self cellForMoleculeSectionWithIndexPath:indexPath];
      break;
      
    case InputDataSectionAdditionData:
      cell = [self cellForAdditionDataSectionWithIndexPath:indexPath];
      break;
      
    case InputDataSectionCalculateButton:
      cell = [self calculateButtonCell];
      break;
  }
  return cell;
}

- (UITableViewCell*)cellForMoleculeSectionWithIndexPath:(NSIndexPath*)path {
  UITableViewCell* cell = nil;
  if (path.row < self.molecules.count) {
    cell = [self moleculeCellForIndexPath:path];
  } else {
    cell = [self addMoleculeCell];
  }
  
  return cell;
}

- (UITableViewCell*)cellForAdditionDataSectionWithIndexPath:(NSIndexPath*)path {
  static NSString* reusableIdentifier = @"EditCellIdentifier";
  EditDataTableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:reusableIdentifier
                                                                     forIndexPath:path];
  switch (path.row) {
    case InputDataSectionAdditionDataDensityRow:
      cell.textField.text = self.density;
      cell.label.text = NSLocalizedString(@"Плотность", nil);
      cell.tag = InputDataSectionAdditionDataDensityRow;
      break;
      
    case InputDataSectionAdditionDataEnergyRow:
      cell.textField.text = self.energy;
      cell.label.text = NSLocalizedString(@"Энергия", nil);
      cell.tag = InputDataSectionAdditionDataEnergyRow;
      break;
  }
  cell.delegate = self;
  return cell;
}

- (MoleculeBarTableViewCell*)moleculeCellForIndexPath:(NSIndexPath*)path {
  MoleculeBarTableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:@"MoleculeBarCellIdentifier" forIndexPath:path];
  [cell.moleculeField setFormulaValue:self.molecules[path.row][0]];
  NSNumber* height = self.molecules[path.row][1];
  cell.barHeightField.text = [height floatValue] > 0 ? [NSString stringWithFormat:@"%@", height] : @"";
  cell.tag = InputDataSectionMoleculeRow;
  
  return cell;
}

- (UITableViewCell*)addMoleculeCell {
  static NSString* reusableIdentifier = @"AddMoleculeBarCellIdentifier";
  UITableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableIdentifier];
  }
  cell.textLabel.text = NSLocalizedString(@"Добавить", nil);
  cell.tag = InputDataSectionMoleculeAddButtonRow;
  return cell;
}

- (UITableViewCell*)calculateButtonCell {
  static NSString* reusableIdentifier = @"CalculateButtonCellIdentifier";
  UITableViewCell* cell = [self.tableView dequeueReusableCellWithIdentifier:reusableIdentifier];
  if (cell == nil) {
    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reusableIdentifier];
  }
  cell.textLabel.text = NSLocalizedString(@"Расчет", nil);
  cell.tag = InputDataSectionCalculateButton;
  return cell;
}

#pragma mark - data
- (void)initializeData {
  self.molecules = [NSMutableArray arrayWithObjects: [self emptyMoleculeContainer], nil];
  self.energy = @"1";
  self.density = @"1";
}

- (id)emptyMoleculeContainer {
  return  @[[MoleculeFieldValue moleculeFieldValue], @(0)];
}

- (void)addEmptyMolecule {
  id moleculeToAdd = [self emptyMoleculeContainer];
  NSIndexPath* insertedRowPath = [NSIndexPath indexPathForRow:self.molecules.count inSection:InputDataSectionMolecule];
  [self.molecules addObject:moleculeToAdd];
  [self.tableView insertRowsAtIndexPaths:@[insertedRowPath] withRowAnimation:UITableViewRowAnimationNone];
  [self.tableView reloadRowsAtIndexPaths:@[insertedRowPath] withRowAnimation:UITableViewRowAnimationAutomatic];
}

- (NSArray*)obtainBarMaterials {
  NSMutableArray* barMaterials = [NSMutableArray array];
  for (NSArray* molecule in self.molecules) {
    MoleculeFieldValue* fieldValue = molecule[0];
    BarMaterial* barMaterial = [BarMaterial barWithMaterial:fieldValue.molecule andThickness:[molecule[1] doubleValue]];
    [barMaterials addObject:barMaterial];
  }
  return barMaterials.copy;
}

#pragma mark - saving

- (void)saveValuesFromCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
  switch (indexPath.section) {
    case InputDataSectionMolecule:
      [self saveMoleculeFromCell:cell withIndexPath:indexPath];
      break;
      
    case InputDataSectionAdditionData:
      [self saveAdditionDataFromCell:cell withIndexPath:indexPath];
      break;
  }
}

- (void)saveMoleculeFromCell:(UITableViewCell*)cell withIndexPath:(NSIndexPath*)path {
  if (path.row < self.molecules.count) {
    MoleculeBarTableViewCell* _cell = (MoleculeBarTableViewCell*)cell;
    NSArray* saveValue = @[[_cell.moleculeField obtainFormulaValue], @([_cell.barHeightField.text doubleValue])];
    [self.molecules replaceObjectAtIndex:path.row withObject:saveValue];
  }
}

- (void)saveAdditionDataFromCell:(UITableViewCell*)cell withIndexPath:(NSIndexPath*)path {
  EditDataTableViewCell* _cell = (EditDataTableViewCell*)cell;
  switch (path.row) {
    case InputDataSectionAdditionDataDensityRow:
      self.density =  _cell.textField.text;
      break;
      
    case InputDataSectionAdditionDataEnergyRow:
      self.energy = _cell.textField.text;
      break;
  }
}

- (void)editDataTableCell:(EditDataTableViewCell *)cell didChangeValue:(NSNumber *)value {
  NSIndexPath* path = [self.tableView indexPathForCell:cell];
  [self saveAdditionDataFromCell:cell withIndexPath:path];
}


@end
