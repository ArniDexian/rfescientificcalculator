//
//  InputDataSourceDelegate.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 13.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
  InputDataSectionMolecule = 0,
  InputDataSectionAdditionData,
  InputDataSectionCalculateButton,
  InputDataSectionsCount
} InputDataSections;

typedef enum {
  InputDataSectionMoleculeRow = 0,
  InputDataSectionMoleculeAddButtonRow,
  InputDataSectionMoleculeRowsCount
} InputDataSectionMoleculeRows;

typedef enum {
  InputDataSectionAdditionDataDensityRow = 0,
  InputDataSectionAdditionDataEnergyRow,
  InputDataSectionAdditionDataRowsCount
} InputDataSectionAdditionDataRows;

@class InputDataSourceDelegate;

@protocol InputDataSourceDelegateProtocol <NSObject>

@end

@interface InputDataSourceDelegate : NSObject <UITableViewDataSource>

+ (id)dataSourceForTableView:(UITableView*)tableView;

//@[MoleculeFieldValue, @(thickness)]
@property (nonatomic) NSMutableArray* molecules;
@property (nonatomic) NSString* density;
@property (nonatomic) NSString* energy;

- (void)addEmptyMolecule;
- (void)saveValuesFromCell:(UITableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath;

- (NSArray*)obtainBarMaterials;

@end
