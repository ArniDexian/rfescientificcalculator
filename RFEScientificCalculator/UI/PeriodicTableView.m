//
//  PeriodicTableView.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 24.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "PeriodicTableView.h"
#import "LocalStorageProvider.h"
#import "PeriodicTable.h"

static void* PeriodicLoadedContext = &PeriodicLoadedContext;

#define TABLE_MARGIN 10
#define ELEMENT_MARGIN 2
#define ROTATE_TIME 0.5
#define ROT_VIEW_TIME 0.3

@interface PeriodicTableView ()

@property (nonatomic) NSArray* elements;
@property (nonatomic) NSArray* elementViews;

@property (nonatomic) UIInterfaceOrientation orientation;
@property (nonatomic) UIInterfaceOrientation viewsOrientation;
@end


@implementation PeriodicTableView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
//        [self loadElements];
        self.directionalLockEnabled = YES;
        if([PeriodicTable sharedInstance].elements)
            [self initElements:[PeriodicTable sharedInstance].elements];
        else{
            [[PeriodicTable sharedInstance] addObserver:self
                   forKeyPath:@"elements"
                      options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew
                      context:PeriodicLoadedContext];
            
        }
        self.orientation = UIInterfaceOrientationLandscapeLeft;
        self.viewsOrientation = UIInterfaceOrientationPortrait;
        [self updateContentSizeSwap:NO];
//        
//        UIRotationGestureRecognizer* gest = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(swapRotationAnimated:)];
//        gest.rotation = M_PI_2;
//        [self addGestureRecognizer:gest];
    }
    return self;
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSLog(@"observeValueForKeyPath: %@", keyPath);
    if(context == PeriodicLoadedContext){
        [self initElements:[PeriodicTable sharedInstance].elements];
    }else if(context != nil) [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
}

-(void)dealloc
{
    self.elements = nil;
    self.elementViews = nil;
}

//-(void)loadElements
//{
////    NSLog(@"request elements...");
//    [LocalStorageProvider requestElementsWithComplete:^(NSArray *elements) {
////        NSLog(@"elements:\n%@", elements);
//        [self initElements:elements];
//    } error:^(NSError *error) {
//        NSLog(@"error: %@", error);
//    }];
//}

-(void)initElements:(NSArray*)elements
{
    if(self.elements.count > 0) return;
    self.elements = elements;
    NSMutableArray* elViews = [NSMutableArray arrayWithCapacity:elements.count];
    for (ChemicalElement* element in elements) {
        ElementView* elView = [ElementView viewWithElement:element];
        [elViews addObject:elView];
        [elView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapElementView:)]];
        elView.backgroundColor = element.color;
        elView.position = [self positionForElement:element];
        [self addSubview:elView];
    }
    self.elementViews = elViews;
    self.contentSize = CGSizeMake(18 * ElementViewSize.width + 17*ELEMENT_MARGIN + 2*TABLE_MARGIN, 10 * ElementViewSize.height + 7*ELEMENT_MARGIN + 3*TABLE_MARGIN);
}

-(CGPoint)positionForElement:(ChemicalElement*)element
{
    int x = TABLE_MARGIN, y = TABLE_MARGIN;
    switch (element.el_group) {
        case MainChemGroup:
        {
            x += (element.group - 1) * ElementViewSize.width + (element.group - 1)*ELEMENT_MARGIN;
            y += (element.period - 1) * ElementViewSize.height + (element.period - 1)*ELEMENT_MARGIN;
        }break;
        case LanthanideChemGroup:
        {
            y += 7 * (ElementViewSize.height + ELEMENT_MARGIN) + TABLE_MARGIN;
            int n_el = element.index - 57; //lanthanide starts from 57 end by 71
            x += (3 + n_el) * (ElementViewSize.width + ELEMENT_MARGIN);
        }break;
        case ActinideChemGroup:
        {
            y += 8 * (ElementViewSize.height + ELEMENT_MARGIN) + TABLE_MARGIN;
            int n_el = element.index - 89; //actinide starts from 89 end by 103
            x += (3 + n_el) * (ElementViewSize.width + ELEMENT_MARGIN);
        }break;
        default:
            x -= TABLE_MARGIN + ElementViewSize.width;
            y -= TABLE_MARGIN + ElementViewSize.height;
            break;
    }
    return CGPointMake(x, y);
}

-(void)tapElementView:(UIGestureRecognizer*)gest
{
    __block ElementView* view = (ElementView*)gest.view;
    [self delegateWasSelected:view.element];
    
    __block CGAffineTransform transform = view.transform;
    [UIView animateWithDuration:0.20 animations:^{
        CGAffineTransform scale = CGAffineTransformMakeScale(1.2, 1.2);
        view.transform = scale;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.20 animations:^{
            view.transform = transform;
        }];
    }];
}

-(void)delegateWasSelected:(ChemicalElement*)el
{
    if([self.delegate respondsToSelector:@selector(periodicTable:elementWasSelected:)]){
        [self.delegate periodicTable:self elementWasSelected:el];
    }
}

-(void)drawLineFrom:(CGPoint)start toPoint:(CGPoint)end thick:(int)thick color:(UIColor*)color
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, thick);
    CGContextSetStrokeColorWithColor(context, color.CGColor);
//    CGContextMoveToPoint(context, start.x, -end.y);
//    CGContextAddLineToPoint(context, end.x, -end.y);
    [self drawLineFromX:start.x y:start.y toX:end.x toY:end.y inContext:context];
    CGContextStrokePath(context);
}

-(void)drawLineFromX:(int)x y:(int)y toX:(int)toX toY:(int)toY inContext:(CGContextRef)context
{
    CGContextMoveToPoint(context, x, -y);
    CGContextAddLineToPoint(context, toX, -toY);
}

#pragma mark - rotation

-(void)swapRotationAnimated:(BOOL)animated
{
    UIInterfaceOrientationIsLandscape(self.orientation) ? [self rotateVerticalAnimated:animated] : [self rotateHorizontalAnimated:animated];
}

-(void)rotateHorizontalAnimated:(BOOL)animated
{
    self.orientation = UIInterfaceOrientationLandscapeLeft;
    if(animated){
        [UIView animateWithDuration:ROTATE_TIME animations:^{
            [self rotateViewsSwap:NO];
        }completion:^(BOOL finished) {
            if(!_disableChangingFrameSize)
            [self delegateFrameWasChanged];
        }];
    }else {
        [self rotateViewsSwap:NO];
        if(!_disableChangingFrameSize)
        [self delegateFrameWasChanged];
    }}

-(void)rotateVerticalAnimated:(BOOL)animated
{
    self.orientation = UIInterfaceOrientationPortrait;
    if(animated){
        [UIView animateWithDuration:ROTATE_TIME animations:^{
            [self rotateViewsSwap:YES];
        } completion:^(BOOL finished) {
            if(!_disableChangingFrameSize)
            [self delegateFrameWasChanged];
        }];
    }else {
        [self rotateViewsSwap:YES];
        if(!_disableChangingFrameSize)
        [self delegateFrameWasChanged];
    }
}

-(void)rotateViewsSwap:(BOOL)swap
{
    for (int i = self.elements.count - 1; i >= 0; --i) {
        ChemicalElement* element = [self.elements objectAtIndex:i];
        CGPoint newPosit = [self positionForElement:element];
        ElementView* view = [self.elementViews objectAtIndex:i];
        view.position = swap ? CGPointMake(newPosit.y, newPosit.x) : newPosit;
    }
    [self updateContentSizeSwap:swap];
}

-(void)updateContentSizeSwap:(BOOL)swap
{
    int width = 18 * ElementViewSize.width + 17*ELEMENT_MARGIN + 2*TABLE_MARGIN;
    int height = 10 * ElementViewSize.height + 7*ELEMENT_MARGIN + 3*TABLE_MARGIN;
    self.contentSize = swap ? CGSizeMake(height, width) : CGSizeMake(width, height);
    
    if(!_disableChangingFrameSize)
    self.height = swap ? width : height;
}

-(void)delegateFrameWasChanged
{
    if([self.delegate respondsToSelector:@selector(periodicTableFrameWasChanged:)])
       [self.delegate periodicTableFrameWasChanged:self];
}

-(void)rotateToOrientation:(UIInterfaceOrientation)toOrient
{
    if (self.viewsOrientation == toOrient) return;
    [UIView animateWithDuration:ROT_VIEW_TIME animations:^{
        [self rotateViewsToOrient:toOrient];
    }];
}

-(void)rotateViewsToOrient:(UIInterfaceOrientation)toOrient
{
    CGFloat angle = 0;
    switch (toOrient) {
        case UIInterfaceOrientationLandscapeRight:angle=M_PI_2;break;
        case UIInterfaceOrientationLandscapeLeft:angle=-M_PI_2;break;
        case UIInterfaceOrientationPortrait:angle=0;break;
        case UIInterfaceOrientationPortraitUpsideDown:angle=M_PI;break;
    }
//    CGAffineTransform rotate = CGAffineTransformRotate(self.playerPreview.transform, UIInterfaceOrientationLandscapeRight ? M_PI_2 : -M_PI_2);
//    viewToRotate.transform = rotate;
    CGAffineTransform curTrans = [(UIView*)[self.elementViews lastObject] transform];
    for (UIView* view in self.elementViews) {
        view.transform = curTrans;
    }
    self.viewsOrientation = toOrient;
}


@end
