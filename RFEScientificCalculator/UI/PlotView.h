//
//  PlotView.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 04.04.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CorePlot-CocoaTouch.h>
#import "AppUtils.h"
#import "AppDefaults.h"
#import "Molecule.h"

extern  NSString* const kXAxisName;
extern  NSString* const kYAxisName;
extern  NSString* const kYAxisValue;
extern  NSString* const kYErrorValue;

@class PlotView;

@protocol PlotViewDataSource <NSObject>

- (NSArray*)dataForPlotView:(PlotView*)plotView;
//- (NSString*)labelTextForPoint:

@end

@interface PlotView : UIView<CPTPlotDataSource, CPTPlotSpaceDelegate>

@property (nonatomic, weak) id<PlotViewDataSource> dataSource;

@property (nonatomic) NSString* xAxisName;
@property (nonatomic) NSString* yAxisName;
@property (nonatomic) NSString* yAxisValueName;
- (void)reloadData;

@end
