//
//  TestViewController.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 08.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "TestViewController.h"
#import "MoleculeField.h"

@interface TestViewController ()
@property (weak, nonatomic) IBOutlet MoleculeField *moleculeField;

@end

@implementation TestViewController

- (instancetype)init
{
  self = [super initWithNibName:@"TestViewController" bundle:nil];
  if (self) {
  }
  return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
  self.navigationController.navigationBar.translucent = NO;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated {
 NSLog(@"enabled %d", self.moleculeField.userInteractionEnabled);
  [self.moleculeField becomeFirstResponder];
}

@end
