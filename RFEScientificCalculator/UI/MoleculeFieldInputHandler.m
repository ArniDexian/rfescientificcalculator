//
//  MoleculeFieldInputHandler.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 08.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "MoleculeFieldInputHandler.h"
#import "MoleculeDisplayView.h"
#import "PeriodicTable.h"

@interface MoleculeFieldInputHandler ()

@property (nonatomic) NSString* text;
@property (nonatomic) BOOL needClearText;

@end

@implementation MoleculeFieldInputHandler

- (NSString*)text {
  if (_text == nil) {
    _text = @"";
  }
  return _text;
}

#pragma mark - UIKeyInput protocol
- (BOOL)hasText {
  return (_text.length > 0) & !self.needClearText;
}

- (void)insertText:(NSString *)text {
  [self enterText:text];
}

- (void)deleteBackward {
  [self updateClearTextState];
  BOOL needDelegate = YES;
  if (self.text.length > 0) {
    self.text = [self.text substringWithRange:NSMakeRange(0, self.text.length-1)];
    [self enterText:@""];
    needDelegate = NO;
  }
  if (needDelegate) {
    [self delegateDidDeletePressed];
  }
}

#pragma mark - delegating
- (void)delegateDidEnterChemicalElement:(ChemicalElement*)element {
  if ([_delegate respondsToSelector:@selector(didEnterChemicalElement:)]) {
    [_delegate didEnterChemicalElement:element];
  }
}

- (void)delegateDidSuggestChemicalElements:(NSArray*)elements withText:(NSString*)text {
  if ([_delegate respondsToSelector:@selector(didSuggestChemicalElements:withText:)]) {
    [_delegate didSuggestChemicalElements:elements withText:text];
  }
}

- (void)delegateDidEnterDigit:(NSInteger)digit {
  if ([_delegate respondsToSelector:@selector(didEnterDigit:)]) {
    [_delegate didEnterDigit:digit];
  }
}

- (void)delegateDidDeletePressed {
  if ([_delegate respondsToSelector:@selector(didDeletePressed)]) {
    [_delegate didDeletePressed];
  }
}

- (void)delegateDidLeftBracketPressed {
  if ([_delegate respondsToSelector:@selector(didLeftBracketPressed)]) {
    [_delegate didLeftBracketPressed];
  }
}

- (void)delegateDidRightBracketPressed {
  if ([_delegate respondsToSelector:@selector(didRightBracketPressed)]) {
    [_delegate didRightBracketPressed];
  }
}

- (void)delegateDidEnterPressed {
  if ([_delegate respondsToSelector:@selector(didEnterPressed)]) {
    [_delegate didEnterPressed];
  }
}

#pragma mark - processing input
- (void)enterText:(NSString*)text {
  [self updateClearTextState];
  NSString* newText = [self.text stringByAppendingString:text];
  NSArray* elements = [[PeriodicTable sharedInstance] elementsStartingWithName:newText];
  if(elements.count > 0) {
    self.text = newText;
    if (elements.count == 1) {
      NSString* suggestedText = [((ChemicalElement*)elements.firstObject) nameStartingWith:newText];
      if ([[suggestedText lowercaseString] isEqualToString:[newText lowercaseString]]) {
        self.needClearText = YES;
        [self delegateDidEnterChemicalElement:elements.firstObject];
      } else {
        [self delegateDidSuggestChemicalElements:elements withText:newText];
      }
    } else {
      [self delegateDidSuggestChemicalElements:elements withText:newText];
    }
  } else {
    if ([newText isStringDigit]) {
      [self delegateDidEnterDigit:[newText integerValue]];
    } else if ([newText isStringLeftBracket]) {
      [self delegateDidLeftBracketPressed];
    } else if ([newText isStringRightBracket]) {
      [self delegateDidRightBracketPressed];
    } else if ([text isEqualToString:@"\n"]){
      [self delegateDidEnterPressed];
    }
  }
}

- (void)updateClearTextState {
  if(self.needClearText) {
    self.text = @"";
    self.needClearText = NO;
  }
}

- (void)clearInput {
  self.text = @"";
  self.needClearText = NO;
}

@end


















