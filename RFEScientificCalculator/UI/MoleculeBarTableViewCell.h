//
//  MoleculeBarTableViewCell.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 10.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MoleculeField.h"

@interface MoleculeBarTableViewCell : UITableViewCell <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet MoleculeField *moleculeField;
@property (weak, nonatomic) IBOutlet UITextField *barHeightField;
@property (weak, nonatomic) IBOutlet UILabel *barHeightLabel;


@end
