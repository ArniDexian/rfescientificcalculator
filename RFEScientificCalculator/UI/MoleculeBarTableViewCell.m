//
//  MoleculeBarTableViewCell.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 10.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "MoleculeBarTableViewCell.h"

@implementation MoleculeBarTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
  self.barHeightLabel.text = NSLocalizedString(@"Толщина", nil);
  self.barHeightField.delegate = self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
  NSString* resultString = [textField.text stringByReplacingCharactersInRange:range withString:string];
  if ([resultString length] != 0) {
    return [resultString isStringNumber];
  }
  return YES;
}

@end
