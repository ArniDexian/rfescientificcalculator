//
//  MoleculeFieldAccessoryView.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 08.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MoleculeFieldAccessoryView;

//@protocol MoleculeFieldAccessoryViewDataSourceDelegate <NSObject>
//@required
//- (NSUInteger)suggestsCountForMoleculeFieldAccessoryView:(MoleculeFieldAccessoryView*)accessoryView;
//- (NSString*)matchingStringForFieldAccessoryView:(MoleculeFieldAccessoryView*)accessoryView;
//- (ChemicalElement*)suggestForFieldAccessoryView:(MoleculeFieldAccessoryView*)accessoryView atIndex:(NSUInteger)suggestIndex;
//
//@end

@interface MoleculeFieldAccessoryView : UIView

+ (instancetype)accessoryView;

@property (nonatomic) NSString* text;
@property (nonatomic) NSArray* suggests;

- (void)setText:(NSString *)text andSuggests:(NSArray*)suggests;
- (ChemicalElement*)obtainCurrentSuggestedChemicalElement;

- (void)clearAccessory;

@end
