//
//  ElementView.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 24.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "ElementView.h"

#define TEXT_OFFSET 3

@interface ElementView ()

@property (nonatomic) UILabel* symbolLabel;
@property (nonatomic) UILabel* nameLabel;
@property (nonatomic) UILabel* indexLabel;
//@property (nonatomic) UILabel* atomicMass;


@end


@implementation ElementView

-(void)setElement:(ChemicalElement *)element
{
    _element = element;
    
    self.symbolLabel.text = element.symbol;
//    self.nameLabel.text = element.name;
    self.indexLabel.text = [NSString stringWithFormat:@"%i", element.index];
//    self.atomicMass.text = [NSString stringWithFormat:@"%.3f", element.atomicMass];
    
    [self updatePositions];
}

-(void)setBackgroundColor:(UIColor *)backgroundColor
{
    [super setBackgroundColor:backgroundColor];
    
    self.symbolLabel.backgroundColor = backgroundColor;
//    self.nameLabel.backgroundColor = backgroundColor;
    self.indexLabel.backgroundColor = backgroundColor;
//    self.atomicMass.backgroundColor = backgroundColor;
}

#pragma mark - init

+(id)viewWithElement:(ChemicalElement *)element
{
    ElementView* elView = [[ElementView alloc] initWithElement:element];
    return elView;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initSubviews];
    }
    return self;
}

-(id)initWithElement:(ChemicalElement *)element
{
    self = [self initWithFrame:CGRectMake(0, 0, ElementViewSize.width, ElementViewSize.height)];
    if(self){
        self.element = element;
//        self.layer.borderWidth = 1;
    }
    return self;
}



-(void)initSubviews
{
    _symbolLabel = [[UILabel alloc] init];
    _symbolLabel.font = [UIFont systemFontOfSize:ElementSymbolFontSize];
    _symbolLabel.textAlignment = NSTextAlignmentCenter;
    _symbolLabel.shadowColor = [UIColor colorWithWhite:.8 alpha:1];
    _symbolLabel.shadowOffset = CGSizeMake(0, 1);
    [self addSubview:_symbolLabel];
    
//    _nameLabel = [[UILabel alloc] init];
//    _nameLabel.font = [UIFont systemFontOfSize:ElementNameFontSize];
//    [self addSubview:_nameLabel];
    
    _indexLabel = [[UILabel alloc] init];
    _indexLabel.font = [UIFont systemFontOfSize:ElementIndexFontSize];
    [self addSubview:_indexLabel];
    
//    _atomicMass = [[UILabel alloc] init];
//    _atomicMass.font = [UIFont systemFontOfSize:ElementAtomicMassFontSize];
//    [self addSubview:_atomicMass];
    
    [self updatePositions];
}

-(void)updatePositions
{
    [self.symbolLabel sizeToFit];
//    [self.nameLabel sizeToFit];
    [self.indexLabel sizeToFit];
//    [self.atomicMass sizeToFit];
    
    self.symbolLabel.center = self.center;
//    self.nameLabel.center = self.center;
//    self.nameLabel.bottomBound = self.height - TEXT_OFFSET;
    self.indexLabel.leftBound = TEXT_OFFSET;
//    self.atomicMass.rightBound = self.width - TEXT_OFFSET;
    if(self.symbolLabel.topBound < self.indexLabel.bottomBound) self.symbolLabel.topBound = self.indexLabel.bottomBound;
}

@end

















