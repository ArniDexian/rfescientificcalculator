//
//  ElementView.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 24.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppUtils.h"
#import "ChemicalElement.h"


@interface ElementView : UIView

@property (nonatomic) ChemicalElement* element;

-(id)initWithElement:(ChemicalElement*)element;
+(id)viewWithElement:(ChemicalElement*)element;

@end
