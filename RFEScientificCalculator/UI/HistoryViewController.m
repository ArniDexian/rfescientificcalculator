//
//  HistoryViewController.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 24/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "HistoryViewController.h"
#import "SimpleResultViewController.h"

NSString* const kHistorySegueIdentifier = @"HistoryViewControllerSegueIdentifier";

@interface HistoryViewController () <HistoryDataSourceDelegate>
@property (nonatomic, strong) HistoryDataSource* dataSource;
@end

@implementation HistoryViewController

- (id)initWithStyle:(UITableViewStyle)style
{
  self = [super initWithStyle:style];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.title = NSLocalizedString(@"История", nil);
  [self setupHistoryDataSource];
  [self setupTableView];
}

- (void)setupTableView {
  self.tableView.opaque = NO;
  self.tableView.backgroundView = nil;
  self.tableView.backgroundColor = [UIColor backgroundScreenColor];
}

- (void)setupHistoryDataSource {
  self.tableView.allowsMultipleSelectionDuringEditing = NO;
  self.dataSource = [HistoryDataSource historyDataSourceWithTable:self.tableView];
  self.dataSource.delegate = self;
  self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
  self.tableView.dataSource = self.dataSource;
  self.tableView.delegate = self.dataSource;
}

- (void)viewWillAppear:(BOOL)animated {
  [self.tableView reloadData];
}

#pragma mark - history
- (void)historyDidSelectItem:(HistoryItem*)item {
  SimpleResultViewController* resultViewController = [self.storyboard instantiateViewControllerWithIdentifier:NSStringFromClass([SimpleResultViewController class])];
  [resultViewController setWithHistoryItem:item];
  [self.navigationController pushViewController:resultViewController animated:YES];
}

- (void)historyDidShowMorePressed {

}

- (void)historyDataSourceChanged {
  //  [self.historyTableView reloadData];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  // Get the new view controller using [segue destinationViewController].
  // Pass the selected object to the new view controller.
}

@end
