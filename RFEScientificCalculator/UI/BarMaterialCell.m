//
//  BarMaterialCell.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 01.10.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "BarMaterialCell.h"

@implementation BarMaterialCell
@synthesize material = _material;

-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    _moleculeDisplay.frame = CGRectMake(5, 5, self.contentView.width-10, 40);
    _materialLabel.frame = CGRectMake(5, _moleculeDisplay.bottomBound+5, 120, 30);
    int right = _materialLabel.rightBound + 5;
    _materialThicknessField.frame = CGRectMake(right, _moleculeDisplay.bottomBound+5, self.contentView.width-right-5, 30);
}

-(void)setMaterial:(BarMaterial *)material
{
    _material = material;
    
    self.moleculeDisplay.molecule = material.material;
    [[self moleculeDisplay] setNeedsDisplay];
    self.materialThicknessField.text = SPRINTF(@"%.4f", material.thickness);
}

-(BarMaterial*)material
{
    if(!_material){
        _material = [BarMaterial new];
    }
    _material.material = _moleculeDisplay.molecule;
    _material.thickness = [_materialThicknessField.text floatValue];
    return _material;
}

-(MoleculeDisplayView*)moleculeDisplay
{
    if(!_moleculeDisplay){
        _moleculeDisplay = [[MoleculeDisplayView alloc] initWithFrame:CGRectMake(5, 5, self.width-10, 40)];
        [self.contentView addSubview:_moleculeDisplay];
    }
    return _moleculeDisplay;
}

-(UILabel*)materialLabel
{
    if(!_materialLabel){
        _materialLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, self.moleculeDisplay.bottomBound+5, 120, 30)];
        _materialLabel.backgroundColor = [UIColor clearColor];
        _materialLabel.textAlignment = NSTextAlignmentRight;
        _materialLabel.font = [UIFont boldSystemFontOfSize:12];
        [self.contentView addSubview:_materialLabel];
    }
    return _materialLabel;
}

-(UITextField*)materialThicknessField
{
    if(!_materialThicknessField){
        int right = self.materialLabel.rightBound + 5;
        _materialThicknessField = [[UITextField alloc] initWithFrame:CGRectMake(right, self.moleculeDisplay.bottomBound+5, self.width-right-5, 30)];
        _materialThicknessField.borderStyle = UITextBorderStyleLine;
        _materialThicknessField.backgroundColor = [UIColor whiteColor];
        _materialThicknessField.textColor = [UIColor blueColor];
        _materialThicknessField.font = [UIFont systemFontOfSize:14];
        _materialThicknessField.textAlignment = NSTextAlignmentLeft;
//        _materialThicknessField.layer.borderWidth = 1;
        _materialThicknessField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _materialThicknessField.keyboardType = UIKeyboardTypeDecimalPad;
        [self.contentView addSubview:_materialThicknessField];
    }
    return _materialThicknessField;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        [self moleculeDisplay];
        [self materialLabel];
        [self materialThicknessField];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return [textField resignFirstResponder];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return [self isRealNumber:newString];
}

-(BOOL)isRealNumber:(NSString*)str
{
    static NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,3})?)?$";
    NSError *error = nil;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:str
                                                        options:0
                                                          range:NSMakeRange(0, [str length])];
    if (numberOfMatches == 0)
        return NO;
    return YES;
}

@end
























