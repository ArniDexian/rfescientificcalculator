//
//  ResultsViewController.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 31.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppUtils.h"
#import "AppDefaults.h"
#import "BarMaterial.h"

@interface ResultsViewController : UITableViewController

@property (nonatomic) CGFloat density;
@property (nonatomic) CGFloat energy;

///Array of substances with their thikness
@property (nonatomic) NSArray* substances;

@end
