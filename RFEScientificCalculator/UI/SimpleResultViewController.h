//
//  SimpleResultViewController.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 21/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SimpleResultViewController : UIViewController

- (void)setMolecue:(MoleculeContainer*)molecule withDensity:(NSDecimalNumber*)density formulaImage:(UIImage*)image;
- (void)setWithHistoryItem:(HistoryItem*)item;

@end
