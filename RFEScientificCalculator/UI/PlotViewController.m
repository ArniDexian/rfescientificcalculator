//
//  PlotViewController.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 03.04.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "PlotViewController.h"
#import "UIImage+Colorize.h"

@interface PlotViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *substanceImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *substanceImageViewWidthConstraint;

@end

@implementation PlotViewController

#pragma mark - properties

#pragma mark - init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
  self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  [self setupPlotView];
}

- (void)setupPlotView {
  self.plotView.xAxisName = NSLocalizedString(@"E", nil);
  self.plotView.yAxisName = NSLocalizedString(@"log(τ)", nil);
    self.plotView.yAxisValueName = NSLocalizedString(@"τ", nil);
  self.plotView.dataSource = self;
  [self.plotView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
  self.substanceImageView.image = [self.formulaImage imageWithTint:[UIColor whiteColor]];
  CGFloat offset = 20.f;
  CGFloat width =  MIN(self.formulaImage.size.width + 2 * offset, CGRectGetWidth(self.view.frame) - 2 * offset);
  self.substanceImageViewWidthConstraint.constant = ceilf(width);
  self.substanceImageView.layer.borderWidth = 1.f;
  self.substanceImageView.layer.borderColor = [UIColor elementsBlueColor].CGColor;
}

#pragma mark - data source

- (NSArray*)dataForPlotView:(PlotView *)plotView {
  NSMutableArray *contentArray = [NSMutableArray array];
  NSArray* elements = [PeriodicTable sharedInstance].elements;
  for (NSUInteger i = 0; i < elements.count; i++) {
    ChemicalElement* element = elements[i];
    NSDecimalNumber* energy = element.decEnergy;
      MoleculeAbsorptance* absRes = [self.absorptanceCalculator absorptanceOfMoleculeWithEnergy:energy];
    double abs = log10([absRes.decAbsorptance doubleValue]);
    if([absRes hasAbsorptance]) {
      [contentArray addObject:@{kXAxisName : energy,
                                kYAxisName : [NSDecimalNumber numberWithFloat:abs],
                                kYAxisValue : absRes.decAbsorptance,
                                kYErrorValue : safeDecimal(absRes.absorptanceDelta)}];
    }
  }

  return contentArray;
}

@end
























