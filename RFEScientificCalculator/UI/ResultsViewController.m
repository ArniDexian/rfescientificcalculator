//
//  ResultsViewController.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 31.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "ResultsViewController.h"
#import "PlotViewController.h"
#import "AbsorptanceCalculator.h"

@interface ResultsViewController ()

@property (nonatomic) CGFloat resAbsorpFactor;
@property (nonatomic) CGFloat resCriticalAngle;
@property (nonatomic) CGFloat resPlasmaAscill;
@property (nonatomic) CGFloat resDesiredIntensity;

@property (nonatomic, strong) AbsorptanceCalculator* absorptanceCalculator;

@end

@implementation ResultsViewController

- (id)initWithStyle:(UITableViewStyle)style {
  self = [super initWithStyle:UITableViewStyleGrouped];
  if (self) {
    // Custom initialization
  }
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  
  self.clearsSelectionOnViewWillAppear = YES;
  [self calculateGivenMolecule];
}

-(void)calculateGivenMolecule {
  BarMaterial* bar = self.substances.lastObject;
  MoleculeContainer* molecule = bar.material;
  if(molecule){
//    self.absorptanceCalculator = [AbsorptanceCalculator calculatorWithMolecule:molecule andDensity:self.density];
//    self.resAbsorpFactor = [self.absorptanceCalculator absorptanceOfMoleculeWithEnergy:self.energy].absorptance;
  }
}

-(void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  self.title = @"Результаты";
  self.navigationController.navigationBarHidden = NO;
}

-(void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  // Return the number of sections.
  return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  // Return the number of rows in the section.
  switch (section) {
    case 0: return 5;
    case 1: return 3;
    default: return 0;
      break;
  }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  static NSString *CellIdentifier = @"Cell";
  UITableViewCell *cell = nil;
  if ([tableView respondsToSelector:@selector(dequeueReusableCellWithIdentifier:)]) {
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
  }else{
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
  }
  
  if(cell == nil){
    cell = [[UITableViewCell alloc] initWithStyle:((indexPath.section == 0) ? UITableViewCellStyleValue2 : UITableViewCellStyleDefault) reuseIdentifier:CellIdentifier];
    KeyValuePair* pair = [self titleForIndexPass:indexPath];
    cell.textLabel.text = pair.key;
    cell.detailTextLabel.text = pair.value;
  }
  
  return cell;
}

-(KeyValuePair*)titleForIndexPass:(NSIndexPath*)path {
  KeyValuePair* descr = [KeyValuePair new];
  
  if(path.section == 0) {
    switch (path.row) {
      case 0:
        descr.key = NSLocalizedString(@"res1", nil);
        descr.value = [NSString stringWithFormat:@"%.4f", self.density];
        break;
      case 1:
        descr.key = NSLocalizedString(@"res2", nil);
        if (self.resAbsorpFactor == .0) {
          descr.value = @"Неизвестно";
        } else {
          descr.value = [NSString stringWithFormat:@"%.4f", self.resAbsorpFactor];
        }
        break;
      case 2:
        descr.key = NSLocalizedString(@"res3", nil);
        descr.value = [NSString stringWithFormat:@"%.4f", self.resCriticalAngle];
        break;
      case 3:
        descr.value = [NSString stringWithFormat:@"%.4f", self.resPlasmaAscill];
        descr.key = NSLocalizedString(@"res4", nil);
        break;
      case 4:
        descr.key = NSLocalizedString(@"res5", nil);
        descr.value = [NSString stringWithFormat:@"%.4f", self.resDesiredIntensity];
        break;
    }
  } else if(path.section == 1) {
    switch (path.row) {
      case 0: descr.key = NSLocalizedString(@"chart1", nil); break;
      case 1: descr.key = NSLocalizedString(@"chart2", nil); break;
      case 2: descr.key = NSLocalizedString(@"chart3", nil); break;
    }
  }
  
  return descr;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  BOOL animateDeselect = NO;
  if(indexPath.section == 1) {
    animateDeselect = YES;
    [self openPlot:(int)indexPath.row+1];
  }
  
  [self.tableView deselectRowAtIndexPath:indexPath animated:animateDeselect];
}

-(void)openPlot:(int)num {
  PlotViewController* plot = [self.view.window.rootViewController.storyboard instantiateViewControllerWithIdentifier:@"PlotViewControllerIdentifier"];
  plot.absorptanceCalculator = self.absorptanceCalculator;
  plot.title = [self titleForIndexPass:[NSIndexPath indexPathForRow:num-1 inSection:1]].key;
  
  [self.navigationController pushViewController:plot animated:YES];
}

@end














