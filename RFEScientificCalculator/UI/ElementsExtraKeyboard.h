//
//  ElementsExtraKeyboard.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 04.10.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ElementsExtraKeyboard;

@protocol ElementsExtraKeyboardDelegate<NSObject>

-(void)extraKeyboard0Pressed:(ElementsExtraKeyboard*)keyboard;
-(void)extraKeyboard1Pressed:(ElementsExtraKeyboard*)keyboard;
-(void)extraKeyboard2Pressed:(ElementsExtraKeyboard*)keyboard;
-(void)extraKeyboard3Pressed:(ElementsExtraKeyboard*)keyboard;
-(void)extraKeyboard4Pressed:(ElementsExtraKeyboard*)keyboard;
-(void)extraKeyboard5Pressed:(ElementsExtraKeyboard*)keyboard;
-(void)extraKeyboard6Pressed:(ElementsExtraKeyboard*)keyboard;
-(void)extraKeyboard7Pressed:(ElementsExtraKeyboard*)keyboard;
-(void)extraKeyboard8Pressed:(ElementsExtraKeyboard*)keyboard;
-(void)extraKeyboard9Pressed:(ElementsExtraKeyboard*)keyboard;
-(void)extraKeyboardLeftBracketPressed:(ElementsExtraKeyboard*)keyboard;
-(void)extraKeyboardRightBracketPressed:(ElementsExtraKeyboard*)keyboard;
-(void)extraKeyboardRemovePressed:(ElementsExtraKeyboard*)keyboard;
-(void)extraKeyboardRotatePressed:(ElementsExtraKeyboard*)keyboard;
-(void)extraKeyboardHidePressed:(ElementsExtraKeyboard*)keyboard;


@end

@interface ElementsExtraKeyboard : UIView

@property (nonatomic, weak) id<ElementsExtraKeyboardDelegate> delegate;

@end
