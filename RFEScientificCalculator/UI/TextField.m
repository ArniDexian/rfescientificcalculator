//
//  TextField.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 25/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "TextField.h"

typedef enum {
  SearchFieldStateUndefined = 0,
  SearchFieldActive,
  SearchFieldInactive
} SearchFieldState;

@interface TextField ()

@property (nonatomic, assign) SearchFieldState fieldState;

@end

@implementation TextField

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
      [self awakeFromNib];
    }
    return self;
}

- (void)awakeFromNib {
  [self setupTextField];
}

- (void) setupTextField {
  self.borderStyle = UITextBorderStyleNone;
  self.textColor = [UIColor whiteColor];
  self.font = [UIFont fontWithName:@"HelveticaNeue" size:17.f];
  
  self.leftViewMode = UITextFieldViewModeAlways;
  self.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 8, 30)];
  
  [self setSearchFieldState:SearchFieldInactive];
  
  [self setPlaceholder:self.placeholder];
}

- (void)setPlaceholder:(NSString *)placeholder {
  if (placeholder) {
  self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder
                                                               attributes:@{NSForegroundColorAttributeName: [UIColor colorWithWhite:0.8 alpha:1]}];
  }
}

- (void)setSearchFieldState:(SearchFieldState)state {
  if (self.fieldState != state) {
    UIImage *strImage = nil;
    switch (state) {
      case SearchFieldActive: {
        strImage = [UIImage imageNamed:@"field_small_active"];
//        self.textColor = [UIColor darkTextColor];
        self.rightViewMode = UITextFieldViewModeAlways;
        break;
      }
        
      case SearchFieldInactive: {
        strImage = [UIImage imageNamed:@"field_small"];
//        self.textColor = [UIColor lightGrayColor];
        break;
      }
      default:
        break;
    }
    
    [self setBackground:[strImage resizableImageWithCapInsets:UIEdgeInsetsMake(5.f, 5.f, 5.f, 5.f)]];
    
    self.fieldState = state;
  }
}

#pragma mark - overrides

- (BOOL)becomeFirstResponder {
  BOOL become = [super becomeFirstResponder];
  if(become) {
    [self setSearchFieldState:SearchFieldActive];
  }
  return become;
}

- (BOOL)resignFirstResponder {
  BOOL resign = [super resignFirstResponder];
  if(resign) {
    [self setSearchFieldState:SearchFieldInactive];
  }
  return resign;
}

@end
