//
//  MoleculeField.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 03.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "MoleculeField.h"
#import "BarMaterial.h"
#import "MoleculeFieldInputHandler.h"
#import "MoleculeFieldAccessoryView.h"

@interface MoleculeFieldValue ()
@property (nonatomic, strong) NSMutableArray* stack;
@property (nonatomic, assign) BOOL isBracketOpen;
@end

@implementation MoleculeFieldValue
+ (id)moleculeFieldValue {
  MoleculeFieldValue* value = [MoleculeFieldValue new];
  value.stack = [NSMutableArray array];
  value.isBracketOpen = NO;
  value.molecule = [MoleculeContainer emptyMolecule];
  return value;
}
@end


@interface MoleculeField () <MoleculeFieldInputHandlerDelegate>

@property (nonatomic) MoleculeFieldInputHandler*    inputHandler;
@property (nonatomic) MoleculeFieldAccessoryView*   inputAccessoryView;

@property (nonatomic) NSMutableArray*   stack;
@property (nonatomic) BOOL              isBracketOpen;

@end

@implementation MoleculeField

- (NSMutableArray*)stack {
  if(_stack == nil) {
    _stack = [NSMutableArray array];
  }
  return _stack;
}

- (MoleculeDisplayView*)moleculeDisplay {
  if(_moleculeDisplay == nil) {
    _moleculeDisplay = [[MoleculeDisplayView alloc] init];
    _moleculeDisplay.size = self.size;
    [self addSubview:_moleculeDisplay];
  }
  return _moleculeDisplay;
}

- (MoleculeFieldAccessoryView*)inputAccessoryView {
  if (_inputAccessoryView == nil) {
    _inputAccessoryView = [MoleculeFieldAccessoryView accessoryView];
  }
  return _inputAccessoryView;
}

- (void)setFrame:(CGRect)frame {
  [super setFrame:frame];
  self.moleculeDisplay.size = self.size;
}

#pragma mark - initialization

- (instancetype)init {
  self = [super init];
  if (self) {
    [self loadView];
  }
  return self;
}

- (void)awakeFromNib {
  [self loadView];
}

- (void)loadView {
  self.backgroundColor = [UIColor clearColor];
  _inputHandler = [MoleculeFieldInputHandler new];
  _inputHandler.delegate = self;
  [self setHeight:[self moleculeDisplay].height];
  
  UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(becomeFirstResponder)];
  [self.moleculeDisplay addGestureRecognizer:tap];
}

#pragma mark - delegating
- (void)delegateDidEnterElement:(ChemicalElement*)element {
  if ([self.delegate respondsToSelector:@selector(moleculeField:didEnterElement:)]  ) {
    [self.delegate moleculeField:self didEnterElement:element];
  }
}

- (void)delegateDidRemoveLastElement {
  if ([self.delegate respondsToSelector:@selector(moleculeFieldDidRemoveLastElement:)]  ) {
    [self.delegate moleculeFieldDidRemoveLastElement:self];
  }
}

#pragma mark - formula value

- (MoleculeFieldValue*)obtainFormulaValue {
  MoleculeFieldValue* value = [MoleculeFieldValue new];
  value.stack = self.stack;
  value.isBracketOpen = self.isBracketOpen;
  value.molecule = self.moleculeDisplay.molecule;
  return value;
}

- (void)setFormulaValue:(MoleculeFieldValue*)value {
  self.stack = value.stack;
  self.isBracketOpen = value.isBracketOpen;
  self.moleculeDisplay.molecule = value.molecule;
  [self.moleculeDisplay setNeedsDisplay];
}

#pragma mark - UIKeyInput protocol

-(BOOL)canBecomeFirstResponder {
  return YES;
}

- (BOOL)hasText {
  return [self.inputHandler hasText];
}

- (void)insertText:(NSString *)text {
  [self.inputHandler insertText:text];
}

- (void)deleteBackward {
  [self.inputHandler deleteBackward];
}

- (BOOL)becomeFirstResponder {
  BOOL firstResp = [super becomeFirstResponder];
  self.moleculeDisplay.selected = firstResp;
  return firstResp;
}

- (BOOL)resignFirstResponder {
  BOOL firstResp = [super resignFirstResponder];
  self.moleculeDisplay.selected = !firstResp;
  return firstResp;
}

#pragma mark - MoleculeFieldInputHandler protocol

- (void) didEnterChemicalElement:(ChemicalElement *)element {
  [self inputElement:element];
  [self.inputAccessoryView setText:[element nameStartingWith:self.inputHandler.text] andSuggests:@[element]];
}

- (void)didSuggestChemicalElements:(NSArray *)elements withText:(NSString *)text {
  [self.inputAccessoryView setText:text andSuggests:elements];
}

-(void)didEnterDigit:(NSInteger)digit {
  [self inputDigit:digit];
}

- (void)didDeletePressed {
  [self.inputAccessoryView clearAccessory];
  [self deleteLastInput];
  [self delegateDidRemoveLastElement];
}

-(void)didLeftBracketPressed {
  [self inputLeftBracket];
}

-(void)didRightBracketPressed {
  [self inputRightBracket];
}

-(void)didEnterPressed {
  ChemicalElement* currentElement = [self.inputAccessoryView obtainCurrentSuggestedChemicalElement];
  if(currentElement) {
    [self.inputAccessoryView setText:[currentElement nameStartingWith:self.inputAccessoryView.text] andSuggests:@[currentElement]];
    [self.inputHandler clearInput];
    [self inputElement:currentElement];
    [self delegateDidEnterElement:currentElement];
  }
}

#pragma mark - elements input logic

-(void)inputElement:(ChemicalElement*)element {
  if(!self.isBracketOpen) {
    [self.stack removeAllObjects];
  }
  
  MoleculeContainer* mol = [self createEmptyMolecule];
  mol.element = element;
  [self addToStackMolecule:mol];
  if(self.isBracketOpen) {
    [self.moleculeDisplay setNeedsDisplay];
  } else {
    [self.moleculeDisplay addMolecule:mol];
  }
}

-(void)inputDigit:(NSInteger)dig {
  MoleculeContainer* molecule = [self lastMoleculeContainer];
  if(molecule != nil) {
    molecule.quantity = [self quantityOfAtomsByAddingTo:molecule.quantity digit:(int)dig];
    [self.moleculeDisplay setNeedsDisplay];
  }
}

-(void)inputLeftBracket {
  //удалить ввод из стека - первое открытие скобки
  if(!self.isBracketOpen) {
    [self.stack removeAllObjects];
    MoleculeContainer* molec = [self createEmptyMolecule];
    [self addToStackMolecule:molec];
    [self.moleculeDisplay addMolecule:molec];
    self.isBracketOpen = YES;
    return;
  }
  
  MoleculeContainer* newMolecule = [self createEmptyMolecule];
  [self addToStackMolecule:newMolecule];
  [self.moleculeDisplay setNeedsDisplay];
}

-(void)inputRightBracket {
  if(!self.isBracketOpen || !self.stack.count) return;
  for (int i = (int)self.stack.count - 1; i >= 0; --i) {
    MoleculeContainer* iCon = [self.stack objectAtIndex:i];
    if([iCon.element isKindOfClass:[NSMutableArray class]] && iCon.quantity == -1) {
      //if the array is empty
      if([(NSMutableArray*)iCon.element count] == 0) {
        [self removeMoleculeFromStackAndDisplay:iCon];
        if(self.stack.count < 1) {
          self.isBracketOpen = NO;
        }
        return;
      }
      iCon.quantity = 0;
      
      if(i + 1 < self.stack.count) {
        [self.stack removeObjectsInRange:NSMakeRange(i+1, self.stack.count-(i+1))];
      }
      
      if(self.stack.count <= 1) {
        self.isBracketOpen = NO;
      }
      break;
    }
  }
  [self.moleculeDisplay setNeedsDisplay];
}

- (void)deleteLastInput {
  MoleculeContainer* lastStack = [self.stack lastObject];
  if(lastStack != nil) {
    [self removeMoleculeFromStackAndDisplay:lastStack];
  } else {
    [self.moleculeDisplay removeLast];
  }
  
  //check if it's last bracket
  if(self.stack.count == 0 || [self lastOpenedMoleculeContainer] == nil){
    self.isBracketOpen = NO;
  }
}

//result - was it added to other molecule
-(BOOL)addToStackMolecule:(MoleculeContainer*)molec {
  [self.stack addObject:molec];
  if(self.stack.count != 0) {
    for (int i = (int)self.stack.count - 2; i >= 0; --i) {
      MoleculeContainer* iCon = [self.stack objectAtIndex:i];
      if([iCon.element isKindOfClass:[NSMutableArray class]]){
        if(iCon.quantity == -1){
          [(NSMutableArray*)iCon.element addObject:molec];
          return YES;
        }
      }
    }
  }
  return NO;
}

-(MoleculeContainer*)createEmptyMolecule {
  MoleculeContainer* mol = [MoleculeContainer new];
  mol.element = [NSMutableArray array];
  mol.quantity = -1;
  return mol;
}

-(MoleculeContainer*)lastOpenedMoleculeContainer {
  if(self.stack.count != 0) {
    MoleculeContainer* mol = nil;
    for (int i = (int)self.stack.count - 1; i >= 0; --i) {
      MoleculeContainer* iCont = [self.stack objectAtIndex:i];
      if([iCont.element isKindOfClass:[NSMutableArray class]] && iCont.quantity == -1){
        mol = iCont;
        break;
      }
    }
    return mol;
  }
  return nil;
}

//last visible molecule
-(MoleculeContainer*)lastMoleculeContainer {
  if(self.stack.count != 0) {
    MoleculeContainer* mol = [self.stack lastObject];
    for (int i = (int)self.stack.count - 1; i >= 0; --i) {
      MoleculeContainer* iCont = [self.stack objectAtIndex:i];
      if([iCont.element isKindOfClass:[NSMutableArray class]]){
        //if we try to set quant to zero count array - remove it
        if([(NSMutableArray*)iCont.element count] == 0){
          [self removeMoleculeFromStackAndDisplay:iCont];
          if ([self lastOpenedMoleculeContainer] == nil) {
            self.isBracketOpen = NO;
          }
          mol = nil;
          break;
        }
        
        if(![[(NSMutableArray*)iCont.element lastObject] isEqual:self.stack.lastObject]){
          break;
        }
        
        if(iCont.quantity != -1){
          mol = iCont;
        }
        break;
      }
    }
    return mol;
  }
  return nil;
}

-(void)removeMoleculeFromStackAndDisplay:(MoleculeContainer*)cont {
  [self.moleculeDisplay removeMolecule:cont];
  [self removeMolecule:cont fromArray:self.stack];
}

-(void)removeMolecule:(MoleculeContainer*)molecToRemove fromArray:(NSMutableArray*)array {
  if(array.count != 0){
    if([array containsObject:molecToRemove])
      [array removeObject:molecToRemove];
    for (MoleculeContainer* molec in array) {
      if([molec.element isKindOfClass:[NSMutableArray class]])
        [self removeMolecule:molecToRemove fromArray:molec.element];
    }
  }
}

-(int)quantityOfAtomsByAddingTo:(int)quant digit:(int)dig {
  if(quant <= 0) {
    quant = 0;
  }
  if(quant < 1){
    quant = dig;
  } else {
    quant = [[NSString stringWithFormat:@"%i%i", quant, dig] intValue];
  }
  if(quant > 999){
    quant = 999;
  }
  return quant;
}

- (void)clear {
  [self.stack removeAllObjects];
  [self.inputHandler clearInput];
  [self.moleculeDisplay removeAll];
}

@end









