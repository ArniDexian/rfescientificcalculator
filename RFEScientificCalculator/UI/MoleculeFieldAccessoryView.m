//
//  MoleculeFieldAccessoryView.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 08.04.2014.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "MoleculeFieldAccessoryView.h"

@interface MoleculeFieldAccessoryView ()

@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *leftButton;
@property (weak, nonatomic) IBOutlet UIButton *rightButton;

@property (nonatomic) NSUInteger selectedIndex;

- (IBAction)leftButtonAction:(id)sender;
- (IBAction)rightButtonAction:(id)sender;

@end

@implementation MoleculeFieldAccessoryView

+ (instancetype)accessoryView {
  id view = [[[NSBundle mainBundle] loadNibNamed:@"MoleculeFieldAccessoryView" owner:self options:nil] lastObject];
  return view;
}

- (id)initWithFrame:(CGRect)frame {
  self = [super initWithFrame:frame];
  if (self) {
    // Initialization code
  }
  return self;
}

- (void)setText:(NSString *)text {
  _text = text;
  [self updateData];
}

- (void)setSuggests:(NSArray *)suggests {
  _suggests = suggests;
  [self updateData];
}

- (void)setText:(NSString *)text andSuggests:(NSArray *)suggests {
  _text = text ? text : @"";
  _suggests = suggests;
  [self updateData];
}

- (IBAction)leftButtonAction:(id)sender {
  [self showNextWithDirection:-1];
}

- (IBAction)rightButtonAction:(id)sender {
  [self showNextWithDirection:1];
}

- (void)updateButtonsVisibility {
  self.leftButton.hidden = !(self.suggests.count && self.selectedIndex > 0);
  self.rightButton.hidden = !(self.suggests.count && self.selectedIndex < self.suggests.count - 1);
}

- (void)updateData {
  [self updateSelectedIndex];
  [self updateButtonsVisibility];
  [self showNextWithDirection:0];
}

- (void)updateSelectedIndex {
  NSString* inputText = self.textField.text;
  if(inputText.length > 0 && self.suggests.count &&
     [inputText rangeOfString:self.text options:(NSAnchoredSearch | NSCaseInsensitiveSearch)].length == 0) {
    NSInteger index = [self.suggests indexOfObject:inputText];
    if (index != NSNotFound) {
      self.selectedIndex = index;
    }
  } else {
    self.selectedIndex = 0;
  }
}

- (void)showNextWithDirection:(NSInteger)direct {
  NSInteger nextInd = self.selectedIndex + direct;
  if(nextInd >= 0 && nextInd < self.suggests.count) {
    ChemicalElement* element = self.suggests[nextInd];
    
    NSString* startText = self.text;
    NSString* text = [element nameStartingWith:self.text];
    NSRange  highlightedRange;
    if (text) {
      NSRange startRange = [text rangeOfString:startText options:NSCaseInsensitiveSearch];
      NSInteger startRangeLocation = startRange.location + startRange.length;
      highlightedRange = NSMakeRange(startRangeLocation, text.length - startRangeLocation);
    } else {
      text = self.text;
      highlightedRange = NSMakeRange(0, 0);
    }
    
    [self setText:text withHighlightedRange:highlightedRange];
    self.selectedIndex = nextInd;
    [self updateButtonsVisibility];
  }
}

- (void)setText:(NSString *)text withHighlightedRange:(NSRange)highlightedRange {
  if (highlightedRange.length != 0 && text.length >= highlightedRange.length) {
    NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString:text];
    [attrString addAttribute:NSBackgroundColorAttributeName value:[UIColor yellowColor] range:highlightedRange];
    self.textField.attributedText = attrString;
  } else {
    self.textField.text = text;
  }
}

- (ChemicalElement *)obtainCurrentSuggestedChemicalElement {
  if (self.selectedIndex < self.suggests.count) {
    return self.suggests[self.selectedIndex];
  }
  return nil;
}

- (void)clearAccessory {
  self.textField.text = @"";
  self.text = @"";
  self.leftButton.hidden = YES;
  self.rightButton.hidden = YES;
}

//#pragma mark - data source protocol
//- (NSUInteger)obtainSuggestCount {
//  if ([_dataSource respondsToSelector:@selector(suggestsCountForMoleculeFieldAccessoryView:)]) {
//    return [_dataSource suggestsCountForMoleculeFieldAccessoryView:self];
//  }
//  return 0;
//}
//
//- (NSString*)obtainMatchingString {
//  if ([_dataSource respondsToSelector:@selector(matchingStringForFieldAccessoryView:)]) {
//    return [_dataSource matchingStringForFieldAccessoryView:self];
//  }
//  return nil;
//}
//
//- (ChemicalElement*)obtainSuggestForIndex:(NSUInteger)index {
//  if ([_dataSource respondsToSelector:@selector(suggestForFieldAccessoryView:atIndex:)]) {
//    return [_dataSource suggestForFieldAccessoryView:self atIndex:index];
//  }
//  return nil;
//}

@end








