//
//  HelpViewController.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 24/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "HelpViewController.h"

NSString* const kHelpSegueIdentifier = @"HelpViewControllerSegueIdentifier";

@interface HelpViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation HelpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  self.navigationController.navigationBar.translucent = NO;
  [self setupUI];
}

- (void)viewWillAppear:(BOOL)animated {
  [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:[Bundle pathForResource:@"abs" ofType:@".pdf"]]]];
}

-(void)viewDidDisappear:(BOOL)animated {
  [self.webView goBack];
}

- (void)setupUI {
  self.title = NSLocalizedString(@"Помощь", nil);
  self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Закрыть", nil)
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self action:@selector(closeAction:)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - actions 

- (void)closeAction:(id)sender {
  [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
