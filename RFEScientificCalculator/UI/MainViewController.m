//
//  MainViewController.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 24.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "MainViewController.h"
#import "AppUtils.h"
#import "ResultsViewController.h"
#import "AbsorptanceTable.h"
#import "PeriodicTable.h"

#define BUTTONS_OFFSET 5
#define DIG_BTN_SIZE CGSizeMake(24, 36)
#define DISPLAY_OFFSET 10
#define CAPTION_FONT_SIZE 12
#define MAX_PRECISION_LEN 9

static int TOOLBAR_HEIGHT = 44;

const BOOL DIGIT_IS_SCALABLE = NO;

@interface MainViewController (){
    CGSize kbSize;
    UITextField* activeField;
    BOOL keyboardIsShown;
}

@property (nonatomic) UIScrollView* scrollView;
@property (nonatomic) PeriodicTableView* periodicTable;

@property (nonatomic) UIView* buttonsView;
@property (nonatomic) UIButton* dig0;
@property (nonatomic) UIButton* dig1;
@property (nonatomic) UIButton* dig2;
@property (nonatomic) UIButton* dig3;
@property (nonatomic) UIButton* dig4;
@property (nonatomic) UIButton* dig5;
@property (nonatomic) UIButton* dig6;
@property (nonatomic) UIButton* dig7;
@property (nonatomic) UIButton* dig8;
@property (nonatomic) UIButton* dig9;
@property (nonatomic) UIButton* leftBracket;
@property (nonatomic) UIButton* rightBracket;
@property (nonatomic) UIButton* removeBtn;
@property (nonatomic) UIButton* rotateBtn;
@property (nonatomic) UIButton* hideBtn;

@property (nonatomic) NSMutableArray* stack;
@property (nonatomic) BOOL isBracketOpen;

@property (nonatomic) UILabel* firstFormulaLabel;
@property (nonatomic) UILabel* secondFormulaLabel;

@property (nonatomic) MoleculeDisplayView* substance1Display;
@property (nonatomic) MoleculeDisplayView* substance2Display;
@property (nonatomic) NSMutableArray* substance1Stack;
@property (nonatomic) NSMutableArray* substance2Stack;
@property (nonatomic) BOOL subst1isBracketOpen;
@property (nonatomic) BOOL subst2isBracketOpen;

@property (nonatomic) UILabel* densityLabel;
@property (nonatomic) UITextField* densityField;
@property (nonatomic) UILabel* energyLabel;
@property (nonatomic) UITextField* energyField;
@property (nonatomic) UILabel* thick1Label;
@property (nonatomic) UILabel* thick2Label;
@property (nonatomic) UITextField* thick1Field;
@property (nonatomic) UITextField* thick2Field;

@property (nonatomic) UIButton* viewResultButton;

@end

@implementation MainViewController

#pragma mark - properties

-(UIScrollView*)scrollView
{
    if(_scrollView == nil){
        _scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
        _scrollView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1];
        [self setView:_scrollView];
    }
    return _scrollView;
}

-(UILabel*)firstFormulaLabel
{
    if(_firstFormulaLabel == nil){
        _firstFormulaLabel = [self createCaptionLabelWithText:NSLocalizedString(@"1st subst formula", nil)];
        _firstFormulaLabel.position = CGPointMake(DISPLAY_OFFSET, DISPLAY_OFFSET);
        [self.view addSubview:_firstFormulaLabel];
    }
    return _firstFormulaLabel;
}

-(MoleculeDisplayView*)substance1Display
{
    if(_substance1Display == nil){
        _substance1Display = [[MoleculeDisplayView alloc] initWithFrame:CGRectMake(DISPLAY_OFFSET, self.firstFormulaLabel.bottomBound, self.view.width-2*DISPLAY_OFFSET, 40)];
        _substance1Display.delegate = self;
        [self.view addSubview:_substance1Display];
    }
    return _substance1Display;
}

-(UILabel*)secondFormulaLabel
{
    if(_secondFormulaLabel == nil){
        _secondFormulaLabel = [self createCaptionLabelWithText:NSLocalizedString(@"2nd subst formula", nil)];
        _secondFormulaLabel.position = CGPointMake(DISPLAY_OFFSET, self.substance1Display.bottomBound + DISPLAY_OFFSET);
        [self.view addSubview:_secondFormulaLabel];
    }
    return _secondFormulaLabel;
}

-(MoleculeDisplayView*)substance2Display
{
    if(_substance2Display == nil){
        _substance2Display = [[MoleculeDisplayView alloc] initWithFrame:CGRectMake(DISPLAY_OFFSET, self.secondFormulaLabel.bottomBound, self.view.width-2*DISPLAY_OFFSET, 40)];
        _substance2Display.delegate = self;
        [self.view addSubview:_substance2Display];
    }
    return _substance2Display;
}

-(NSMutableArray*)stack
{
    return [self.moleculeDisplay isEqual:self.substance2Display] ? self.substance2Stack : self.substance1Stack;
}

-(NSMutableArray*)substance1Stack
{
    if(_substance1Stack == nil) _substance1Stack = [NSMutableArray array];
    return _substance1Stack;
}

-(NSMutableArray*)substance2Stack
{
    if(_substance2Stack == nil) _substance2Stack = [NSMutableArray array];
    return _substance2Stack;
}

-(BOOL)isBracketOpen
{
    return [self.moleculeDisplay isEqual:self.substance2Display] ? self.subst2isBracketOpen : self.subst1isBracketOpen;
}

-(void)setIsBracketOpen:(BOOL)isBracketOpen
{
    [self.moleculeDisplay isEqual:self.substance2Display] ? (self.subst2isBracketOpen = isBracketOpen) : (self.subst1isBracketOpen = isBracketOpen);
}

-(UILabel*) densityLabel
{
    if(_densityLabel == nil){
        _densityLabel = [self createCaptionLabelWithText:NSLocalizedString(@"elements density", nil)];
        _densityLabel.position = CGPointMake(DISPLAY_OFFSET, self.substance2Display.bottomBound + DISPLAY_OFFSET);
        [self.view addSubview:_densityLabel];
    }
    return _densityLabel;
}

-(UITextField*) densityField
{
    if(_densityField == nil){
        _densityField = [self createTextField];
        _densityField.position = CGPointMake(DISPLAY_OFFSET, self.densityLabel.bottomBound);
        [self.view addSubview:_densityField];
    }
    return _densityField;
}

-(UILabel*) energyLabel
{
    if(_energyLabel == nil){
        _energyLabel = [self createCaptionLabelWithText:NSLocalizedString(@"foton energy", nil)];
        _energyLabel.position = CGPointMake(DISPLAY_OFFSET, self.densityField.bottomBound + DISPLAY_OFFSET);
        [self.view addSubview:_energyLabel];
    }
    return _energyLabel;
}

-(UITextField*) energyField
{
    if(_energyField == nil){
        _energyField = [self createTextField];
        _energyField.position = CGPointMake(DISPLAY_OFFSET, self.energyLabel.bottomBound);
        [self.view addSubview:_energyField];
    }
    return _energyField;
}

-(UILabel*) thick1Label
{
    if(_thick1Label == nil){
        _thick1Label = [self createCaptionLabelWithText:NSLocalizedString(@"1st thick", nil)];
        _thick1Label.position = CGPointMake(DISPLAY_OFFSET, self.energyField.bottomBound + DISPLAY_OFFSET);
        [self.view addSubview:_thick1Label];
    }
    return _thick1Label;
}

-(UITextField*) thick1Field
{
    if(_thick1Field == nil){
        _thick1Field = [self createTextField];
        _thick1Field.position = CGPointMake(DISPLAY_OFFSET, self.thick1Label.bottomBound);
        [self.view addSubview:_thick1Field];
    }
    return _thick1Field;
}

-(UILabel*) thick2Label
{
    if(_thick2Label == nil){
        _thick2Label = [self createCaptionLabelWithText:NSLocalizedString(@"2nd thick", nil)];
        _thick2Label.position = CGPointMake(DISPLAY_OFFSET, self.thick1Field.bottomBound + DISPLAY_OFFSET);
        [self.view addSubview:_thick2Label];
    }
    return _thick2Label;
}

-(UITextField*) thick2Field
{
    if(_thick2Field == nil){
        _thick2Field = [self createTextField];
        _thick2Field.returnKeyType = UIReturnKeyDone;
        _thick2Field.position = CGPointMake(DISPLAY_OFFSET, self.thick2Label.bottomBound);
        [self.view addSubview:_thick2Field];

    }
    return _thick2Field;
}

-(PeriodicTableView*)periodicTable
{
    if(_periodicTable == nil){
        _periodicTable = [[PeriodicTableView alloc] initWithFrame:CGRectMake(0, self.buttonsView.bottomBound + BUTTONS_OFFSET, self.view.width, self.view.height - self.substance2Display.bottomBound)];
        _periodicTable.delegate = self;
        _periodicTable.hidden = YES;
        [self.view addSubview:_periodicTable];
    }
    return _periodicTable;
}

-(UIButton*)viewResultButton
{
    if(_viewResultButton == nil){
        _viewResultButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _viewResultButton.frame = CGRectMake((self.view.width - 300)/2, self.thick2Field.bottomBound + DISPLAY_OFFSET, 300, 40);
        UIEdgeInsets caps = UIEdgeInsetsMake(4, 4, 4, 4);
        _viewResultButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
        [_viewResultButton setBackgroundImage:[[UIImage imageNamed:@"blue_btn"] resizableImageWithCapInsets:caps] forState:UIControlStateNormal];
        [_viewResultButton setBackgroundImage:[[UIImage imageNamed:@"blue_btn_sel"] resizableImageWithCapInsets:caps] forState:UIControlStateHighlighted];
        [_viewResultButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [_viewResultButton setTitle:NSLocalizedString(@"view_result", nil) forState:UIControlStateNormal];
        [_viewResultButton addTarget:self action:@selector(viewResultAction:) forControlEvents:UIControlEventTouchDown];
        [self.view addSubview:_viewResultButton];
    }
    return _viewResultButton;
}

#pragma mark - initialization
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        NSLog(@"initWithNibName");
        [self registerForKeyboardNotifications];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self scrollView];
    
    self.title = @"Вещество";
    
	// Do any additional setup after loading the view.
    [PeriodicTable sharedInstance];
    [self initButtons];
    [self periodicTable];
//    [self thick2Field];
    [self updatePositions];
//    [self viewResultButton];
    
    [AbsorptanceTable sharedInstance];
}

-(void)initButtons
{
    self.buttonsView = [[UIView alloc] initWithFrame:CGRectMake(0, self.substance2Display.bottomBound + BUTTONS_OFFSET, 0, 0)];
    self.buttonsView.hidden = YES;
    self.dig0 = [self createButtonWithText:@"0"];
    [self setPositionForButton:self.dig0 afterButton:nil];
    [self.dig0 addTarget:self action:@selector(dig0Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig1 = [self createButtonWithText:@"1"];
    [self setPositionForButton:self.dig1 afterButton:self.dig0];
    [self.dig1 addTarget:self action:@selector(dig1Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig2 = [self createButtonWithText:@"2"];
    [self setPositionForButton:self.dig2 afterButton:self.dig1];
    [self.dig2 addTarget:self action:@selector(dig2Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig3 = [self createButtonWithText:@"3"];
    [self setPositionForButton:self.dig3 afterButton:self.dig2];
    [self.dig3 addTarget:self action:@selector(dig3Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig4 = [self createButtonWithText:@"4"];
    [self setPositionForButton:self.dig4 afterButton:self.dig3];
    [self.dig4 addTarget:self action:@selector(dig4Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig5 = [self createButtonWithText:@"5"];
    [self setPositionForButton:self.dig5 afterButton:self.dig4];
    [self.dig5 addTarget:self action:@selector(dig5Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig6 = [self createButtonWithText:@"6"];
    [self setPositionForButton:self.dig6 afterButton:self.dig5];
    [self.dig6 addTarget:self action:@selector(dig6Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig7 = [self createButtonWithText:@"7"];
    [self setPositionForButton:self.dig7 afterButton:self.dig6];
    [self.dig7 addTarget:self action:@selector(dig7Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig8 = [self createButtonWithText:@"8"];
    [self setPositionForButton:self.dig8 afterButton:self.dig7];
    [self.dig8 addTarget:self action:@selector(dig8Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.dig9 = [self createButtonWithText:@"9"];
    [self setPositionForButton:self.dig9 afterButton:self.dig8];
    [self.dig9 addTarget:self action:@selector(dig9Clicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.leftBracket = [self createButtonWithText:@"["];
    [self setPositionForButton:self.leftBracket afterButton:self.dig9];
    [self.leftBracket addTarget:self action:@selector(leftBracketClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.rightBracket = [self createButtonWithText:@"]"];
    [self setPositionForButton:self.rightBracket afterButton:self.leftBracket];
    [self.rightBracket addTarget:self action:@selector(rightBracketClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.removeBtn = [self createButtonWithText:@"<-"];
    self.removeBtn.width = 60;
    [self setPositionForButton:self.removeBtn afterButton:self.rightBracket];
    self.removeBtn.rightBound = self.view.rightBound - DISPLAY_OFFSET;
    [self.removeBtn addTarget:self action:@selector(removeClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    if([AppUtils isIPhone]){
        self.rotateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.rotateBtn.size = CGSizeMake(DIG_BTN_SIZE.height, DIG_BTN_SIZE.height);
        self.rotateBtn.rightBound = self.removeBtn.leftBound - BUTTONS_OFFSET;
        self.rotateBtn.topBound = self.removeBtn.topBound;
        [self.rotateBtn setBackgroundImage:[[UIImage imageNamed:@"rotate_btn_sel"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
        [self.rotateBtn setBackgroundImage:[[UIImage imageNamed:@"rotate_btn"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
        [self.rotateBtn addTarget:self action:@selector(rotateAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.buttonsView addSubview:self.rotateBtn];
    }
    
    self.hideBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.hideBtn.size = CGSizeMake(DIG_BTN_SIZE.height, DIG_BTN_SIZE.height);
    self.hideBtn.rightBound = ([AppUtils isIPhone] ? self.rotateBtn.leftBound : self.removeBtn.leftBound) - BUTTONS_OFFSET;
    self.hideBtn.topBound = self.removeBtn.topBound;
    [self.hideBtn setBackgroundImage:[[UIImage imageNamed:@"hide_btn_sel"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
    [self.hideBtn setBackgroundImage:[[UIImage imageNamed:@"hide_btn"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
    [self.hideBtn addTarget:self action:@selector(hideAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonsView addSubview:self.hideBtn];
    self.buttonsView.size = CGSizeMake(self.view.width, self.hideBtn.bottomBound - self.dig0.topBound);
    [self.view addSubview:self.buttonsView];
    
    NSLog(@"%@", NSStringFromCGRect(self.buttonsView.frame));
}

-(UIButton*)createButtonWithText:(NSString*)text
{
    UIFont* font = [UIFont boldSystemFontOfSize:20];
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.size = DIG_BTN_SIZE;
    button.titleLabel.font = font;
    [button setTitle:text forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setBackgroundImage:[[UIImage imageNamed:@"dig_bkg_dark"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateNormal];
    [button setBackgroundImage:[[UIImage imageNamed:@"dig_bkg_dark_sel"] resizableImageWithCapInsets:UIEdgeInsetsMake(5, 5, 5, 5)] forState:UIControlStateHighlighted];
    [self.buttonsView addSubview:button];
    return button;
}

-(void)setPositionForButton:(UIButton*)btn afterButton:(UIButton*)lastBtn
{
    int width = self.view.width;
    int oX = !lastBtn ? DISPLAY_OFFSET : lastBtn.rightBound + BUTTONS_OFFSET;
    int oY = !lastBtn ? BUTTONS_OFFSET : lastBtn.topBound;
    
    if((oX + btn.width + BUTTONS_OFFSET) > width){
        oX = DISPLAY_OFFSET;
        oY = lastBtn.bottomBound + BUTTONS_OFFSET;
    }
    
    btn.position = CGPointMake(oX, oY);
    
}

-(UILabel*)createCaptionLabelWithText:(NSString*)text
{
    UILabel* label = [self createCaptionLabel];
    label.text = text;
//    [AppUtils adjustLabelWidth:label withMargin:0];
    [label sizeToFit];
    
    return label;
}

-(UILabel*)createCaptionLabel
{
    UILabel* label = [[UILabel alloc] init];
    label.font = [UIFont systemFontOfSize:CAPTION_FONT_SIZE];
    label.textColor = [UIColor darkGrayColor];
    label.shadowColor = [UIColor colorWithWhite:0.85 alpha:1];
    label.shadowOffset = CGSizeMake(0, -1);
    label.backgroundColor = [UIColor clearColor];
    return label;
}

-(UITextField*)createTextField
{
    UITextField* field = [[UITextField alloc] init];
    field.layer.cornerRadius = 5;
    field.layer.borderColor = [UIColor grayColor].CGColor;
    field.layer.borderWidth = 1;
    field.backgroundColor = [UIColor whiteColor];
    field.font = [UIFont systemFontOfSize:16];
    field.layer.sublayerTransform = CATransform3DMakeTranslation(5, 0, 0);
    field.autocorrectionType = UITextAutocorrectionTypeNo;
    field.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    field.returnKeyType = UIReturnKeyNext;
//    field.enablesReturnKeyAutomatically = NO;
//    field.returnKeyType = UIReturnKeyDone;
    field.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    field.delegate = self;
    
    field.size = CGSizeMake(320-2*DISPLAY_OFFSET, 40);
    return field;
}

-(void)viewDidUnload
{
    [super viewDidUnload];
    [self unregisterForKeyboardNotifications];
    self.dig0 = nil;
    self.dig1 = nil;
    self.dig2 = nil;
    self.dig3 = nil;
    self.dig4 = nil;
    self.dig5 = nil;
    self.dig6 = nil;
    self.dig7 = nil;
    self.dig8 = nil;
    self.dig9 = nil;
    self.leftBracket = nil;
    self.rightBracket = nil;
    self.removeBtn = nil;
    self.substance1Display = nil;
    self.substance2Display = nil;
    self.substance1Stack = nil;
    self.substance2Stack = nil;
    self.hideBtn = nil;
    self.rotateBtn = nil;
    self.buttonsView = nil;
    self.densityLabel = nil;
    self.densityField = nil;
    self.energyLabel = nil;
    self.energyField = nil;
    self.thick1Label = nil;
    self.thick2Label = nil;
    self.thick1Field = nil;
    self.thick2Field = nil;
    self.firstFormulaLabel = nil;
    self.secondFormulaLabel = nil;
    self.firstFormulaLabel = nil;
    self.secondFormulaLabel = nil;
    self.scrollView = nil;
    activeField = nil;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBarHidden = YES;
}

#pragma rotation

-(BOOL)shouldAutorotate
{
    return YES;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [super willRotateToInterfaceOrientation:toInterfaceOrientation duration:duration];
//    [self.periodicTable rotateToOrientation:toInterfaceOrientation];
}

-(void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];
    [self updateSizes];
}

#pragma  mark - delegation

-(void)updateSizes
{
    self.periodicTable.width = self.view.width;
}

-(void)updatePositions
{
    if(self.moleculeDisplay == self.substance1Display){
        self.buttonsView.topBound = self.substance1Display.bottomBound + DISPLAY_OFFSET;
        self.periodicTable.topBound = self.buttonsView.bottomBound + DISPLAY_OFFSET;
        self.secondFormulaLabel.topBound = (self.periodicTable.hidden ? self.substance1Display.bottomBound : self.periodicTable.bottomBound) + DISPLAY_OFFSET;
        self.substance2Display.topBound = self.secondFormulaLabel.bottomBound;
        self.densityLabel.position = CGPointMake(DISPLAY_OFFSET, self.substance2Display.bottomBound + DISPLAY_OFFSET);
    }else{
        self.secondFormulaLabel.topBound = self.substance1Display.bottomBound + DISPLAY_OFFSET;
        self.substance2Display.topBound = self.secondFormulaLabel.bottomBound;
        self.buttonsView.topBound = self.substance2Display.bottomBound + DISPLAY_OFFSET;
        self.periodicTable.topBound = self.buttonsView.bottomBound + DISPLAY_OFFSET;
        self.densityLabel.position = CGPointMake(DISPLAY_OFFSET, (self.periodicTable.hidden ? self.substance2Display.bottomBound : self.periodicTable.bottomBound) + DISPLAY_OFFSET);
    }
    
//    self.densityLabel.position = CGPointMake(DISPLAY_OFFSET, (self.periodicTable.hidden ? self.substance2Display.bottomBound : self.periodicTable.bottomBound) + DISPLAY_OFFSET);
    self.densityField.position = CGPointMake(DISPLAY_OFFSET, self.densityLabel.bottomBound);
    self.energyLabel.position = CGPointMake(DISPLAY_OFFSET, self.densityField.bottomBound + DISPLAY_OFFSET);
    self.energyField.position = CGPointMake(DISPLAY_OFFSET, self.energyLabel.bottomBound);
    
    if([AppUtils isIPad]){
        int oY = self.view.width - self.thick1Field.width - DISPLAY_OFFSET;
        self.thick1Label.position = CGPointMake(oY, self.densityLabel.topBound);
        self.thick1Field.position = CGPointMake(oY, self.thick1Label.bottomBound);
        self.thick2Label.position = CGPointMake(oY, self.thick1Field.bottomBound + DISPLAY_OFFSET);
        self.thick2Field.position = CGPointMake(oY, self.thick2Label.bottomBound);
    }else{
        self.thick1Label.position = CGPointMake(DISPLAY_OFFSET, self.energyField.bottomBound + DISPLAY_OFFSET);
        self.thick1Field.position = CGPointMake(DISPLAY_OFFSET, self.thick1Label.bottomBound);
        self.thick2Label.position = CGPointMake(DISPLAY_OFFSET, self.thick1Field.bottomBound + DISPLAY_OFFSET);
        self.thick2Field.position = CGPointMake(DISPLAY_OFFSET, self.thick2Label.bottomBound);
    }
    
    self.viewResultButton.position = CGPointMake((self.view.width - 300)/2, self.thick2Field.bottomBound + DISPLAY_OFFSET);
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width, self.viewResultButton.bottomBound + DISPLAY_OFFSET);
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self returnKeyDoAction];
//    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    BOOL checkOffset = (activeField != textField && keyboardIsShown);
    activeField = textField;
    if(checkOffset){
        [self updateActiveFieldOffset];
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    activeField = nil;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger oldLength = [textField.text length];
    NSUInteger replacementLength = [string length];
    NSUInteger rangeLength = range.length;
    
    NSUInteger newLength = oldLength - rangeLength + replacementLength;
    
    BOOL returnKey = [string rangeOfString: @"\n"].location != NSNotFound;
    
    if(returnKey){
        [self returnKeyDoAction];
    }
    
    BOOL canEdit = newLength <= MAX_PRECISION_LEN || returnKey;
    
    if(!canEdit) return NO;
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    return [self isRealNumber:newString];
}

-(void)returnKeyDoAction
{
    if(activeField == self.densityField) [self.energyField becomeFirstResponder];
    else if(activeField == self.energyField) [self.thick1Field becomeFirstResponder];
    else if(activeField == self.thick1Field) [self.thick2Field becomeFirstResponder];
    else [self viewResultAction:nil];
}

-(BOOL)isRealNumber:(NSString*)str
{
    static NSString *expression = @"^([0-9]+)?(\\.([0-9]{1,3})?)?$";
    NSError *error = nil;
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:str
                                                        options:0
                                                          range:NSMakeRange(0, [str length])];
    if (numberOfMatches == 0)
        return NO;
    return YES;
}

-(void)periodicTable:(PeriodicTableView *)table elementWasSelected:(ChemicalElement *)element
{
    [self inputElement:element];
}

-(void)periodicTableFrameWasChanged:(PeriodicTableView *)table
{
    [self updateSizes];
    [UIView animateWithDuration:0.5 animations:^{
    [self updatePositions];
    }];
}

-(void)moleculeDisplayDidBeginEditing:(MoleculeDisplayView *)molecDispl
{
    [activeField resignFirstResponder];
    self.moleculeDisplay = molecDispl;
    self.substance1Display.selected = NO;
    self.substance2Display.selected = NO;
    
    self.moleculeDisplay.selected = YES;
    [self showPeriodicTable];
}

#pragma mark - interaction

-(void)hideAction:(id)sender
{
    self.moleculeDisplay.selected = NO;
    [self hidePeriodicTable];
}

-(void)showPeriodicTable
{
//    if(self.periodicTable.hidden){
//        [UIView animateWithDuration:0.5 animations:^{
            self.periodicTable.hidden = NO;
            self.buttonsView.hidden = NO;
            [self updatePositions];
//        } completion:^(BOOL finished) {
//            
//        }];
//    }
}

-(void)hidePeriodicTable
{
    [UIView animateWithDuration:0.5 animations:^{
        self.periodicTable.hidden = YES;
        self.buttonsView.hidden = YES;
        [self updatePositions];
    }];
}

#pragma mark - actions
-(void)animateScale:(UIView*)view
{
    if(!DIGIT_IS_SCALABLE)return;
    __block CGAffineTransform transform = view.transform;
    [UIView animateWithDuration:0.16 animations:^{
        CGAffineTransform scale = CGAffineTransformMakeScale(1.2, 1.2);
        view.transform = scale;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.14 animations:^{
            view.transform = transform;
        }];
    }];
}

-(void)rotateAction:(id)sender
{
    [self.periodicTable swapRotationAnimated:YES];
}

-(void)dig0Clicked:(id)sender
{
    [self inputDigit:0];
    [self animateScale:sender];
}

-(void)dig1Clicked:(id)sender
{
    [self inputDigit:1];
    [self animateScale:sender];
}

-(void)dig2Clicked:(id)sender
{
    [self inputDigit:2];
    [self animateScale:sender];
}

-(void)dig3Clicked:(id)sender
{
    [self inputDigit:3];
    [self animateScale:sender];
}

-(void)dig4Clicked:(id)sender
{
    [self inputDigit:4];
    [self animateScale:sender];
}

-(void)dig5Clicked:(id)sender
{
    [self inputDigit:5];
    [self animateScale:sender];
}

-(void)dig6Clicked:(id)sender
{
    [self inputDigit:6];
    [self animateScale:sender];
}

-(void)dig7Clicked:(id)sender
{
    [self inputDigit:7];
    [self animateScale:sender];
}

-(void)dig8Clicked:(id)sender
{
    [self inputDigit:8];
    [self animateScale:sender];
}

-(void)dig9Clicked:(id)sender
{
    [self inputDigit:9];
    [self animateScale:sender];
}

-(void)leftBracketClicked:(id)sender
{
    [self inputLeftBracket];
    [self animateScale:sender];
}

-(void)rightBracketClicked:(id)sender
{
    [self inputRightBracket];
    [self animateScale:sender];
}

-(void)removeClicked:(id)sender
{
    MoleculeContainer* lastStack = [self.stack lastObject];
    if(lastStack != nil) [self removeMoleculeFromStackAndDisplay:lastStack];
    else [self.moleculeDisplay removeLast];
    
    //check if it's last bracket
    if(self.stack.count == 0 || [self lastOpenedMoleculeContainer] == nil)
        self.isBracketOpen = NO;
}

-(void)viewResultAction:(id)sender
{
    ResultsViewController* res = [ResultsViewController new];
//    res.substance1 = self.substance1Display.molecule;
//    res.substance2 = self.substance2Display.molecule;
    res.density = [self.densityField.text floatValue];
    res.energy = [self.energyField.text floatValue];
//    res.firstObjectThickness = [self.thick1Field.text floatValue];
//    res.secondObjectThickness = [self.thick2Field.text floatValue];
    NSMutableArray* bars = @[].mutableCopy;
    [bars addObject:[BarMaterial barWithMaterial:self.substance1Display.molecule andThickness:[self.thick1Field.text floatValue]]];
    res.substances = bars;
    
    [self.navigationController pushViewController:res animated:YES];
}

#pragma mark - logic

-(void)inputElement:(ChemicalElement*)element
{
    if(!self.isBracketOpen){
        [self.stack removeAllObjects];
    }
    
    MoleculeContainer* mol = [self createEmptyMolecule];
    mol.element = element;
    [self addToStackMolecule:mol];
    if(self.isBracketOpen){
        [self.moleculeDisplay setNeedsDisplay];
    }else{
        [self.moleculeDisplay addMolecule:mol];
    }
}

-(void)inputDigit:(int)dig
{
    MoleculeContainer* molecule = [self lastMoleculeContainer];
    if(molecule == nil) return;
    
    molecule.quantity = [self quantityOfAtomsByAddingTo:molecule.quantity digit:dig];

    [self.moleculeDisplay setNeedsDisplay];
}

//ready
-(void)inputLeftBracket
{
    //удалить ввод из стека - первое открытие скобки
    if(!self.isBracketOpen) {
        [self.stack removeAllObjects];
        MoleculeContainer* molec = [self createEmptyMolecule];
        [self addToStackMolecule:molec];
        [self.moleculeDisplay addMolecule:molec];
        self.isBracketOpen = YES;
        return;
    }

//    MoleculeContainer* rootMolecule = [self.stack objectAtIndex:0];
    MoleculeContainer* newMolecule = [self createEmptyMolecule];
    [self addToStackMolecule:newMolecule];
    [self.moleculeDisplay setNeedsDisplay];
    
}

//may be ready
-(void)inputRightBracket
{
    if(!self.isBracketOpen || !self.stack.count) return;
//    int elementsAfter = 0;
    for (int i = self.stack.count - 1; i >= 0; --i) {
        MoleculeContainer* iCon = [self.stack objectAtIndex:i];
        if([iCon.element isKindOfClass:[NSMutableArray class]] && iCon.quantity == -1){
            //if the array is empty
            if([(NSMutableArray*)iCon.element count] == 0){
                [self removeMoleculeFromStackAndDisplay:iCon];
                if(self.stack.count < 1) self.isBracketOpen = NO;
                return;
            }
            
            iCon.quantity = 0;
            
            if(i + 1 < self.stack.count)
                [self.stack removeObjectsInRange:NSMakeRange(i+1, self.stack.count-(i+1))];
            
            if(self.stack.count <= 1) self.isBracketOpen = NO;
            
            break;
        }
//        elementsAfter++;
    }
    [self.moleculeDisplay setNeedsDisplay];
}

//result - was it added to other molecule
-(BOOL)addToStackMolecule:(MoleculeContainer*)molec
{
    [self.stack addObject:molec];
    if(self.stack.count == 0) return NO;
    
    for (int i = self.stack.count - 2; i >= 0; --i) {
        MoleculeContainer* iCon = [self.stack objectAtIndex:i];
        if([iCon.element isKindOfClass:[NSMutableArray class]]){
            if(iCon.quantity == -1){
                [(NSMutableArray*)iCon.element addObject:molec];
                return YES;
            }
        }
    }
    return NO;
}

-(MoleculeContainer*)createEmptyMolecule
{
    MoleculeContainer* mol = [MoleculeContainer new];
    mol.element = [NSMutableArray array];
    mol.quantity = -1;
    return mol;
}

//remove
/*-(MoleculeContainer*)nestingDepthFromMolecule:(MoleculeContainer*)molecule
{
    if(molecule.element == nil) return molecule;
    if([molecule.element isKindOfClass:[ChemicalElement class]]) return molecule;
    return [self nestingDepthFromMolecule:molecule];
}*/

-(MoleculeContainer*)lastOpenedMoleculeContainer
{
    if(self.stack.count == 0) return nil;
    MoleculeContainer* mol = nil;//[self.stack objectAtIndex:0];
    for (int i = self.stack.count - 1; i >= 0; --i) {
        MoleculeContainer* iCont = [self.stack objectAtIndex:i];
        if([iCont.element isKindOfClass:[NSMutableArray class]] && iCont.quantity == -1){
            mol = iCont;
            break;
        }
    }
    return mol;
}

//last visible molecule
-(MoleculeContainer*)lastMoleculeContainer
{
    if(self.stack.count == 0) return nil;
    MoleculeContainer* mol = [self.stack lastObject];
    for (int i = self.stack.count - 1; i >= 0; --i) {
        MoleculeContainer* iCont = [self.stack objectAtIndex:i];
        if([iCont.element isKindOfClass:[NSMutableArray class]]){
            //if we try to set quant to zero count array - remove it
            if([(NSMutableArray*)iCont.element count] == 0){
                [self removeMoleculeFromStackAndDisplay:iCont];
                if ([self lastOpenedMoleculeContainer] == nil) {
                    self.isBracketOpen = NO;
                }
                mol = nil;
                break;
            }
            
            if(![[(NSMutableArray*)iCont.element lastObject] isEqual:self.stack.lastObject]) break;
            
            if(iCont.quantity != -1)
                mol = iCont;
            break;
        }
    }
    return mol;
}

-(void)removeMoleculeFromStackAndDisplay:(MoleculeContainer*)cont
{
    [self.moleculeDisplay removeMolecule:cont];
    [self removeMolecule:cont fromArray:self.stack];
}
           
-(void)removeMolecule:(MoleculeContainer*)molecToRemove fromArray:(NSMutableArray*)array
{
    if(array.count == 0) return;
    if([array containsObject:molecToRemove])
        [array removeObject:molecToRemove];
    for (MoleculeContainer* molec in array) {
        if([molec.element isKindOfClass:[NSMutableArray class]])
            [self removeMolecule:molecToRemove fromArray:molec.element];
    }
    
}

-(int)quantityOfAtomsByAddingTo:(int)quant digit:(int)dig
{
    if(quant <= 0) quant = 0;
    if(quant < 1){
        quant = dig;
    }else{
        quant = [[NSString stringWithFormat:@"%i%i", quant, dig] intValue];
    }
    if(quant > 999) quant = 999;
    
    return quant;
}


#pragma mark - up fields

- (void)registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShown:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)unregisterForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    // unregister for keyboard notifications while not visible.
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

-(void)keyboardWillShown:(NSNotification*)aNotification
{
    keyboardIsShown = YES;
//    [self moveUpViewAnimated:YES];
    NSDictionary* info = [aNotification userInfo];
    kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    [UIView animateWithDuration:0.25 animations:^{
        self.scrollView.contentSize = CGSizeMake(self.view.width, self.viewResultButton.bottomBound + DISPLAY_OFFSET + kbSize.height - TOOLBAR_HEIGHT);
    }];
    [self updateActiveFieldOffset];
}

-(void)updateActiveFieldOffset
{
    int offset = self.scrollView.contentOffset.y;
    
    if(activeField.bottomBound - offset > self.view.height + TOOLBAR_HEIGHT - kbSize.height){
        int labHeight = self.densityLabel.height;
        int newOffset = self.scrollView.contentOffset.y + kbSize.height - TOOLBAR_HEIGHT;
        if(newOffset > activeField.topBound - labHeight) newOffset = activeField.topBound - labHeight;
        self.scrollView.contentOffset = CGPointMake(0, newOffset);
    }
}

-(void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    keyboardIsShown = NO;
    [UIView animateWithDuration:0.25 animations:^{
       self.scrollView.contentSize = CGSizeMake(self.view.width, self.viewResultButton.bottomBound + DISPLAY_OFFSET);
    }];
}


@end
































