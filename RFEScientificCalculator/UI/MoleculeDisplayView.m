//
//  MoleculeDisplayView.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 26.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "MoleculeDisplayView.h"

#define LEFT_BRACKET @"("
#define RIGHT_BRACKET @")"
#define DEF_FONT_SIZE 20

@interface MoleculeDisplayView ()

@property (nonatomic) NSMutableArray* chars;

@end

@implementation MoleculeDisplayView

-(UIColor*)defaultColor
{
  return [UIColor whiteColor];
}

-(UIFont*)defaultFont
{
  return [UIFont boldSystemFontOfSize:DEF_FONT_SIZE];
}

#pragma mark - properties

-(NSMutableArray*)molecules
{
  if(_molecules == nil){
    _molecules = [NSMutableArray array];
  }
  return _molecules;
}

-(MoleculeContainer*)molecule
{
  MoleculeContainer * mol = [[MoleculeContainer alloc] init];
  mol.element = self.molecules;
  mol.quantity = 1;
  return mol;
}

-(void)setMolecule:(MoleculeContainer *)molecule
{
  if(molecule && [molecule.element isKindOfClass:[NSMutableArray class]]){
    [self setMolecules:molecule.element];
  }
}

-(UIColor*)textColor
{
  return !_textColor ? [self defaultColor] : _textColor;
}

-(int)fontSize
{
  return _fontSize;
}

#pragma  mark - initialization

- (id)initWithFrame:(CGRect)frame
{
  self = [super initWithFrame:frame];
  if (self) {
    self.fontSize = DEF_FONT_SIZE;
    // Initialization code
    self.showsHorizontalScrollIndicator = NO;
    
    UIGestureRecognizer* gest = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(delegateDidBeginEditing)];
    [self addGestureRecognizer:gest];
    
    [self setBorder];
    [self setSelected:NO];
  }
  return self;
}

-(void)addMolecule:(MoleculeContainer *)molecule
{
  [self.molecules addObject:molecule];
  [self setNeedsDisplay];
}

-(void)removeMolecule:(MoleculeContainer *)cont
{
  [self removeMolecule:cont fromArray:self.molecules];
  [self setNeedsDisplay];
}

-(BOOL)containMolecule:(MoleculeContainer *)cont
{
  return [self.molecules containsObject:cont];
}

-(void)removeLast
{
  [self.molecules removeLastObject];
  [self setNeedsDisplay];
}

- (void)removeAll {
  [self.molecules removeAllObjects];
  [self setNeedsDisplay];
}

-(void)removeMolecule:(MoleculeContainer*)molecToRemove fromArray:(NSMutableArray*)array
{
  if(array.count == 0) return;
  if([array containsObject:molecToRemove])
    [array removeObject:molecToRemove];
  for (MoleculeContainer* molec in array) {
    if([molec.element isKindOfClass:[NSMutableArray class]])
      [self removeMolecule:molecToRemove fromArray:molec.element];
  }
  
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
  // Drawing code
  //    int indOffs = 2;
  //    [self drawBackgroundInRect:rect];
  
  
  int fontSize = [self fontSize];
  int indexFontSize = fontSize/2;
  
  UIFont* atomFont = [UIFont systemFontOfSize:fontSize];
  UIFont* indexFont = [UIFont systemFontOfSize:indexFontSize];
  
  UIColor* notActiveCol =
  [self.textColor colorWithAlphaComponent:0.5];
  
  [self.textColor set];
  
  int oX = 5;
  _chars = [self formListOfCharsFrom:self.molecules];
  for (int i = 0; i < _chars.count; ++i) {
    KeyValuePair* pair = [_chars objectAtIndex:i];
    CharType type = [(NSNumber*)pair.key intValue];
    NSString* symb = (NSString*)pair.value;
    
    if(type == CharTypeSymbol || type == CharTypeLeftBracket){
      CGSize symbSize = [symb sizeWithFont:atomFont];
      [symb drawAtPoint:CGPointMake(oX, (self.height - symbSize.height)/2) withFont:atomFont];
      oX += symbSize.width;
    }else if (type == CharTypeRightBracket){//if next el is not index - set light color
      if(i+1<_chars.count){
        KeyValuePair* numb = [_chars objectAtIndex:i+1];
        CharType numbType = [(NSNumber*)numb.key intValue];
        if(numbType != CharTypeIndex || (numbType == CharTypeIndex && [(NSString*)numb.value intValue] < 0)){
          [notActiveCol set];
        }
      }else [notActiveCol set];
      
      CGSize symbSize = [symb sizeWithFont:atomFont];
      [symb drawAtPoint:CGPointMake(oX, (self.height - symbSize.height)/2) withFont:atomFont];
      oX += symbSize.width;
      
      [self.textColor set];
    }else{
      if([symb intValue] > 0){
        CGSize quantSize = [symb sizeWithFont:indexFont];
        [symb drawAtPoint:CGPointMake(oX, self.height/2 + atomFont.lineHeight/2 - quantSize.height) withFont:indexFont];
        oX += quantSize.width;
      }
    }
  }
  
  self.contentSize = CGSizeMake(oX, self.height);
  if(self.contentSize.width > self.width) self.contentOffset = CGPointMake(self.contentSize.width - self.width, 0);
}

-(void)setBorder
{
  self.layer.borderWidth = 1;
  self.layer.cornerRadius = 5;
  self.layer.masksToBounds = YES;
}

-(void)setSelected:(BOOL)selected
{
  _selected = selected;
  if (selected) {
    self.backgroundColor = RGB(68.f, 68.f, 68.f);
    self.layer.borderColor = RGB(133.f, 133.f, 133.f).CGColor;
  } else {
    self.backgroundColor = RGB(77.f, 77.f, 77.f);
    self.layer.borderColor = [UIColor clearColor].CGColor;
  }
}

-(NSMutableArray*)formListOfCharsFrom:(NSMutableArray*)from
{
  NSMutableArray* chars = [NSMutableArray array];
  for (int i = 0; i < from.count; ++i) {
    MoleculeContainer* molec = [from objectAtIndex:i];
    if([molec.element isKindOfClass:[ChemicalElement class]]){
      ChemicalElement* el = (ChemicalElement*)molec.element;
      [chars addObject:[KeyValuePair key:[NSNumber numberWithInt:CharTypeSymbol] value:el.symbol]];
      if(molec.quantity > 0)
        [chars addObject:[KeyValuePair key:[NSNumber numberWithInt:CharTypeIndex] value:[NSString stringWithFormat:@"%i",molec.quantity]]];
    }else{
      NSMutableArray* ret = [self formListOfCharsFrom:molec.element];
      [chars addObject:[KeyValuePair key:[NSNumber numberWithInt:CharTypeLeftBracket] value:LEFT_BRACKET]];
      [chars addObjectsFromArray:ret];
      [chars addObject:[KeyValuePair key:[NSNumber numberWithInt:CharTypeRightBracket] value:RIGHT_BRACKET]];
      //            if(molec.quantity > 0)
      [chars addObject:[KeyValuePair key:[NSNumber numberWithInt:CharTypeIndex] value:[NSString stringWithFormat:@"%i",molec.quantity]]];
    }
  }
  return chars;
}

//not used
/*-(void)drawElement:(MoleculeContainer*)molec at:(int*)oX withAtomFont:(UIFont*)atomFont andIndexFont:(UIFont*)indexFont
 {
 ChemicalElement* elem = molec.element;
 CGSize strSize = [elem.symbol sizeWithFont:atomFont];
 [elem.symbol drawAtPoint:CGPointMake(*oX, (self.height - strSize.height)/2) withFont:atomFont];
 if(molec.quantity >= 1){
 NSString* quantStr = [NSString stringWithFormat:@"%i", molec.quantity];
 CGSize quantSize = [quantStr sizeWithFont:indexFont];
 [quantStr drawAtPoint:CGPointMake(*oX + strSize.width, self.height/2 + strSize.height/2 - quantSize.height) withFont:indexFont];
 *oX += quantSize.width;
 }
 *oX += strSize.width;
 }*/

-(void)drawBackgroundInRect:(CGRect)rect
{
  CGContextRef context = UIGraphicsGetCurrentContext();
  CGContextSetRGBFillColor(context, 1, 1, 1, 1);
  CGContextFillRect(context, rect);
  CGContextSetStrokeColorWithColor(context, [UIColor grayColor].CGColor);
  CGContextSetLineWidth(context, 1);
  CGContextAddRect(context, rect);
  CGContextStrokePath(context);
}

-(void)drawString:(NSString*)nsText inRect:(CGRect)viewRect font:(UIFont*)font
{
  CGSize size = [nsText sizeWithFont:font
                   constrainedToSize:viewRect.size
                       lineBreakMode:(NSLineBreakByWordWrapping)];
  float x_pos = (viewRect.size.width - size.width) / 2;
  float y_pos = (viewRect.size.height - size.height) /2;
  [nsText drawAtPoint:CGPointMake(viewRect.origin.x + x_pos, viewRect.origin.y + y_pos) withFont:font];
}

#pragma mark - delegation

-(void)delegateDidBeginEditing
{
  if([self.delegate respondsToSelector:@selector(moleculeDisplayDidBeginEditing:)])
    [self.delegate moleculeDisplayDidBeginEditing:self];
}

#pragma mark - render

- (UIImage*)formulaImage {
  UIGraphicsBeginImageContextWithOptions(self.contentSize, NO, 2.f);
  [self drawRect:self.bounds];
  UIImage *screenShot = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return screenShot;
}

@end


















