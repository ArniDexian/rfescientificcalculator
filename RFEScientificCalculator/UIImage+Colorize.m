//
//  UIImage+Colorize.m
//  FindTheWay
//
//  Created by ArniDexian on 17.11.13.
//  Copyright (c) 2013 Arni-MacBook. All rights reserved.
//

#import "UIImage+Colorize.h"

@implementation UIImage (Colorize)

+ (UIImage *)imageWithColor:(UIColor *)color {
  return [UIImage imageWithColor:color size:CGSizeMake(1.f, 1.f)];
}

+ (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size {
  UIImage* image = nil;
  if (!CGSizeEqualToSize(size, CGSizeZero)) {
    CGRect rect = CGRectZero;
    rect.size = size;
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
  }
  return image;
}

- (UIImage *)imageWithTint:(UIColor *)tintColor
{
	UIGraphicsBeginImageContextWithOptions(self.size, 0, self.scale);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextTranslateCTM(context, 0, self.size.height);
	CGContextScaleCTM(context, 1.0, -1.0);
	CGRect rect = CGRectMake(0, 0, self.size.width, self.size.height);
	
	CGContextSetBlendMode(context, kCGBlendModeNormal);
	[tintColor setFill];
	CGContextFillRect(context, rect);
	CGContextSetBlendMode(context, kCGBlendModeDestinationIn);
	CGContextDrawImage(context, rect, self.CGImage);
	
	UIImage *coloredImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return coloredImage;
}


@end
