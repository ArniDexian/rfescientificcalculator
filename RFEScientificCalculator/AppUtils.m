//
//  AppUtils.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 24.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "AppUtils.h"
#import <AddressBook/AddressBook.h>
#import <AddressBookUI/AddressBookUI.h>
#import <CommonCrypto/CommonDigest.h>

static char encodingTable[64] = {
    'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',
    'Q','R','S','T','U','V','W','X','Y','Z','a','b','c','d','e','f',
    'g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v',
    'w','x','y','z','0','1','2','3','4','5','6','7','8','9','+','/' };

@implementation KeyValuePair

@synthesize key, value;
+(KeyValuePair*)key:(id)_key value:(id)_value
{
    KeyValuePair*  obj = [[KeyValuePair alloc] init];
    obj.key = _key;
    obj.value = _value;
    return obj;
}

-(NSString*)description
{
    return [NSString stringWithFormat:@"{Key: %@ Value: %@}", self.key, self.value];
}

@end
@implementation AppUtils

+(NSMutableArray*)URLParams:(NSString *)url
{
    NSScanner *scanner = [NSScanner scannerWithString:url];
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"&?"]];
    NSString *tempString;
    NSMutableArray *vars = [NSMutableArray new];
    //ignore the beginning of the string and skip to the vars
    [scanner scanUpToString:@"?" intoString:nil];
    while ([scanner scanUpToString:@"&" intoString:&tempString]) {
        [vars addObject:[tempString copy]];
    }
    NSMutableArray* params = [NSMutableArray arrayWithCapacity:vars.count];
    for (NSString* word in vars) {
        NSArray* subParam = [word componentsSeparatedByString:@"="];
        if(!subParam || subParam.count < 2)
            continue;
        NSString* key = [subParam objectAtIndex:0];
        NSString* value = [subParam objectAtIndex:1];
        KeyValuePair* key_val = [KeyValuePair key:key value:value];
        [params addObject:key_val];
    }
    return params;
}

+(NSString*)urlWithParams:(NSMutableArray*)params forBaseUrl:(NSString*)baseUrl
{
    if(!baseUrl || baseUrl.length == 0)
        return nil;
    //    NSMutableString* resUrl = [NSMutableString stringWithString:baseUrl];
    //    NSString* param = [self paramWithDict:params];
    //    if(!param || param.length == 0)
    //        return [resUrl copy];
    //    [resUrl appendFormat:@"&%@",param];
    //    return resUrl.copy;
    NSMutableString* resUrl = [NSMutableString string];
    if(params.count == 0){
        return baseUrl;
    }
    
    [resUrl appendFormat:@"%@?", baseUrl];
    for (KeyValuePair* key_value in params) {
        [resUrl appendFormat:@"%@=%@&", key_value.key, key_value.value];
    }
    [resUrl deleteCharactersInRange:NSMakeRange([resUrl length]-1, 1)];
    return resUrl;
}

//NOT USED
+(NSString*)paramWithDict:(NSDictionary*)params
{
    if(!params || params.count == 0)
        return nil;
    NSMutableString* param = [NSMutableString string];
    for (NSString* key in params) {
        NSString* value = [params objectForKey:key];
        [param appendFormat:@"%@=%@&", key, value];
    }
    [param deleteCharactersInRange:NSMakeRange([param length]-1, 1)];
    return param;
}

+(NSString *)valueOfURL:(NSString *)url forKey:(NSString*)key{
    NSString *string =  key;//[url stringByReplacingPercentEscapesUsingEncoding:NSStringEncodingConversionAllowLossy];
    //    NSLog(@"string: %@", string);
    NSScanner *scanner = [NSScanner scannerWithString:string];
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"&?"]];
    NSString *tempString;
    NSMutableArray *vars = [NSMutableArray new];
    //ignore the beginning of the string and skip to the vars
    [scanner scanUpToString:@"?" intoString:nil];
    while ([scanner scanUpToString:@"&" intoString:&tempString]) {
        [vars addObject:[tempString copy]];
    }
    NSArray* variables = vars;
    NSString* res = nil;
    for (NSString *var in variables) {
        if ([var length] > [key length]+1 && [[var substringWithRange:NSMakeRange(0, [key length]+1)] isEqualToString:[key stringByAppendingString:@"="]]) {
            res = [var substringFromIndex:[key length]+1];
            break;
        }
    }
    return res;
}

+(NSString*)encodeURL:(NSString*)escapedStr
{
    return [escapedStr stringByReplacingPercentEscapesUsingEncoding:NSStringEncodingConversionAllowLossy];
}

+(NSString*)decodeURL:(NSString*)unescapeStr
{
    NSString *escapedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                                    NULL,
                                                                                                    (__bridge CFStringRef) unescapeStr,
                                                                                                    NULL,
                                                                                                    (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                                    kCFStringEncodingUTF8));
    return escapedString;
}


+(BOOL)isIPad
{
    return [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad;
}

+(BOOL)isIPhone
{
    return [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone;
}

+(void)adjustLabelWidth:(UILabel *)lbl withMargin :(int)marg
{
    [self adjustLabelWidth:lbl constrainedToSize:lbl.frame.size withMargin:marg];
}

+(void)adjustLabelWidth:(UILabel *)lbl constrainedToSize:(CGSize)size withMargin:(int)marg
{
    if(!lbl.text.length) return;
    CGSize expectedLabelSize = [lbl.text sizeWithFont:lbl.font
                                    constrainedToSize:size
                                        lineBreakMode:lbl.lineBreakMode];
    if(expectedLabelSize.width == 0 && lbl.text.length > 0)return;
    //adjust the label the the new height.
    CGRect newFrame = lbl.frame;
    newFrame.size.width = expectedLabelSize.width + 2*marg;
    newFrame.size.height = expectedLabelSize.height + 2*marg;
    lbl.frame = newFrame;
}

+(void)adjustButtonWidth:(UIButton *)button withMargin:(int)marg minWidth:(int)minButtonWidth
{
    UILabel* label = button.titleLabel;
    if(label.text.length == 0) return;
    
    CGSize expSize = [label.text sizeWithFont:label.font];
    
    button.width = ((expSize.width + 2*marg) <= minButtonWidth) ? minButtonWidth : expSize.width + 2*marg;
}

+(UIInterfaceOrientation)orientation
{
    return [[UIApplication sharedApplication] statusBarOrientation];
}

+(CGSize)landscapeFrameSize
{
    CGRect frame = [UIScreen mainScreen].bounds;
    return CGSizeMake(frame.size.height, frame.size.width-20);
}

+(CGSize)portraitFrameSize
{
    CGRect frame = [UIScreen mainScreen].bounds;
    return CGSizeMake(frame.size.width, frame.size.height-20);
}

+(CGSize)landscapeFrameSizeWithBar
{
    CGSize size = [self landscapeFrameSize];
    return CGSizeMake(size.width, size.height - 44);
}

+(CGSize)portraitFrameSizeWithBar
{
    CGSize size = [self portraitFrameSize];
    return CGSizeMake(size.width, size.height - 44);
}

+(CGSize)currentFrameSize
{
    return UIInterfaceOrientationIsLandscape([self orientation]) ? [self landscapeFrameSize] : [self portraitFrameSize];
}

+(CGSize)currentFrameSizeWithBar
{
    return UIInterfaceOrientationIsLandscape([self orientation]) ? [self landscapeFrameSizeWithBar] : [self portraitFrameSizeWithBar];
}

+(CGRect)landscapeBounds
{
    CGRect frame = [UIScreen mainScreen].bounds;
    frame.size = CGSizeMake(frame.size.height, frame.size.width);
    return frame;
}

+(CGRect)portraitBounds
{
    return [UIScreen mainScreen].bounds;
}

+(void)adjustSegmControl:(UISegmentedControl*)segmentedControl withFont:(UIFont*)font
{
    for (id segment in [segmentedControl subviews])
    {
        for (id label in [segment subviews])
        {
            if ([label isKindOfClass:[UILabel class]])
            {
                [label setTextAlignment:NSTextAlignmentCenter];
                [label setFont:font];
                //[label setTextColor:[UIColor greenColor]];
            }
        }
    }
}

+(BOOL)aspectIs4x3
{
    return YES;
}

+(NSString*)getUserEmail
{
    //    ABAddressBookRef adrBook = nil;
    //    adrBook = ABAddressBookCreate();
    return nil;
}

+(BOOL)isVal:(Float64)val1 apprEqualTo:(Float64)val2
{
    float appr = 0.5f;
    return val1 <= val2 + appr && val1 >= val2 - appr;
}

+(NSString *) base64EncodingWithLineLength:(NSData*) data {
    
    const unsigned char* bytesArr=[data bytes];
    unsigned int lineLength=0;
    NSMutableString *result = [NSMutableString stringWithCapacity:[data length]];
    unsigned long ixtext = 0;
    unsigned long lentext = [data length];
    long ctremaining = 0;
    unsigned char inbuf[3], outbuf[4];
    unsigned short i = 0;
    unsigned short charsonline = 0, ctcopy = 0;
    unsigned long ix = 0;
    
    while( YES ) {
        ctremaining = lentext - ixtext;
        if( ctremaining <= 0 ) break;
        
        for( i = 0; i < 3; i++ ) {
            ix = ixtext + i;
            if( ix < lentext ) inbuf[i] = bytesArr[ix];
            else inbuf [i] = 0;
        }
        
        outbuf [0] = (inbuf [0] & 0xFC) >> 2;
        outbuf [1] = ((inbuf [0] & 0x03) << 4) | ((inbuf [1] & 0xF0) >> 4);
        outbuf [2] = ((inbuf [1] & 0x0F) << 2) | ((inbuf [2] & 0xC0) >> 6);
        outbuf [3] = inbuf [2] & 0x3F;
        ctcopy = 4;
        
        switch( ctremaining ) {
            case 1:
                ctcopy = 2;
                break;
            case 2:
                ctcopy = 3;
                break;
        }
        
        for( i = 0; i < ctcopy; i++ )
            [result appendFormat:@"%c", encodingTable[outbuf[i]]];
        
        for( i = ctcopy; i < 4; i++ )
            [result appendString:@"="];
        
        ixtext += 3;
        charsonline += 4;
        
        if( lineLength > 0 ) {
            if( charsonline >= lineLength ) {
                charsonline = 0;
                [result appendString:@"\n"];
            }
        }
    }
    
    return [NSString stringWithString:result];
}

+(void)stopTimer:(NSTimer*)timer
{
    if(!timer)return;
    if([timer isValid]){
        [timer invalidate];
    }
    timer = nil;
}

+(void)openUrlInBrowser:(NSURL*)url
{
    [[UIApplication sharedApplication] openURL:url];
}

+ (NSString*)md5HexDigest:(NSString*)input {
    const char* str = [input UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, strlen(str), result);
    
    NSMutableString *ret = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
    for(int i = 0; i<CC_MD5_DIGEST_LENGTH; i++) {
        [ret appendFormat:@"%02x",result[i]];
    }
    return ret;
}

+ (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

+ (UIColor *)colorFromHexString:(NSString *)hexString {
    if(hexString == nil)return nil;
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}

+(BOOL)isFileVideo:(NSString*)path
{
    BOOL isVid = NO;
    NSString* ext = [[path pathExtension] lowercaseString];
    if([ext isEqualToString:@"mp4"]){
        isVid = YES;
    }
    return isVid;
}

+(BOOL)isFileImage:(NSString*)path
{
    BOOL isImg = NO;
    NSString* ext = [[path pathExtension] lowercaseString];
    if([ext isEqualToString:@"jpg"]){
        isImg = YES;
    } else if([ext isEqualToString:@"jpeg"]){
        isImg = YES;
    } else if([ext isEqualToString:@"gif"]){
        isImg = YES;
    }
    else if([ext isEqualToString:@"png"]){
        isImg = YES;
    }
    
    return isImg;
}

+(CAGradientLayer*)gradientWithTopColor:(UIColor *)topColor andBottomColot:(UIColor *)bottomColor forBounds:(CGRect)bounds andCornerRadius:(CGFloat)radius
{
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = bounds;
    
    gradientLayer.colors = [NSArray arrayWithObjects:(id)topColor.CGColor,(id)bottomColor.CGColor,nil];
    gradientLayer.locations = [NSArray arrayWithObjects:
                               [NSNumber numberWithFloat:0.0f],
                               [NSNumber numberWithFloat:1.0f],
                               nil];
    
    gradientLayer.cornerRadius = radius;
    return gradientLayer;
}

+(CAGradientLayer*)gradientWithLeftColor:(UIColor *)leftColor andRightColot:(UIColor *)rightColor forBounds:(CGRect)bounds andCornerRadius:(CGFloat)radius
{
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = bounds;
    
    gradientLayer.colors = [NSArray arrayWithObjects:(id)leftColor.CGColor,(id)rightColor.CGColor,nil];
    //    gradientLayer.locations = [NSArray arrayWithObjects:
    //                               [NSNumber numberWithFloat:0.0f],
    //                               [NSNumber numberWithFloat:1.0f],
    //                               nil];
    [gradientLayer setStartPoint:CGPointMake(0.0, 1.0)];
    [gradientLayer setEndPoint:CGPointMake(1.0, 1.0)];
    
    gradientLayer.cornerRadius = radius;
    return gradientLayer;
}

+(void)setMaskTo:(UIView*)view byRoundingCorners:(UIRectCorner)corners withRadius:(CGFloat)radius
{
    UIBezierPath* rounded = [UIBezierPath bezierPathWithRoundedRect:view.bounds byRoundingCorners:corners cornerRadii:CGSizeMake(radius, radius)];
    
    CAShapeLayer* shape = [[CAShapeLayer alloc] init];
    [shape setPath:rounded.CGPath];
    
    view.layer.mask = shape;
}

+(UIView*)barButtonView:(UIBarButtonItem *)item
{
    return [item valueForKey:@"view"];
}

+(void)setBorderForSubviews:(UIView*)view
{
    for (UIView* v in view.subviews) {
        v.layer.borderWidth = 1;
        //        CGColorRef col = RGB(arc4random(), arc4random(), arc4random()).CGColor;
        v.layer.borderColor = RANDCOLOR().CGColor;
        v.layer.backgroundColor = CGColorCreateCopyWithAlpha(v.layer.borderColor, 0.3);
        [self setBorderForSubviews:v];
    }
}
@end
