//
//  Formatter.h
//  RFEScientificCalculator
//
//  Created by ArniDexian on 25.05.16.
//  Copyright © 2016 Boolbalabs LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Formatter : NSObject

@property (nonatomic, strong) NSNumberFormatter* decimalFormatter;
@property (nonatomic, strong) NSNumberFormatter* percentFormatter;

+ (instancetype)shared;

- (NSString*)stringFromDecimal:(NSDecimalNumber*)decimal;
- (NSString*)stringPercentFromDecimal:(NSDecimalNumber*)decimal;

@end
