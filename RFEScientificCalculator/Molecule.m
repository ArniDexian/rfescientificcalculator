//
//  Molecule.m
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 26.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import "Molecule.h"


@implementation MoleculeContainer

+(id)emptyMolecule
{
  MoleculeContainer* mol = [MoleculeContainer new];
  mol.element = [NSMutableArray array];
  mol.quantity = -1;
  return mol;
}

+ (id)moleculeWithArray:(NSArray*)array {
  MoleculeContainer* molec = [MoleculeContainer emptyMolecule];
  molec.element = array;
  molec.quantity = 1;
  return molec;
}

- (id)copyWithZone:(NSZone *)zone {
  typeof(self) copy = [[self class] new];
  copy.element = [_element copy];
  copy.quantity = _quantity;
  return copy;
}

+ (id)moleculeWithElement:(id)element andQuantity:(int)quantity {
  MoleculeContainer* mol = [MoleculeContainer new];
  mol.element = element;
  mol.quantity = quantity;
  return mol;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
  self = [super init];
  if (self) {
    self.element = [coder decodeObjectForKey:NSStringFromSelector(@selector(element))];
    self.quantity = [[coder decodeObjectForKey:NSStringFromSelector(@selector(quantity))] intValue];
  }
  return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
  [coder encodeObject:self.element forKey:NSStringFromSelector(@selector(element))];
  [coder encodeObject:@(self.quantity) forKey:NSStringFromSelector(@selector(quantity))];
}

- (NSString*)description {
  NSMutableString* res = [NSMutableString string];
  if ([self.element isKindOfClass:[ChemicalElement class]]) {
    [res appendFormat:@"%@%i ", [self.element symbol], self.quantity];
  } else if ([self.element isKindOfClass:[NSArray class]]) {
    for (MoleculeContainer* cont in self.element) {
      [res appendString:[cont description]];
    }
  }
  return [res copy];
}

@end
