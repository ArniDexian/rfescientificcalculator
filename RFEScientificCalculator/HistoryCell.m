//
//  HistoryCell.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 22/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "HistoryCell.h"
#import "YPCellBackgroundView.h"

@implementation HistoryCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
      [self awakeFromNib];
    }
    return self;
}

- (void)awakeFromNib {
  self.backgroundColor = [UIColor clearColor];
  self.backgroundView = [YPCellBackgroundView new];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+ (CGFloat)cellHeight {
  return 40;
}

@end
