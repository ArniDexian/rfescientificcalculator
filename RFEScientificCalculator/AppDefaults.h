//
//  NSObject_AppDefaults.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 24.03.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <Foundation/Foundation.h>

static const CGSize ElementViewSize = {36.f, 36.f};
static const NSInteger ElementSymbolFontSize = 16;
static const NSInteger ElementAtomicMassFontSize = 10;
static const NSInteger ElementIndexFontSize = 10;
static const NSInteger ElementNameFontSize = 10;

