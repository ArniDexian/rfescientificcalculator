//
//  AbsorptanceTable.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 10.04.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Array2D.h"

@interface AbsorptanceTable : NSObject

+ (AbsorptanceTable*)sharedInstance;

@property (nonatomic, strong) NSArray* data;

- (NSDecimalNumber*)absorptanceForSource:(int)sourceInd andAbsorber:(int)absorberInd;

@end
