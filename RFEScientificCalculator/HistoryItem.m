//
//  HistoryItem.m
//  RFEScientificCalculator
//
//  Created by ArniDexian on 22/05/14.
//  Copyright (c) 2014 Boolbalabs LLC. All rights reserved.
//

#import "HistoryItem.h"

@implementation HistoryItem

- (instancetype)initWithCoder:(NSCoder *)coder
{
  self = [super init];
  if (self) {
    self.molecule = [coder decodeObjectForKey:NSStringFromSelector(@selector(molecule))];
    self.density = [coder decodeObjectForKey:NSStringFromSelector(@selector(density))];
    self.formulaImage = [coder decodeObjectForKey:NSStringFromSelector(@selector(formulaImage))];
    self.stringFormula = [coder decodeObjectForKey:NSStringFromSelector(@selector(stringFormula))];
  }
  return self;
}

- (void)encodeWithCoder:(NSCoder *)coder {
  [coder encodeObject:self.molecule forKey:NSStringFromSelector(@selector(molecule))];
  [coder encodeObject:self.density forKey:NSStringFromSelector(@selector(density))];
  [coder encodeObject:self.formulaImage forKey:NSStringFromSelector(@selector(formulaImage))];
  [coder encodeObject:self.stringFormula forKey:NSStringFromSelector(@selector(stringFormula))];
}

- (BOOL)isEqual:(HistoryItem*)other {
  if (other == self) {
    return YES;
  } else {
    return [self.stringFormula isEqualToString:other.stringFormula];
  }
}

- (NSUInteger)hash
{
  return [self.stringFormula hash];
}

+ (instancetype)historyItemWithMolecule:(MoleculeContainer*)molecule density:(NSDecimalNumber*)density formulaImage:(UIImage*)image {
  HistoryItem* item = [HistoryItem new];
  item.molecule = [MoleculeContainer moleculeWithArray:[[AbsorptanceCalculator calculatorWithMolecule:molecule andDensity:density] simplifyMolecule:molecule]];
  item.density = density;
  item.formulaImage = image;
  item.stringFormula = [[[[item.molecule description] componentsSeparatedByString:@" "] sortedArrayUsingSelector:@selector(compare:)] componentsJoinedByString:@""];
  return item;
}

@end
