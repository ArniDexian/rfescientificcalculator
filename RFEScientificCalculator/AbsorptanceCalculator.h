//
//  AbsorptanceCalculator.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 14.04.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AbsorptanceTable.h"
#import "Molecule.h"
#import "ChemicalElement.h"
#import "AppUtils.h"

@interface MoleculeAbsorptance : NSObject

/**
 *Value input energy
 */
@property (nonatomic, strong) NSDecimalNumber* decInputEnergy;

/**
 *Apsorptance for input energy
 */
@property (nonatomic, strong) NSDecimalNumber* decAbsorptance;

/**
 *Apsorptance value was interpolated by input energy
 */
@property (nonatomic, assign) BOOL interpolated;
/**
 *Value of nearest table energy to input energy
 */
@property (nonatomic, strong) NSDecimalNumber* decTableEnergy;
/**
 *Table value of absorptance for tableEnergy
 */
@property (nonatomic, strong) NSDecimalNumber* decTableAbsorptance;
/**
 *  Calculated table delta as df(E)
 */
@property (nonatomic, strong) NSDecimalNumber* tableAbsorptanceDelta;
/**
 *  Calculated table delta as df(E)
 */
@property (nonatomic, strong) NSDecimalNumber* absorptanceDelta;

- (BOOL)hasAbsorptance;

- (NSDecimalNumber*)absorptanceError;
- (NSDecimalNumber*)tableAbsorptanceError;

@end

@interface AbsorptanceCalculator : NSObject

+ (instancetype)calculatorWithMolecule:(id)molecule andDensity:(NSDecimalNumber*)density;

+ (BOOL)validateMolecule:(id)molecule;
- (NSArray*)simplifyMolecule:(id)molecule;

- (MoleculeAbsorptance*)absorptanceOfMoleculeWithEnergy:(NSDecimalNumber*)energy;

- (void)setMoleculeForCalculation:(id)molecule withDensity:(NSDecimalNumber*)density;

/**
 *Returns chemical element if the molecule has simple structure
 */
+ (ChemicalElement*)simpleMolecule:(id)molecule;

/**
 *Check if the molecule not contains elements
 */
+ (BOOL)isEmptyMolecule:(id)molecule;

@end






















