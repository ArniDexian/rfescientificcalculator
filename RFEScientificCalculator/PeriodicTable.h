//
//  PeriodicTable.h
//  RFEScientificCalculator
//
//  Created by Arni-MacBook on 10.04.13.
//  Copyright (c) 2013 Archepix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ChemicalElement.h"

@interface PeriodicTableEnergyResult : NSObject

/**
 *Element with energy to search
 */
@property (nonatomic, assign) NSDecimalNumber* inputEnergy;
/**
 *Nearest result
 */
@property (nonatomic, assign) ChemicalElement* element;
/**
 *If the result isn't precise
 */
@property (nonatomic, assign) BOOL approximated;
/**
 *When result is approximated left nearest
 */
@property (nonatomic, strong) ChemicalElement* approximatedLeftElement;
/**
 *When result is approximated right nearest
 */
@property (nonatomic, strong) ChemicalElement* approximatedRightElement;

@end


@interface PeriodicTable : NSObject

+(PeriodicTable *)sharedInstance;

@property (nonatomic, strong) NSArray* elements;
@property (nonatomic, assign) NSDecimalNumber* maxEnergy;

- (PeriodicTableEnergyResult*)elementWithEnergy:(NSDecimalNumber*)energy;

- (NSArray*)elementsStartingWithName:(NSString*)name;

@end





